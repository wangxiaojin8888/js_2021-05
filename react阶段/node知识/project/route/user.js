const express = require('express'),
    route = express.Router(),
    fs = require('fs').promises,
    path = require('path');
const { handleMD5, responsePublic, getUserInfo, Token } = require('../utils');
const pathUser = path.resolve(__dirname, '../database/user.json');

// 用户登录
route.post('/login', (req, res) => {
    // 获取客户端基于请求主体传递的账号密码「依赖BODY-PARSER中间件」
    let { account, password } = req.body;
    // 为了保护密码的安全，服务器存储的密码都是在md5加密的基础上二次加密的
    password = handleMD5(password);
    // 到全部的用户中查找,和当前账号密码匹配的那一个用户
    let item = req.$userDATA.find(item => {
        let { name, email, phone } = item;
        return (name === account || email === account || phone === account) && item.password === password;
    });
    if (!item) {
        // 登录失败
        responsePublic(res, false);
        return;
    }
    // 登录成功:获取登录者信息 && 根据登录者的信息生成Token && 返回给客户端结果
    item = getUserInfo(item.id, req);
    let token = Token.sign(item);
    responsePublic(res, true, {
        token,
        info: item
    });
});

// 校验用户是否登录
route.get('/login', (req, res) => {
    // 能通过中间件的Token校验进入到这里，说明已经是登录的了；
    // req.$token中存储的就是当前登录者的信息！
    responsePublic(res, true, {
        info: req.$token
    });
});

// 获取指定条件下的用户列表
route.get('/list', (req, res) => {
    // 获取客户端问号传参的信息
    let { departmentId = 0, search = '' } = req.query;
    // 按照条件进行筛选
    let data = req.$userDATA;
    if (+departmentId !== 0) {
        // 按照部门筛选
        data = data.filter(item => +item.departmentId === +departmentId);
    }
    if (search.length > 0) {
        // 按照关键词筛选
        data = data.filter(item => {
            let { name, email, phone } = item;
            return name.includes(search) || email.includes(search)
                || phone.includes(search);
        });
    }
    // 把筛选后的数据返回客户端
    data = data.map(item => getUserInfo(item.id, req));
    responsePublic(res, true, {
        data
    });
});

// 获取指定用户的详细信息
route.get('/info', (req, res) => {
    let { userId } = req.query;
    if (!userId) {
        // 获取登录者信息
        responsePublic(res, true, {
            info: req.$token
        });
        return;
    }
    // 获取指定编号用户的信息
    let info = getUserInfo(userId, req);
    if (!info.hasOwnProperty('name')) {
        // 当前用户不存在
        responsePublic(res, false, { info: null });
        return;
    }
    // 找到了
    responsePublic(res, true, {
        info
    });
});

// 新增用户
route.post('/add', async (req, res) => {
    try {
        let data = req.$userDATA;
        // 把请求主体传递的信息替换默认的用户信息
        let item = Object.assign({
            id: data.length === 0 ? 1 : (+data[data.length - 1]['id'] + 1),
            name: '',
            password: "8376ac810bb9f231d28fcf1f",
            sex: 0,
            email: '',
            phone: '',
            departmentId: 1,
            jobId: 1,
            desc: '',
            time: +new Date(),
            state: 0
        }, req.body);
        // 把这一项放在数组末尾 && 重新写入到文件中
        data.push(item);
        await fs.writeFile(pathUser, JSON.stringify(data), 'utf-8');
        responsePublic(res, true);
    } catch (err) {
        // 新增失败
        responsePublic(res, false, {
            codeText: err.message
        });
    }
});

// 修改用户信息
route.post('/update', async (req, res) => {
    try {
        // 在现有数组中找到要修改那一项(有可能那一项不存在)，按照请求主体传递的信息替换这一项的内容；把替换后的数组重新写入到文件中即可；
        let data = req.$userDATA,
            flag = false;
        data = data.map(item => {
            if (+item.id === +req.body.userId) {
                // 要被修改的这一项
                Reflect.deleteProperty(req.body, 'userId');
                item = Object.assign(item, req.body);
                flag = true;
            }
            return item;
        });
        if (!flag) {
            // 一项都没有修改
            responsePublic(res, false, { codeText: 'no match!' });
            return;
        }
        // 有修改
        await fs.writeFile(pathUser, JSON.stringify(data), 'utf-8');
        responsePublic(res, true);
    } catch (err) {
        responsePublic(res, false, { codeText: err.message });
    }
});

// 删除用户信息「本质也是修改，只是把state改为1」
route.get('/delete', async (req, res) => {
    try {
        let { userId } = req.query;
        let data = req.$userDATA,
            flag = false;
        data = data.map(item => {
            if (+item.id === +userId) {
                item.state = 1;
                flag = true;
            }
            return item;
        });
        if (!flag) {
            responsePublic(res, false, { codeText: 'no match!' });
            return;
        }
        await fs.writeFile(pathUser, JSON.stringify(data), 'utf-8');
        responsePublic(res, true);
    } catch (err) {
        responsePublic(res, false, { codeText: err.message });
    }
});

module.exports = route;