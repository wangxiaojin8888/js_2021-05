const express = require('express'),
    route = express.Router(),
    fs = require('fs').promises,
    path = require('path');
const { responsePublic, getCustomerInfo } = require('../utils');

// 获取客户列表，支持筛选和分页
route.get('/list', (req, res) => {
    // 获取问号传参的信息(赋值默认值)
    let { lx = 'all', type = '', search = '', limit = 10, page = 1 } = req.query;
    // 权限校验
    if (lx === 'all' && !req.$token.power.includes('customerall')) {
        responsePublic(res, false, {
            codeText: 'You do not have this permission'
        });
        return;
    }
    // 获取所有客户信息，按照type/search/lx进行筛选
    let data = req.$customerDATA;
    if (type.length > 0) data = data.filter(item => item.type === type);
    if (search.length > 0) {
        data = data.filter(item => {
            let { name, email, phone, QQ, weixin } = item;
            return name.includes(search) || email.includes(search) || phone.includes(search) || QQ.includes(search) || weixin.includes(search);
        });
    }
    if (lx === 'my') data = data.filter(item => +item.userId === +req.$token.id);
    // 进行分页处理
    limit = +limit;
    page = +page;
    let total = data.length,
        totalPage = Math.ceil(total / limit),
        arr = [];
    for (let i = (page - 1) * limit; i <= page * limit - 1; i++) {
        let item = data[i];
        if (!item) break;
        arr.push(getCustomerInfo(item.id, req));
    }
    responsePublic(res, true, {
        page,
        limit,
        total,
        totalPage,
        data: arr
    });
});

module.exports = route;