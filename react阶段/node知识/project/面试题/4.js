const handle = arr => {
    // 循环数组{从第二项~倒数第二项}：找到某一项既比左边大、也比右边大的，返回这一项的索引即可「所谓的峰值」
    for (let i = 1; i <= arr.length - 2; i++) {
        let item = arr[i],
            prev = arr[i - 1],
            next = arr[i + 1];
        if (item > prev && item > next) return i;
    }
    return -1;
};
console.log(handle([1, 2, 1, 3, 4, 5, 7, 6]));