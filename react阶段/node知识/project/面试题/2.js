/* 
 arr中包含每个人要购买票的数量
 k是亚历克斯的编号「按照数组索引算的」，例如：k是1，亚历克斯排第二位，需要购买2张票
 ---
 每个人购买一张票算一个时间节点，买完一张后，如果还需要再买，则需要退到数组的末尾重新排队...最后返回亚历克斯买完需要的票所经历的时间节点
 */
const handle = (arr, k) => {
    /* 第一个人买  1-1=0 直接移除数组队列即可  [2,5]
    第二个人买(亚历克斯) 2-1=1 把其放在队列尾部 [5,1]
    第三个人买 5-1=4 把其放在队列尾部 [1,4]
    第四个人买(亚历克斯) 1-1=0 直接移除数组队列即可 [4] */
    
    // @1 先给亚历克斯做标记
    arr = arr.map((item, index) => {
        return {
            n: item,
            b: index === k
        };
    });
    // arr = [{ n: 1, b: false }, { n: 2, b: true }, { n: 5, b: false }]
    // n:记录每个人购买的数量 b记录是不是亚历克斯

    //@2 基于递归开始排队买票
    let num = 0;
    const operate = () => {
        num++;
        let item = arr[0],
            diff = item.n - 1;
        if (diff <= 0) {
            arr.splice(0, 1);
        } else {
            item.n = diff;
            arr.push(arr.shift());
        }
        if (diff <= 0 && item.b) return; //亚历克斯已经买完了
        operate();
    };
    operate();
    return num;
};
console.log(handle([3, 2, 1], 0));