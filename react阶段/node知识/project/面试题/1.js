const handle = (str, sub) => {
    let res = '';
    [].forEach.call(str, item => {
        if (sub.indexOf(item) === -1) {
            res += item;
        }
    });
    return res;
};
console.log(handle('They are students', 'aeiou'));
// => 还可以把两个字符串变为数组「split」，接下来求两个数组的“差集”即可