/* [3,4,2,5,1,0] k=2
right [3,4,5]
left [1,0]
center [2,2]
left+center+right = [1,0,2,2,3,4,5] */
const handle = (arr, k) => {
    if (arr.length === 0) return 0;
    if (arr.indexOf(k) === -1) return 0;
    let left = [],
        right = [],
        center = [];
    arr.forEach(item => {
        if (item < k) left.push(item);
        if (item > k) right.push(item);
        if (item == k) center.push(item);
    });
    arr = left.concat(center, right);
    return arr.indexOf(k);
};
console.log(handle([5, 3, 2, 6, 4, 3, 2, 5, 1], 3));