const express = require('express'),
    bodyParse = require('body-parser'),
    fs = require('fs').promises,
    path = require('path'),
    app = express(),
    config = require('./package.json').config,
    port = config.server,
    cros = config.cros;
const pathDataBase = path.resolve(__dirname, './database');
const { filter, Token, responsePublic } = require('./utils');

/* 创建服务 & 监听端口 */
app.listen(port, () => console.log(`服务已正常启动,监听的端口号是:${port}`));

/* 使用中间件 */
// 处理CROS跨域请求
if (cros.open) {
    app.use((req, res, next) => {
        let origin = req.headers.origin || req.headers.referer || "";
        origin = origin.replace(/\/$/g, '');
        origin = !cros.safeList.includes(origin) ? '' : origin;
        res.header("Access-Control-Allow-Origin", origin);
        res.header("Access-Control-Allow-Credentials", true);
        res.header("Access-Control-Allow-Methods", 'GET,POST,PUT,DELETE,HEAD,OPTIONS');
        res.header("Access-Control-Allow-Headers", 'DNT,web-token,app-token,authorization,Accept,Origin,Keep-Alive,User-Agent,X-Mx-ReqToken,X-Data-Type,X-Auth-Token,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range');
        req.method === 'OPTIONS' ? res.send() : next();
    });
}

// 处理请求主体信息
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({ extended: true }));

// 处理API接口之前,把存储的数据获取到
app.use(async (req, res, next) => {
    try {
        req.$customerDATA = filter(await fs.readFile(`${pathDataBase}/customer.json`, `utf-8`));
        req.$departmentDATA = filter(await fs.readFile(`${pathDataBase}/department.json`, `utf-8`));
        req.$jobDATA = filter(await fs.readFile(`${pathDataBase}/job.json`, `utf-8`));
        req.$userDATA = filter(await fs.readFile(`${pathDataBase}/user.json`, `utf-8`));
        req.$visitDATA = filter(await fs.readFile(`${pathDataBase}/visit.json`, `utf-8`));
        next();
    } catch (err) {
        res.status(500);
        res.send();
    }
});

// 处理Token的校验
app.use(async (req, res, next) => {
    // 某些接口不需要进行校验「例如登录」
    if (req.path.includes('/user/login') && /^POST$/i.test(req.method)) {
        next();
        return;
    }
    // 获取客户端基于请求主体传递的token信息
    let authorization = req.headers['authorization'];
    let { token, data } = Token.verify(authorization);
    if (!token) {
        // 无效的token信息
        responsePublic(res, false, {
            codeText: 'The token information is illegal!'
        });
        return;
    }
    // token信息有效
    req.$token = data;
    next();
});

/* 基于路由管理各版块中的接口 */
app.use('/user', require('./route/user'));
app.use('/customer', require('./route/customer'));
app.use('/department', require('./route/department'));
app.use('/job', require('./route/job'));
app.use('/visit', require('./route/visit'));

/* 处理静态资源文件 & 404页面 */
app.use(express.static('./static'));
app.use((_, res) => {
    res.status(404);
    res.send();
});