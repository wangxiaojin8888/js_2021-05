let a = 10,
    b = 20;
// a = a + b; //a是总和
// b = a - b; //用总和-b的值 是原始a的值，赋值给b
// a = a - b; //用总和-b的值(已经是原始a的值) ，相当于把原始b的值赋值给a
[b, a] = [a, b];
console.log(a, b);

// console.log(global);