const express = require('express'),
    route = express.Router(),
    path = require('path'),
    fs = require('fs').promises;
const pathPackage = path.resolve(__dirname, '../package.json'),
    pathUser = path.resolve(__dirname, '../database/user.json');

route.get('/list', async (req, res) => {
    let { lx = 'pro' } = req.query;
    let data = await fs.readFile(pathPackage, 'utf-8');
    data = JSON.parse(data);
    data = lx === 'dev' ? data['devDependencies'] : data['dependencies'];
    res.send({
        code: 0,
        codeText: 'OK',
        data
    });
});

route.post('/insert', async (req, res) => {
    try {
        let data = await fs.readFile(pathUser, 'utf-8');
        data = JSON.parse(data);
        data.push(req.body);
        await fs.writeFile(pathUser, JSON.stringify(data), 'utf-8')
        res.send({
            code: 0,
            codeText: 'OK'
        });
    } catch (err) {
        res.send({
            code: 1,
            codeText: err.message
        });
    }
});

module.exports = route;