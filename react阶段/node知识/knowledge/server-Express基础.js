const express = require('express'),
    app = express(),
    PORT = 80;
/* app.listen:创建web服务并且监听指定的端口号 */
app.listen(PORT, () => {
    console.log(`Web服务启动成功,监听的端口号是：${PORT}~~`);
});

/* 
 数据服务器：完成对接口请求的处理 
   req(request对象):存储着客户端发送的请求信息(含:请求地址、请求方式、请求头、请求主体等)
     + method 请求方式
     + query 获取问号传参的信息(自动解析为对象键值对)
     + body 配合其他的中间件，可以获取请求主体信息
     + path 获取除协议、域名、端口号外的请求地址信息
     + headers 存储着请求头信息
     + ...
   res(response对象):存储着相关的方法，可以供后台把数据返回给客户端
     + status 设置HTTP状态码，默认200
     + type 设置服务器返回内容的MIME类型
     + send 设置响应主体信息，格式：字符串、Buffer；如果写的是普通对象，默认会转换JSON字符串返回；
     + header 设置响应头信息
     + ...
 */
const fs = require('fs').promises,
    bodyParser = require('body-parser');

// bodyParser中间件就是用来解析请求主体信息，并且挂载到req.body上
app.use(bodyParser.json()); //识别客户端请求主体传递的JSON字符串，并且把它变为对象
app.use(bodyParser.urlencoded({ //识别客户端请求主体传递的URLENCODED格式字符串，并且把它变为对象
    extended: true
}));

/* 
// app.use() 使用中间件：把多个请求公共的部分先进行统一的处理，处理完再进入真正的请求中执行
app.use(async (req, res, next) => {
    // 如果是POST系列请求，我们获取请求主体信息「把获取的信息放在req.body上」
    if (/^(POST|PUT|PATCH)$/i.test(req.method)) {
        let str = ``;
        req.on('data', chunk => {
            // 正在一点点的接收客户端请求主体内容，chunk是本次接收到的那一些信息
            str += chunk;
        });
        req.on('end', () => {
            // 请求主体信息已经全部接收完毕
            req.body = JSON.parse(str);
            next();
        });
        return;
    }
    next();
}); 
*/

app.get('/list', async (req, res) => {
    // 如果请求方式是GET，请求地址是“/list”，则执行此回调函数；在此函数中，把客户端需要的数据(例如：package.json)返回给客户端！
    // 获取问号传参的信息
    let { lx = 'pro' } = req.query;

    // 获取文件内容
    let data = await fs.readFile('./package.json', 'utf-8');
    data = JSON.parse(data);
    data = lx === 'dev' ? data['devDependencies'] : data['dependencies'];

    // 把内容返回给客户端
    res.send({
        code: 0,
        codeText: 'OK',
        data
    });
});

app.post('/insert', async (req, res) => {
    // 处理POST请求
    try {
        // 1.获取现有user中的信息，把客户端传递的信息push到数组的末尾
        let data = await fs.readFile('./database/user.json', 'utf-8');
        data = JSON.parse(data);
        data.push(req.body);
        // 2.把最新的数组信息重新写入到user中
        await fs.writeFile('./database/user.json', JSON.stringify(data), 'utf-8')
        // 3.反馈给客户端对应的结果
        res.send({
            code: 0,
            codeText: 'OK'
        });
    } catch (err) {
        res.send({
            code: 1,
            codeText: err.message
        });
    }
});

/* 指定Web资源查找的目录「完成404的处理」 */
app.use(express.static('./static'));
app.use((_, res) => {
    res.status(404);
    res.send();
});