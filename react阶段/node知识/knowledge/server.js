const express = require('express'),
    app = express(),
    PORT = 80;
app.listen(PORT, () => {
    console.log(`Web服务启动成功,监听的端口号是：${PORT}~~`);
});

/* 使用中间件 */
// 处理CROS跨域的
const safeList = [
    'http://127.0.0.1:5500',
    'http://127.0.0.1:8080',
    'http://127.0.0.1:3000'
];
app.use((req, res, next) => {
    let origin = req.headers.origin || req.headers.referer || "";
    origin = origin.replace(/\/$/g, '');
    origin = !safeList.includes(origin) ? '' : origin;
    res.header("Access-Control-Allow-Origin", origin);
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Methods", 'GET,POST,PUT,DELETE,HEAD,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'DNT,web-token,app-token,Authorization,Accept,Origin,Keep-Alive,User-Agent,X-Mx-ReqToken,X-Data-Type,X-Auth-Token,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range');
    req.method === 'OPTIONS' ? res.send() : next();
});

// 处理请求主体
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/* API接口的管理 */
app.use('/user', require('./route/user'));
app.use('/job', require('./route/job'));

/* 指定Web资源查找的目录「完成404的处理」 */
app.use(express.static('./static'));
app.use((_, res) => {
    res.status(404);
    res.send();
});