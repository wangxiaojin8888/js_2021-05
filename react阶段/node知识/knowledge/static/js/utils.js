(function () {
    "use strict";
    /* 检测数据类型的 */
    const getProto = Object.getPrototypeOf,
        class2type = {},
        toString = class2type.toString, //Object.prototype.toString
        hasOwn = class2type.hasOwnProperty; //Object.prototype.hasOwnProperty

    // 检测是否为函数
    const isFunction = function isFunction(obj) {
        return typeof obj === "function" && typeof obj.nodeType !== "number" &&
            typeof obj.item !== "function";
    };

    // 检测是否为window对象
    const isWindow = function isWindow(obj) {
        return obj != null && obj === obj.window;
    };

    // 通用检测数据类型的办法，返回结果：字符串、含小写的数据类型
    const toType = function toType(obj) {
        let reg = /^\[object (.+)\]$/;
        if (obj == null) return obj + "";
        return typeof obj === "object" || typeof obj === "function" ?
            reg.exec(toString.call(obj))[1].toLowerCase() :
            typeof obj;
    };

    // 检测是否为标准普通对象(纯粹对象)
    const isPlainObject = function isPlainObject(obj) {
        let proto, Ctor;
        if (!obj || toString.call(obj) !== "[object Object]") return false;
        proto = getProto(obj);
        if (!proto) return true;
        Ctor = hasOwn.call(proto, "constructor") && proto.constructor;
        return typeof Ctor === "function" && Ctor === Object;
    };

    // 检测是否为空对象
    const isEmptyObject = function isEmptyObject(obj) {
        if (obj == null || !/^(object|function)$/.test(typeof obj)) return false;
        let keys = Object.getOwnPropertyNames(obj);
        if (typeof Symbol !== "undefined") keys = keys.concat(Object.getOwnPropertySymbols(obj));
        return keys.length === 0;
    };

    // 检测是否为数组或者类数组
    const isArrayLike = function isArrayLike(obj) {
        let length = !!obj && "length" in obj && obj.length,
            type = toType(obj);
        if (isFunction(obj) || isWindow(obj)) return false;
        return type === "array" || length === 0 ||
            typeof length === "number" && length > 0 && (length - 1) in obj;
    };

    /* 函数防抖&节流 */
    const clearTimer = function clearTimer(timer) {
        if (timer !== null) clearTimeout(timer);
        return null;
    };
    const debounce = function debounce(func, wait, immediate) {
        if (typeof func !== "function") throw new TypeError("func is not a function!");
        if (typeof wait === "boolean") immediate = wait;
        if (typeof wait !== "number") wait = 300;
        if (typeof immediate !== "boolean") immediate = false;
        let timer = null;
        return function operate(...params) {
            let now = !timer && immediate;
            timer = clearTimer(timer);
            timer = setTimeout(() => {
                timer = clearTimer(timer);
                if (!immediate) func.apply(this, params);
            }, wait);
            if (now) return func.apply(this, params);
        };
    };
    const throttle = function throttle(func, wait) {
        if (typeof func !== "function") throw new TypeError("func is not a function!");
        if (typeof wait !== "number") wait = 300;
        let timer = null,
            previous = 0;
        return function operate(...params) {
            let now = +new Date(),
                remaining = wait - (now - previous);
            if (remaining <= 0) {
                timer = clearTimer(timer);
                previous = +new Date();
                return func.apply(this, params);
            }
            if (!timer) {
                timer = setTimeout(() => {
                    timer = clearTimer(timer);
                    previous = +new Date();
                    func.apply(this, params);
                }, remaining);
            }
        };
    };

    /* 暴露API */
    const utils = {
        version: '1.0.0',
        debounce,
        throttle,
        isFunction,
        isWindow,
        toType,
        isPlainObject,
        isEmptyObject,
        isArrayLike
    };
    if (typeof module === "object" && typeof module.exports === "object") module.exports = utils;
    if (typeof window !== "undefined") window.utils = utils;
})();