(function () {
  //搜索框
  let inp = document.querySelector('#inp'),
    btn = document.querySelector('.btn'),
    spring = document.querySelector('.spring'),
    del = document.querySelectorAll('.del'),
    scaCode = document.querySelector('.scaCode'),
    way = document.querySelector('#way'),
    load = document.querySelector('.load');

  //轮播图获取
  let dotBox = document.querySelector('.dot_box'),
    dots = dotBox.getElementsByTagName('span'),
    inBan = document.querySelector('.inBan'),
    imgBox = Array.from(inBan.getElementsByTagName('img')),
    bigBanner = document.querySelector('.bigBanner'),
    outerBanner = document.querySelector('.outerBanner'),
    leftBtn = document.querySelector('.leftBtn'),
    rightBtn = document.querySelector('.rightBtn');

  //热门推荐获取的元素
  let middleWrap = document.querySelector('.middleWrap'),
    rollBox = document.querySelector('.roll_box'),
    ol = document.querySelectorAll('ol'),
    singerBox = document.querySelector('.singer_box'),
    anchorBox = document.querySelector('.anchor_box'),
    bottomList = document.querySelector('.bottomList'),
    numBox;

  //回到顶部
  let backTop = document.querySelector('.backTop');

  //最下面盒子
  let below = document.querySelector('.below'),
    belowBox = document.querySelector('.below_box'),
    lock = document.querySelector('.lock>a');
  let lockBox = false,
    play = false;

  //播放按钮
  let suspend = document.querySelector('.suspend'),
    //点击播放按钮让播放盒子出现
    contentBox = document.querySelector('.contentBox');

  let audio = document.querySelector('#audio');

  let cur = document.querySelector('.cur');

  let index = 0;
  let timer, timer2,timer3;
  let max = 0;
  let dataAry = [];

  function query(url) {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          let data = JSON.parse(xhr.response);
          resolve(data);
        }
      };
      xhr.send();
    });
  }

  query('./data.json').then(data => {
    max = data.length;
    dataAry = data;
    renderBanner(data);
    imgBox = Array.from(inBan.getElementsByTagName('img'));
    move();
    bigBan(data);
    bindDotEvent();
  });
  query('./hot.json').then(data => {
    renderHot(data);
    renderAudio(data[index], false);
  });
  query('./new.json').then(data => {
    renderNew(data);
    renderAudio(data[index], false);
  });
  query('./song1.json').then(data => {
    renderSoar(data, ol[0]);
    numBox = document.querySelectorAll('dd>ol>li>span');
    renderAudio(data[index], false);
  });
  query('./song2.json').then(data => {
    renderSoar(data, ol[1]);
    renderAudio(data[index], false);
  });
  query('./song3.json').then(data => {
    renderSoar(data, ol[2]);
    renderAudio(data[index], false);
  });
  query('./ResidentSinger.json').then(data => {
    renderReRinger(data);
  });
  query('./anchor.json').then(data => {
    anchor(data);
  });

  //渲染热门推荐界面
  function renderHot(ary) {
    let str = '';
    ary.forEach(item => {
      let { pic, http, text, listen } = item;
      str += `<li>
                <div class="card">
                  <img src="${pic}" alt="">
                    <a class="msk cardBg" href=""></a>
                    <div class="blockBg cardBg">
                      <a class="iconfont icon-bofang" href="javascript:;" data-id='a'></a>
                      <span class="headset iconfont icon-iconheadset"></span>
                      <span class="nb">${listen}</span>
                    </div>
                    <p class="describe">
                      <a href="">
                        <i class="radio-Bg"></i>
                      ${text}</a>
                    </p>
                </div>
              </li>`;
    });
    middleWrap.innerHTML = str;
  }

  //渲染新碟上架的界面
  function renderNew(ary) {
    let str = '';
    ary.forEach(item => {
      let { pic, http, Album, singer } = item;
      str += `<li class="s-bg">
                <div class="cover">
                  <img src="${pic}" alt="">
                  <a class="cardBg" href=""></a>
                  <a class="play iconfont icon-bofang" href="javascript:;" data-id='a'></a>
                </div>
                <p>
                  <a href="">${Album}</a>
                </p>
                <p>
                  <a class="name" href="">${singer}</a>
                  <a class="name" href="">&nbsp;</a>
                </p>
              </li>`;
    });
    rollBox.innerHTML = str;
  }

  //渲染下面飙升榜、新歌榜、原创榜
  function renderSoar(ary, box) {
    let str = '';
    ary.forEach((item, index) => {
      let { songTitle, id } = item;
      str += `<li>
                <span class="${index <= 2 ? 'num' : ''}" >${id}</span>
                <a class="song" href="">${songTitle}</a>
                <div class="oper">
                  <a class="iconfont icon-bofang " href="javascript:;" data-id='a'></a>
                  <a class="iconfont icon-76xinzeng" href=""></a>
                  <a class="iconfont icon-tianjiaxiangmu1" href=""></a>
                </div>
              </li>`;
    });
    box.innerHTML = str;
  }

  //渲染入驻歌手界面
  function renderReRinger(ary) {
    let str = '';
    ary.forEach(item => {
      let { pic, name, describe } = item;
      str += `<li>
              <a class="vocalist clearfix" href="">
                <img src="${pic}" alt="">
                <div class="info">
                  <h4>${name}</h4>
                  <p>${describe}</p>
                </div>
              </a>
            </li>`;
    });
    singerBox.innerHTML = str;
  }

  //渲染电台主播界面
  function anchor(ary) {
    let str = '';
    ary.forEach(item => {
      let { pic, name, describe } = item;
      str += `<li>
              <a href="">
                <img src="${pic}" alt="">
              </a>
              <div class="infom">
                <a href="">${name}</a>
                <p>${describe}</p>
              </div>
            </li>`;
    });
    anchorBox.innerHTML = str;
  }

  // 渲染轮播图页面
  function renderBanner(ary) {
    let str = '',
      str2 = '';
    ary.forEach((item, index) => {
      str += `<a href="">
            <img class="${index == 0 ? 'active' : ''}" src="${
        item.pic
      }" alt="" title="1" />
          </a>`;
      str2 += `<span class='${index == 0 ? 'current' : ''}'></span>`;
    });
    inBan.innerHTML = str;
    dotBox.innerHTML = str2;
  }
  function changeImg() {
    //给每个img添加类名
    imgBox.forEach(item => {
      item.classList.remove('active');
    });
    imgBox[index > max ? 0 : index]?.classList?.add('active');
    console.log();
  }
  function changeDots() {
    //轮播图动，下面的圆点也动
    [...dots].forEach(item => {
      item.classList.remove('current');
    });
    dots[index]?.classList?.add('current');
  }
  function bigBan(ary) {
    //背景图移动
    timer2 = setInterval(() => {
      bigBanner.style.background = `url(${ary[index].pic})`;
      bigBanner.style.backgroundSize = `6000px`;
    }, 2000);
  }
  function move() {
    timer = setInterval(() => {
      next();
    }, 2000);
  }
  function next() {
    //下一张轮播图
    index++;
    if (index >= max) {
      index = 0;
    }
    changeImg();
    changeDots();
    bigBanner.style.background = `url(${dataAry[index].pic})`;
    bigBanner.style.backgroundSize = `6000px`;
  }
  function prev() {
    index--;
    if (index < 0) {
      index = max;
    }
    changeImg();
    changeDots();
    bigBanner.style.background = `url(${dataAry[index].pic})`;
    bigBanner.style.backgroundSize = `6000px`;
  }
  outerBanner.onmouseenter = function () {
    clearInterval(timer);
    clearInterval(timer2);
  };
  outerBanner.onmouseleave = function () {
    move();
    bigBan(dataAry);
  };
  leftBtn.onclick = function () {
    prev();
  };
  rightBtn.onclick = function () {
    next();
  };
  function bindDotEvent() {
    [...dots].forEach((item, i) => {
      item.onclick = function () {
        index = i;
        changeImg();
        changeDots();
      };
    });
  }

  //搜索框
  inp.onfocus = function () {
    inp.placeholder = '';
  };
  inp.onblur = function () {
    inp.placeholder = '音乐/视频/电台/用户';
  };
  //弹出登录框
  btn.onclick = function () {
    spring.style.display = 'block';
  };
  del[0].onclick = function () {
    spring.style.display = 'none';
  };
  scaCode.onclick = function () {
    way.style.display = 'block';
  };
  del[1].onclick = function () {
    way.style.display = 'none';
  };
  load.onclick = function () {
    spring.style.display = 'block';
  };

  //回到顶部盒子
  window.onscroll = utils.throttle(function () {
    if (document.documentElement.scrollTop === 0) {
      backTop.style.display = 'none';
    } else {
      backTop.style.display = 'block';
    }
  });

  //让下面盒子出现
  below.onmouseenter = function () {
    belowBox.classList.add('active');
  };
  below.onmouseleave = function () {
    belowBox.classList.remove('active');
  };
  lock.onclick = function () {
    if (!lockBox) {
      lock.classList.add('openLock');
      belowBox.classList.add('active');
      below.onmouseleave = 'none';
      lockBox = true;
    } else {
      lock.classList.remove('openLock');
      belowBox.classList.remove('active');
      lockBox = false;
      below.onmouseleave = function () {
        belowBox.classList.remove('active');
      };
    }
  };

  //点击播放按钮
  function playBox() {
    suspend.onclick = function () {
      if (!play) {
        suspend.classList.remove('icon-bofang1');
        suspend.classList.add('icon-zanting1');
        play = true;
        audio.play();
        renderAudio();
        renderBar();
        play = true;
      } else {
        suspend.classList.add('icon-bofang1');
        suspend.classList.remove('icon-zanting1');
        audio.pause();
        clearInterval(timer3);
        play = false;
      }
    };
  }
  playBox();

  //点击播放按钮的时候，让最下面盒子出现
  contentBox.onclick = function (e) {
    if (e.target.dataset.id === 'a') {
      belowBox.classList.add('active');
      suspend.classList.add('icon-zanting1');
      audio.play();
      renderAudio();
      renderBar();
    } else {
      belowBox.classList.remove('active');
    }
  };

  function renderAudio(item, flag) {
    audio.src = item.src;
  }

  //进度条移动
  function renderBar() {
    timer3 = setInterval(() => {
      cur.style.width =
        (audio.currentTime / audio.duration) * 100 + '%';
    }, 200);
  }

})();
