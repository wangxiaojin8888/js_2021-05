const fs = require('fs').promises,
    lessc = require('less');
let list = ['index.less', 'login.less'];
list.forEach(async item => {
    let [name] = item.split('.');
    let pathname1 = `./less/${item}`,
        pathname2 = `./css/${name}-min.css`;
    // 获取less中的代码，基于lessc.render方法进行编译，把编译后的css写入到指定文件中
    let lessText = await fs.readFile(pathname1, 'utf-8');
    let output = await lessc.render(lessText, { compress: true });
    fs.writeFile(pathname2, output.css, 'utf-8');
});