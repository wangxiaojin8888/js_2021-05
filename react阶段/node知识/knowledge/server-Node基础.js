/* 
 导入内置/第三方模块，前面无需加相对地址；但是导入自定义模块，必须加相对地址（例如：./ 或 ../ 或 /）！！ 
 */
/* let path = require('path'),
    fs = require('fs'),
    fsPromise = fs.promises;
// console.log(path.resolve()); //获取当前文件所在绝对路径，类似于__dirname
// path.resolve(url1,url2) 在url1的基础上，基于url2计算出一个新的路径
// console.log(path.resolve('/knowledge/test/xxx', '../1.js')); // /knowledge/test/1.js
// console.log(path.resolve('/knowledge/test/xxx', './1.js')); // /knowledge/test/xxx/1.js
// console.log(path.resolve('/knowledge/test/xxx', '/1.js')); // /1.js
// console.log(path.resolve('/knowledge/test/xxx', '../../1.js')); // /knowledge/1.js
// console.log(path.resolve(__dirname, './package.json')); //最后返回的是package.json的绝对路径

let pathname = path.resolve(__dirname, './package.json'); */

/* 
// 传统异步读取是基于回调函数的方式管理，容易引发回调地狱，所以我们一般都是基于promise管理
//   读取成功：返回成功的promise实例，值是读取的内容
//   读取失败：返回失败的promise实例，值是失败的原因
(async function () {
    try {
        let result = await fsPromise.readFile(pathname, 'utf-8');
        // 读取成功
        console.log(result);
    } catch (err) {
        // 读取失败
    }
})();
console.log('OK'); 
*/

/* // 真实项目中我们一般会采用“无阻塞式I/O”操作：也就是异步读取文件中的内容
fs.readFile(pathname, 'utf-8', (err, result) => {
    if (err) {
        // 读取失败，err错误对象记录了错误的原因
        return;
    }
    // 读取成功
    console.log(result);
}); */

/* // readFileSync：同步读取文件中的内容,默认获取的是Buffer格式的数据;设置utf-8后，会把读取到的内容，默认转换为字符串格式！！{同步读取会阻碍代码渲染：阻塞式I/O，不推荐}
let result = fs.readFileSync(pathname, 'utf-8');
console.log(result); */


//====================
const path = require('path'),
    fs = require('fs').promises;

(async function () {
    /* // writeFile 向文件中写入内容，写入内容的格式是字符串/Buffer格式
    //   + 如果文件不存在，则默认创建文件！！{文件夹必须是存在的}
    //   + 每一次的写入都是把之前的内容覆盖掉
    await fs.writeFile('./test/1.txt', '昨天下了大雪~~', 'utf-8'); */

    /* // 如果想向现有内容中追加新的信息：appendFile 向文件的末尾追加新的内容
    await fs.appendFile('./test/1.txt', '\n今天天气很好~~', 'utf-8'); */

    /* // 真实项目中我们往往是：向原有内容(一般是JSON对象)中间插入/修改信息
    //  + 先获取原有的内容(把获取的字符串转换为对象)
    //  + 按照需求更改
    //  + 把最新更改后的内容(变为字符串)重新写入到文件中即可
    let data = JSON.parse(await fs.readFile('./package.json', 'utf-8'));
    data['scripts']['build'] = "webpack-dev-server";
    await fs.writeFile('./package.json', JSON.stringify(data), 'utf-8'); */
})();

(async function () {
    /* // readdir:读取指定目录下所有的文件列表，返回结果是一个数组
    let pathList = await fs.readdir('./');
    console.log(pathList); */

    /* // mkdir 在指定目录下创建文件夹，如果文件夹已经存在，再创建就会返回失败的promise实例
    await fs.mkdir('./upload'); */

    /* // rmdir 删除指定的文件夹，注意：如果文件夹中还存在文件，是不允许删除的！！
    await fs.rmdir('./upload'); */

    /* // unlink 删除指定的文件
    await fs.unlink('./static/index.html');
    await fs.rmdir('./static'); */
})();

// 强制删除某个目录
const forceRemoveDir = async function forceRemoveDir(pathname) {
    try {
        // 获取目录中的文件/文件夹列表
        let fileList = await fs.readdir(pathname);
        if (!fileList || fileList.length === 0) {
            // 目录中没有任何的文件：直接删除
            await fs.rmdir(pathname);
        } else {
            // 目录中有文件：我们需要先把文件删除掉，然后再删除目录
            fileList = fileList.map(item => {
                let temp = path.resolve(pathname, `./${item}`);
                if (/\.[a-zA-Z\d]+/.test(item)) {
                    // 文件
                    return fs.unlink(temp);
                }
                // 文件夹
                return forceRemoveDir(temp);
            });
            await Promise.all(fileList);
            // 文件都删除后，我们删除这个文件夹即可
            await fs.rmdir(pathname);
        }
    } catch (_) {
        throw new Error('操作异常！请重试！');
    }
};
// forceRemoveDir('./test');