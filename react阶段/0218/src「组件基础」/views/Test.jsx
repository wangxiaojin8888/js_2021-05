import React, { Component, PureComponent } from "react";

/*
 基于this.setState修改状态，一般都是异步的：目的是保障更新阶段,相关周期函数的有序执行；也是为了连续出现多次setState,但是我们只更新一次,类似于渲染队列机制！！
   + 周期函数中执行setState是异步的
   + 合成事件绑定(onXxx=xxx)的方法中,setState一般也是异步的
   + ...

 但是如果setState出现在其他的异步操作中，则它本身会变为同步操作!!
   + 定时器
   + 手动基于addEventListener实现事件绑定
   + ...
 */
export default class Test extends Component {
    state = {
        num: 0
    };

    change = () => {
        setTimeout(() => {
            this.setState({
                num: this.state.num + 1
            });
            console.log('OK');
        }, 0);
    };

    render() {
        console.log('RENDER');
        let { num } = this.state;
        return <div className="box">
            <span>{num}</span>
            <button onClick={this.change}>点我啊</button>
        </div>;
    }
};


//======================
/*
 如果我们继承的是PureComponent组件，则React内部会默认为我们的组件设置 shouldComponentUpdate 周期函数，并且在周期函数中 拿最新要改变的属性/状态值 和 之前的属性/状态值 进行 “浅比较”!!
   + 对于属性/状态中，如果其中某个状态的值是对象类型，那么新老比较的时候，比较的是地址「这就是浅比较」
   + 但是如果我们自己也设置了 shouldComponentUpdate 周期函数，则以我们自己设置的为主
   + 如果继承了PureComponent，而且某个状态值是对象，我们想修改状态值，也能控制视图更新
     + 每一次都把状态改为新的堆内存地址「可以把之前的状态值copy一份过来，在设置一些新的值即可」
     + 不使用setState更改状态，而是直接this.state.xxx=xxx修改，基于this.forceUpdate()方法通知视图强制更新「执行forceUpdate后，不会走shouldComponentUpdate这个周期函数的校验，直接进入 componentWillUpdate」

// PureComponent其实就是做了和下述类似的事情
shouldComponentUpdate(nextProps, nextState) {
    let { props, state } = this,
        prevKeys = Reflect.ownKeys(state),
        nextKeys = Reflect.ownKeys(nextState),
        flag = false;
    // 拿原始状态state 和 最新的状态nextState 进行比较
    if (prevKeys.length !== nextKeys.length) {
        flag = true;
    } else {
        nextKeys.forEach(key => {
            if (nextState[key] !== state[key]) {
                flag = true;
            }
        });
    }
    return flag;
}
 */
/* export default class Test extends PureComponent {
    state = {
        num: [10, 20]
    };

    change = () => {
        // this.state.num.push(30); //在原始数组对象中加入30这一项
        // this.setState({
        //     num: [...this.state.num] //给新状态设置新的内存地址
        // });
        //----
        this.state.num.push(30);
        this.forceUpdate();
    };

    render() {
        let { num } = this.state;
        return <div className="box">
            <span>{num}</span>
            <button onClick={this.change}>点我啊</button>
        </div>;
    }
}; */

//==========================
/* export default class Test extends Component {
    render() {
        return <div className="box">
            <span ref="spanBox">0</span>
            <button ref={x => this.submit = x}>点我啊</button>
        </div>;
    }
    componentDidMount() {
        const spanBox = this.refs.spanBox,
            submit = this.submit;
        submit.addEventListener('click', () => {
            spanBox.innerHTML++;
        });
    }
}; */
/*
 REF的使用
   方案一 ：给元素设置ref值(例如:xxx)，这样在视图渲染完，我们可以基于 this.refs.xxx 或者这个DOM元素
     + this.refs 是一个对象，存储所有基于ref获取的DOM元素

   方案二「推荐」：利用ref的值是个函数，函数中的x获取的是当前DOM元素，我们把DOM元素(也就是x)挂载到实例上，这样在其它方法中直接基于实例获取就可以了！！
     <button ref={x => {
         // x存储的就是这个button元素
         // 把元素赋值给实例的submit属性，后续基于this.submit就获取到这个元素
         this.submit = x;
     }}>点我啊</button>
 */