import React, { Component } from "react";
import PT from 'prop-types';
import './Vote.less';

export default class Vote extends Component {
    // 设置默认属性值 && 校验规则
    static defaultProps = {
        title: ''
    };
    static propTypes = {
        title: PT.string
    };

    // 设置初始状态
    state = {
        supNum: 10,
        oppNum: 5
    };

    // 支持&反对
    change = type => {
        if (type === 'sup') {
            this.setState({
                supNum: this.state.supNum + 1
            });
            return;
        }
        this.setState({
            oppNum: this.state.oppNum + 1
        });
    };

    render() {
        let { title } = this.props,
            { supNum, oppNum } = this.state;
        // React中没有计算属性，需要啥值，自己编写逻辑处理即可
        let ratio = '--',
            total = supNum + oppNum;
        if (total > 0) ratio = (supNum / total * 100).toFixed(2) + '%';

        return <div className="vote-box">
            <div className="head">
                <h3 className="title">{title}</h3>
                <span>参与人数:{supNum + oppNum}人</span>
            </div>
            <div className="main">
                <p>支持人数：{supNum}人</p>
                <p>反对人数：{oppNum}人</p>
                <p>支持比率：{ratio}</p>
            </div>
            <div className="foot">
                <button onClick={this.change.bind(null, 'sup')}>支持</button>
                <button onClick={this.change.bind(null, 'opp')}>反对</button>
            </div>
        </div>;
    }
};
