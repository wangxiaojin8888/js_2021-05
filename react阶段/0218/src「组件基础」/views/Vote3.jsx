import React, { useState, useEffect, useRef } from 'react';
import './Vote.less';

const Vote = function Vote(props) {
    let { title, children } = props;
    children = React.Children.toArray(children);

    // 创建状态
    let [state, setState] = useState({
        supNum: 10,
        oppNum: 5
    });

    // 按钮操作
    const change = type => {
        if (type === 'sup') {
            setState({
                ...state,
                supNum: state.supNum + 1
            });
            return;
        }
        setState({
            ...state,
            oppNum: state.oppNum + 1
        });
    };

    // 使用REF
    let submit = useRef();

    useEffect(() => {
        // 第一次渲染完
        console.log(submit.current);
    }, []);

    return <div className="vote-box">
        <div className="head">
            <h3 className="title">{title}</h3>
            <span>参与人数:{state.supNum + state.oppNum}人</span>
        </div>
        <div className="main">
            <p>支持人数：{state.supNum}人</p>
            <p>反对人数：{state.oppNum}人</p>
        </div>
        <div className="foot" ref={submit}>
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>
    </div>;
};

//-------------
/* const Vote = function Vote(props) {
    let { title, children } = props;
    children = React.Children.toArray(children);

    // 创建状态
    let [supNum, setSupNum] = useState(10),
        [oppNum, setOppNum] = useState(5);

    // 按钮操作
    const change = type => {
        if (type === 'sup') {
            setSupNum(supNum + 1);
            return;
        }
        setOppNum(oppNum + 1);
    };

    return <div className="vote-box">
        <div className="head">
            <h3 className="title">{title}</h3>
            <span>参与人数:{supNum + oppNum}人</span>
        </div>
        <div className="main">
            <p>支持人数：{supNum}人</p>
            <p>反对人数：{oppNum}人</p>
        </div>
        <div className="foot">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>
    </div>;
}; */
export default Vote;