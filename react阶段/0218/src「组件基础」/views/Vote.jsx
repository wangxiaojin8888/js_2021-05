import React from 'react';
import PT from 'prop-types';
import './Vote.less';

const Vote = function Vote(props) {
    console.log(props);
    let { title, children } = props;
    children = React.Children.toArray(children);

    let supNum = 10;
    const change = () => {
        supNum++;
        console.log(supNum);
    };

    return <div className="vote-box">
        <div className="head">
            <h3 className="title">{title}</h3>
            <span>参与人数:0人</span>
        </div>
        <div className="main">
            <p>支持人数：{supNum}人</p>
            <p>反对人数：0人</p>
            <p>支持比率：--</p>
        </div>
        <div className="foot">
            <button onClick={change}>支持</button>
            <button>反对</button>
        </div>
    </div>;
};
Vote.defaultProps = {
    title: '',
    num: 10
};
Vote.propTypes = {
    title: PT.string
};

export default Vote;