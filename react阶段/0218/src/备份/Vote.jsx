import React, { Component } from 'react';
import VoteMain from './VoteMain';
import VoteFoot from './VoteFoot';
import './Vote.less';

export default class Vote extends Component {
    render() {
        return <div className="vote-box">
            <div className="head">
                <h3 className="title">React好学还是Vue好学?</h3>
                <span>参与人数:0人</span>
            </div>
            <VoteMain />
            <VoteFoot />
        </div>;
    }
};