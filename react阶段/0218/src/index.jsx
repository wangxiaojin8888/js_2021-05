import React from 'react';
import ReactDOM from 'react-dom';
import './assets/reset.min.css';
import Vote from './views/Vote';

ReactDOM.render(
  <>
    <Vote/>
  </>,
  document.getElementById('root')
);