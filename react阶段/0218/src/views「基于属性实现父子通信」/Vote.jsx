import React, { Component } from 'react';
import VoteMain from './VoteMain';
import VoteFoot from './VoteFoot';
import './Vote.less';

export default class Vote extends Component {
    // 初始状态
    state = {
        supNum: 10,
        oppNum: 5
    };

    // 修改状态的方法
    change = type => {
        let { supNum, oppNum } = this.state;
        if (type === 'sup') {
            this.setState({ supNum: supNum + 1 });
            return;
        }
        this.setState({ oppNum: oppNum + 1 });
    };

    render() {
        let { supNum, oppNum } = this.state;
        return <div className="vote-box">
            <div className="head">
                <h3 className="title">React好学还是Vue好学?</h3>
                <span>参与人数:{supNum + oppNum}人</span>
            </div>
            <VoteMain supNum={supNum} oppNum={oppNum} />
            <VoteFoot change={this.change} />
        </div>;
    }
};