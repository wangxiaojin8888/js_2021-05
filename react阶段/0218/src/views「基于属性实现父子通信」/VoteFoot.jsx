import React, { Component } from "react";

export default class VoteFoot extends Component {
    render() {
        let { change } = this.props;
        return <div className="foot">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>;
    }
};