import React, { Component } from "react";
import ThemeContext from "../ThemeContext";

export default class VoteFoot extends Component {
    static contextType = ThemeContext;

    render() {
        let { change } = this.context;
        return <div className="foot">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>;
    }
};