import React from 'react';
import ReactDOM from 'react-dom';

let title = "很适合睡觉~~";
let styObj = {
  color: 'red',
  fontSize: '16px'
};
let arr = [{
  id: 1,
  text: 'Vue框架全家桶'
}, {
  id: 2,
  text: 'React框架全家桶'
}, {
  id: 3,
  text: 'Node全栈开发'
}];

let obj = { name: 'obj' };
const fn = function fn(x, y, ev) {
  console.log(x, y, ev, this);
};

ReactDOM.render(
  <>
    <h2 className="title" style={styObj}>{title}</h2>
    <ul>
      {arr.map((item, index) => {
        return <li key={item.id}>
          {item.text}
          {index >= 2 ? null : <button>删除</button>}
        </li>;
      })}
    </ul>
    {/* 事件绑定的处理 */}
    <button onClick={ev => {
      console.log(ev, this); //ev->“合成事件对象” this->undefined
    }}>打我啊1</button>
    <button onClick={fn}>打我啊2</button>
    <button onClick={fn.bind(obj, 10, 20)}>打我啊3</button>
  </>,
  document.getElementById('root')
);

/*
 JSX:javascript + html 
   @1 基于大括号“{}”嵌入和渲染JS表达式
     + 存储某个值的变量 或者 就是一个值
       + 原始值类型中只能渲染数字和字符串，其余类型值都会渲染为“空”
       + 对象类型值的渲染
         + 不能是普通对象，只能是“JSX元素对象”「排除设置style样式」
         + 可以是数组对象：但并不是把数组变为字符串，而是迭代数组每一项，把每一项单独进行渲染，每一项之间不会存在逗号分隔
         + 如果是事件绑定，可以在大括号中嵌入函数；除此外，其余对象类型的值在大括号中是不能直接渲染的...
     + 也可以进行相关的运算
     + 判断:三元运算符
     + 数组的迭代:使用map方法，因为其支持返回值
     + ...

   @2 给元素设置样式
     + 设置样式类名：不用class，而是基于className代替
     + 设置行内样式：给style设置的值不能是字符串，必须是基于大括号嵌套“样式对象(普通对象中基于键值对设置对应的样式)”

   @3 循环动态绑定内容
     + 基于数组的map方法进行处理
     + 每一个循环创建的元素都要设置属性key，key的值需要是本轮循环唯一的值
       key属性是用来优化DOM-DIFF计算的；不论是vue还是react框架，都是先把视图编译为虚拟DOM，经过DOM-DIFF比较，最后渲染为真实的DOM！！
       + key需要是唯一值：在DOM-DIFF处理的时候，新的虚拟DOM和之前的虚拟DOM对象，是按照相同的key值进行两两比较的
       + key值最好不用循环的索引，而是用不会受到顺序改变而发生变化的值，例如：唯一的id...

   @4 React的JSX中，每一个视图只允许出现一个“根节点”，这样每一个视图外层都要用一个盒子包起来，导致最后HTML结构层级过深！！但是，它提供了 React.Fragment “<></>” 空文档标记标签，基于它可以保证视图只有一个根节点，但是渲染完的结果并不会产生新的层级！！

   @5 JSX视图中的“事件绑定”：onXxx={函数}
     + React中的事件绑定是经过特殊处理的：
       它是“合成事件 SyntheticEvent”，所以获取的事件对象也是“合成事件对象”
       React使用合成事件的原因，是为了处理跨端开发中事件兼容问题，例如：click在移动端的300ms延迟...
         + 基于事件委托实现合成事件
         + 我们给每个元素设置onXxx，其实并没有给每个元素设置事件绑定，都依托于事件的冒泡传播机制，传播到外层容器的相关事件行为上！在外层容器绑定的方法中：
           + 处理事件兼容，并且重构一个新的事件对象出来「合成事件对象」
           + 根据触发的事件源，把之前在事件源上基于onXxx注册的方法执行，所以this默认都被处理为undefined
     + 当事件触发，方法执行，方法中的this不是元素本身，而是undefined

     React技术栈可以实现“跨端开发”：PC、移动端WebApp、移动端NativeApp；Vue框架本身只能做WebApp，无法实现NativeApp，但是可以基于uni-app框架开发NativeApp；
 */