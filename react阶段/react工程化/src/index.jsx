import React from 'react';
import ReactDOM from 'react-dom';

let n = 3;
ReactDOM.render(
  <div className="box" style={{ fontSize: '14px' }} id="box">
    good good study!
    <p className="text">day day up!</p>
    <span>奥利给~~</span>
    {React.createElement(`h${n}`, null, '哈哈哈')}
  </div>,
  document.getElementById('root')
);

/*
 JSX的渲染原理：如何把JSX编译为虚拟DOM、最后如何变为真实的DOM
   @1 基于 babel-preset-react-app 把JSX视图编译为 “React.createElement(...)” 这种格式
     + 但凡是元素节点，都会编译为 React.createElement
     + createElement具备两个及以上的参数
       + 第一个参数 type：存储的是标签名或者组件
       + 第二个参数 props：可能是null(没有任何属性) 或者是 包含元素设置的各个属性的对象
       + 第三个及以上 children：如果有子节点，则一项项传递进来

   @2 把createElement方法执行，创建出虚拟DOM对象「“JSX对象”、“JSX元素对象”」
     + 框架自己内部定义的对象，基于一些键值对，来描述当前标签特征的对象
      {
        $$typeof: Symbol(react.element),
        type:标签名/组件,
        props:{
          包含设置的各个属性,
          childrens:可能没有/可能是一个值/也可能是个数组
        },
        ref:null,
        key:null,
        ...
      }

   @3 基于 ReactDOM.render(虚拟DOM对象,容器,回调函数) 这个方法，把虚拟DOM变为真实的DOM，并且插入到页面指定的容器中「如果设置了回调函数，在真实DOM渲染完，会触发回调函数执行」
 */
let res = React.createElement(
  "div",
  {
    className: "box",
    style: {
      fontSize: '14px'
    },
    id: "box"
  },
  "good good study!",
  React.createElement("p", { className: "text" }, "day day up!"),
  React.createElement("span", null)
);
console.log(res);