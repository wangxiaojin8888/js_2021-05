import React from 'react';
import Button from './button'
import { connect } from 'react-redux'
class List extends React.Component {
  render() {
    let { data = [], dispatch } = this.props;
    return <ul className=''>
      {
        data.map(item => <li key={item}>
          {item}
          <Button className='bbbbb' onClick={() => {
            data = data.filter(v => v != item)
            dispatch({ type: 'changeList', list: data })
          }}>X</Button>
        </li>)
      }
    </ul>;
  }
}

List = connect((state) => {
  return {
    data: state.todoReducer.list,
  }
}, (dispatch) => {
  return {
    dispatch
  }
})(List)
export default List