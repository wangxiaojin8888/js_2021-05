import React from 'react';
/* 
  v-bind='$attrs'
  v-on='$listeners'

*/
class Myinput extends React.Component {
  render() {
    let { onEnter, ...qqq } = this.props;
    console.log(qqq)
    return <div className=''>
      自己的<input
        type="text"
        {...qqq}
        onKeyDown={e => {
          if (e.keyCode == 13) {
            onEnter()
          }
        }}
      />
    </div>;
  }
}
export default Myinput