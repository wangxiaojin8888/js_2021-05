import React from 'react';
import ReactDOM from 'react-dom';
import MyInput from './myinput'
import List from './list'
import store from './store'
import { connect, Provider } from 'react-redux'
/* 
  function 组件
  class 组件
  状态 state 自己的数据 
  属性  props  别人给的

  上下文 context 

  ref='qqq'  this.refs.qqq
  ref={el=>this.qqq=el}   this.qqq
  ref 只能获取类组件 不能获取函数式组件；


*/
function Qqq() {
  return <><h1>rrrrr</h1></>
}
class App2 extends React.Component {
  state = {
    todo: 'zhufeng',
    list: []
  }
  // ddd = React.createRef()
  // componentDidMount() {
  //   console.log(this.ddd.current)
  //   this.ddd.current.value = this.state.todo
  // }
  del = (item) => {
    this.setState({
      list: this.state.list.filter(val => val != item)
    })
  }
  render() {
    /* 
      受控组件 表单的数据受组件数据的控制
    */
    let { todo, list } = this.state;
    return <div className=''>
      <MyInput value={todo} onChange={(e) => {
        this.setState({
          todo: e.target.value
        })
      }} onEnter={() => {
        this.setState({
          todo: '',
          list: [todo, ...list]
        })
      }} />
      <List data={list} onDel={this.del} />
      {/* <input type="text" value={todo} onChange={(e) => {
        this.setState({
          todo: e.target.value
        })
      }} onKeyDown={(e) => {
        if (e.keyCode == 13) {
          this.setState({
            todo: '',
            list: [todo, ...list]
          })
        }
      }} /> */}

      {/* <ul>
        {
          list.map(item => <li key={item}>
            {item}
            <button onClick={
              () => { this.del(item) }
            }>X</button>
          </li>)
        }
      </ul> */}
      {/* <input type="text" ref={this.ddd} onChange={e => {
        this.setState({
          todo: e.target.value
        })
      }} /> */}
    </div>;
  }
}

class App extends React.Component {
  render() {
    let { todo, list, dispatch } = this.props
    console.log('list', list)
    return <>
      <MyInput value={todo} onChange={e => {
        dispatch({ type: "changeTodo", todo: e.target.value })
      }}
        onEnter={e => {
          dispatch({ type: "changeList", list: [todo, ...list] })
          dispatch({ type: "changeTodo", todo: '' })
        }}
      />
      <List/>
    </>
  }
}
App = connect((state) => {
  return {
    todo: state.todoReducer.todo,
    list: state.todoReducer.list,
  }
}, (dispatch) => {
  return {
    dispatch
  }
})(App)

ReactDOM.render(
  <Provider store={store}><App /></Provider>
  , document.getElementById('root'))