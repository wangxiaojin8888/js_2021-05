import { createStore, combineReducers } from 'redux'
// redux-thunk
// dispatch(action) action必须是个对象
// 使用redux-thunk之后 action就可以是一个函数了
const todoReducer = function (state, action) {
  state = state || {
    todo: '',
    list: []
  }
  switch (action.type) {
    case 'changeTodo':
      state = {
        ...state,
        todo: action.todo
      }
      break;
    case 'changeList':
      // console.log(666, action)
      state = {
        ...state,
        list: action.list
      }
      break;

  }
  // console.log(666, state)
  return {
    ...state
  }
}
let rootReducer = combineReducers({
  todoReducer
})

let store = createStore(rootReducer)

export default store