import React from 'react';
class Button extends React.Component {
  render() {
    let { children, ...qqq } = this.props;
    return <button className='' {...qqq}>
      {children}
    </button>;
  }
}
export default Button