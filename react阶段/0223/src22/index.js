import React from 'react';
import ReactDOM from 'react-dom';
import Home from './views/home'
import About from './views/about'
import { HashRouter, Switch, Redirect, BrowserRouter, Link, NavLink, Route } from 'react-router-dom'
/* 
HashRouter :hash模式
BrowserRouter： history模式
Route 用来渲染对应的路径对应的组件 path 控制路径；component 控制组件的；
      render 可以编写逻辑来控制最终的展示的组件 ； exact 精准匹配
Switch： 控制所有的Route从上到下 只要有一个匹配上了 就不再向下匹配了；
Redirect  ：控制重定向  to那就是去哪
Link，NavLink :控制点击跳转路径；to ;NavLink 会有对应的活动类名

子路由 哪个组件的子路由 就在哪个组件中编写Route

凡是Route渲染出来的组件 都会有路由插件给的API(history location match)
若不是Route渲染出来的组件  但是想用这些API,使用 withRouter处理该组件

编程式导航使用history.push .replace;

传参 ：query(?)   params(/:xxx)  state(结合history模式)


useCallback  useMemo 这两个是优化用的 需要结合 memo这个api



*/






let NoFound = function () { return <h1>404</h1> }
class App extends React.Component {

  render() {

    return <div className=''>
      <NavLink to='/' exact>首页</NavLink>
      <NavLink to='/about'>about页</NavLink>
      <Switch>
        <Route path='/' exact component={Home}></Route>
        <Route path='/about' component={About}></Route>
        {/* <Route path='/*' component={NoFound}></Route> */}
        <Route path='/*' render={(props) => {
          return <Redirect to='/' />
        }}></Route>
      </Switch>
    </div>;
  }
}

ReactDOM.render(
  <BrowserRouter><App /></BrowserRouter>
  , document.getElementById('root'))