import React from 'react';
import { Route } from 'react-router-dom'
import A from './A'
import B from './B'
/* 
  传参  
  query
  params 
  state 结合history模式 而且刷新不丢失
*/
class App extends React.Component {
  render() {
    console.log(this.props)
    return <div className=''>
      about

      <button onClick={() => { this.props.history.push('/about/a', { q: 1 }) }}>aaa</button>
      <Route path='/about/a' component={A}></Route>
      <B />
    </div>;
  }
}
export default App