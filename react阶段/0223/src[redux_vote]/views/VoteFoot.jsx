import React, { Component } from "react";
import { Button } from 'antd';
import actions from '../store/actions';
import { connect } from 'react-redux';

class VoteFoot extends Component {
    render() {
        let { support, oppose } = this.props;
        return <div className="foot">
            <Button type="primary" onClick={support}>支持</Button>
            <Button danger onClick={oppose}>反对</Button>
        </div>;
    }
};
export default connect(null, actions.vote)(VoteFoot);