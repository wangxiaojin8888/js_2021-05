import React, { Component } from 'react';
import { TeamOutlined } from '@ant-design/icons';
import VoteMain from './VoteMain';
import VoteFoot from './VoteFoot';
import './Vote.less';
import { connect } from 'react-redux';

class Vote extends Component {
    render() {
        let { supNum, oppNum } = this.props;
        return <div className="vote-box">
            <div className="head">
                <h3 className="title">good good study,day day up!</h3>
                <div className="count">
                    <TeamOutlined />
                    {supNum + oppNum}
                </div>
            </div>
            <VoteMain />
            <VoteFoot />
        </div>;
    }
};
export default connect(state => state.vote)(Vote);

/*
 react-redux提供了connect高阶组件「内部基于闭包/高阶函数管理」
   connect([mapStateToProps],[mapDispatchToProps])(组件)
   + 获取到在根组件上基于Provider注册的上下文信息(也就是获取store)
   + 这样就可以获取公共状态信息、获取disptach派发方法...
   + 最主要的是：它会默认向store的事件池中加入一个，让当前组件更新的方法
   + ...

   connect(
       //mapStateToProps
       state=>{
          //state是redux中的公共状态:{vote:{...},task:{...}}
          //返回的对象中包含哪些信息，最后就基于“属性”把这些信息传递给Vote组件   
          return {
              supNum:state.vote.supNum,
              taskList:state.task.taskList
          };
       }
   )(Vote);

   -----------
   connect(
       null,
       //mapDispatchToProps
       dispatch=>{
         //dispatch:store.dispatch这个派发方法
         //基于return把需要派发的任务，通过属性传递给组件Vote
         return {
             support(){
                dispatch({
                    type:TYPE.VOTE_SUP
                }); 
             },
             oppose(){
                dispatch({
                    type:TYPE.VOTE_OPP
                }); 
             }
         };
       }
   )(Vote);

   但是他有更简便的写法:
   connect(
       null,
       //mapDispatchToProps可以是一个ActionCreator对象「这是简便写法，最后connect会把其变为上述的标准写法」：外层对象中有哪些方法就会给Vote组件传递哪些属性；函数执行，方法中返回的对象，会自动基于dispatch进行派发！！
       {
           support(){
               return {
                   type:TYPE.VOTE_SUP
               }
           },
           oppose(){
               return {
                   type:TYPE.VOTE_OPP
               }
           }
       }
   )(Vote);
 */