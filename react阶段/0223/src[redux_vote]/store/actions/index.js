/* 
合并ACTION 
  actions = {
      vote:{
         support(){...},
         oppose(){...}
      },
      task:{
        ...
      }
  }
*/
import voteAction from "./voteAction";
import taskAction from "./taskAction";

const actions = {
    vote: voteAction,
    task: taskAction
};
export default actions;