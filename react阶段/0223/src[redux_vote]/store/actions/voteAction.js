import * as TYPE from '../action-types';
const query = () => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('OK');
        }, 2000);
    });
};

const voteAction = {
    // redux-promise
    /* async support() {
        await query();
        return {
            type: TYPE.VOTE_SUP
        };
    }, */
    // redux-thunk
    support() {
        return async dispatch => {
            await query();
            dispatch({
                type: TYPE.VOTE_SUP
            });
        };
    },
    oppose() {
        return {
            type: TYPE.VOTE_OPP
        };
    }
};
export default voteAction;