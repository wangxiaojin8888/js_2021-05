import React from 'react';
import ReactDOM from 'react-dom';
import Vote from './views/Vote';
/* ANTD */
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import './assets/reset.min.css';
import 'antd/dist/antd.css';
/* STORE */
import store from './store';
import { Provider } from 'react-redux';

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <Vote />
    </Provider>
  </ConfigProvider>,
  document.getElementById('root')
);