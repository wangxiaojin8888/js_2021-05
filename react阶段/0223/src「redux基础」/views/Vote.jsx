import React, { Component } from 'react';
import { TeamOutlined } from '@ant-design/icons';
import VoteMain from './VoteMain';
import VoteFoot from './VoteFoot';
import './Vote.less';
import Theme from '../store/ThemeContext';

export default class Vote extends Component {
    static contextType = Theme;
    render() {
        // getState():获取公共状态值
        const { store } = this.context;
        let { supNum, oppNum } = store.getState();
        return <div className="vote-box">
            <div className="head">
                <h3 className="title">good good study,day day up!</h3>
                <div className="count">
                    <TeamOutlined />
                    {supNum + oppNum}
                </div>
            </div>
            <VoteMain />
            <VoteFoot />
        </div>;
    }
    // 在第一次渲染完成后,我们需要基于发布订阅,把“让组件更新”的方法加入到STORE的事件池中;只有这样才能保证:公共状态更新,组件视图可以更新,显示最新的公共状态值!!
    // const unsubscribe = store.subscribe(函数);
    //   + 函数的目的是让组件更新
    //   + 返回的unsubscribe执行，可以把刚加入到事件池中的方法移除掉！！
    componentDidMount() {
        const { store } = this.context;
        store.subscribe(() => {
            // 强制更新
            this.forceUpdate();
        });
    }
};