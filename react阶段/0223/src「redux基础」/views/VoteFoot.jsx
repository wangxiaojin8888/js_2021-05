import React, { Component } from "react";
import { Button } from 'antd';
import Theme from "../store/ThemeContext";

export default class VoteFoot extends Component {
    static contextType = Theme;
    render() {
        const { store } = this.context;
        return <div className="foot">
            <Button type="primary" onClick={() => {
                // dispatch([action对象]):派发任务、通知REDUCER执行去修改状态
                store.dispatch({
                    type: 'sup'
                });
            }}>支持</Button>

            <Button danger onClick={() => {
                store.dispatch({
                    type: 'opp'
                });
            }}>反对</Button>
        </div>;
    }
};