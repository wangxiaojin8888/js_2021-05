import React, { useContext, useEffect, useState } from "react";
import Theme from "../store/ThemeContext";

export default function VoteMain() {
    const { store } = useContext(Theme);
    let { supNum, oppNum } = store.getState();

    // 第一次渲染完:向STORE事件池中加入让组件更新的方法
    let [num, setNum] = useState(0);
    useEffect(() => {
        store.subscribe(() => {
            setNum(num + 1);
        });
    }, []);

    return <div className="main">
        <p>支持人数：{supNum}人</p>
        <p>反对人数：{oppNum}人</p>
    </div>;
};