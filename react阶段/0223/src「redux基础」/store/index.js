import { createStore } from 'redux';

/* 
创建修改公共状态的管理员 
  state:存储的是当前容器中的公共状态「第一次派发reducer执行，容器中还没有状态(state===undefined)，此时我们为其赋值初始状态值initial」
  action:每一次dispatch派发传递的行为对象；要求对象中必须具备type属性(派发行为标识)；而reducer中就是根据行为标识不同，基于不同的逻辑修改状态的；
  -----
  reducer执行完的返回结果是啥，就会把原始容器中的公共状态整体替换成啥！！
    + reducer中获取的state和原始容器中的状态是相同的堆内存地址，如果state.xxx=xxx进行操作，也会直接把原始状态值改变了，等不到最后的return「这样不好」
    + 所以我们在进入reducer的第一件事，就是把获取的state拷贝(浅拷贝/深拷贝)一下，这样后续对state的操作不会对原始状态产生影响，只有最后return的时候，才会把最新的state值替换原始状态值！！
*/
const initial = {
    supNum: 10,
    oppNum: 5
};
const reducer = function reducer(state = initial, action) {
    state = { ...state };
    switch (action.type) {
        case 'sup':
            state.supNum++;
            break;
        case 'opp':
            state.oppNum++;
            break;
    }
    return state;
};

/* 创建STORE容器 */
const store = createStore(reducer);
export default store;