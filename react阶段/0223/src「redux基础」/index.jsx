import React from 'react';
import ReactDOM from 'react-dom';
import Vote from './views/Vote';
/* ANTD */
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import './assets/reset.min.css';
import 'antd/dist/antd.css';
/* STORE */
import store from './store';
import Theme from './store/ThemeContext';

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Theme.Provider value={{ store }}>
      <Vote />
    </Theme.Provider>
  </ConfigProvider>,
  document.getElementById('root')
);