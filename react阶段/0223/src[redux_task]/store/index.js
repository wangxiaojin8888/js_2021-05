import { createStore, combineReducers } from 'redux'

const tableDataReducer = function (state = {
  list: []
}, action) {
  switch (action.type) {
    case 'updateTableList':
      state.list = action.list
      break;
    default:
      break;
  }
  return {
    ...state
  }
}



let rootReducer = combineReducers({
  tableData: tableDataReducer,
})

let store = createStore(rootReducer)

export default store
