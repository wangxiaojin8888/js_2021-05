import React from 'react';
import ReactDOM from 'react-dom';
/* ANTD */
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import 'antd/dist/antd.css';
import store from './store'
import { Provider } from 'react-redux'

import Task from './views/Task'

function App() {
  //  hooks

}
class App2 extends React.Component {

}

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <Task />
    </Provider>

  </ConfigProvider>,
  document.getElementById('root')
);