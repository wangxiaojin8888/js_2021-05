import React, { useEffect, useState } from 'react'
import { Button, Tag, Table, Modal, Form, Input, DatePicker, message, Popconfirm } from 'antd';
import { connect } from 'react-redux';
function Task(props) {

  let { TextArea } = Input
  // hook只能用在顶层作用域
  let [state, setState] = useState({
    visible: false,
    confirmLoading: false,
    loading: false,
    selected: 0
  })
  const columns = [{
    title: '编号',
    dataIndex: 'id',
    align: 'center',
    width: '8%'
  }, {
    title: '任务描述',
    dataIndex: 'task',
    width: '50%'
  }, {
    title: '状态',
    dataIndex: 'state',
    align: 'center',
    width: '10%',
    render: text => +text === 1 ? '未完成' : '已完成'
  }, {
    title: '完成时间',
    dataIndex: 'time',
    align: 'center',
    width: '15%',
    render: (_, record) => {
      let { state, time, complete } = record;
      if (+state === 2) time = complete;
      return time;
    }
  }, {
    title: '操作',
    render: (_, record) => {
      let { id, state } = record;
      return <>
        <Popconfirm title="您确定要删除此任务吗?"
          onConfirm={handleDelete.bind(null, id)}>
          <Button type="link">删除</Button>
        </Popconfirm>

        {+state !== 2 ? <Popconfirm title="您确定要修改此任务吗?"
          onConfirm={handleUpdate.bind(null, id)}>
          <Button type="link">完成</Button>
        </Popconfirm> : null}
      </>;
    }
  }];
  const [formObj] = Form.useForm();
  const submit = () => { }
  const cancel = () => {
    setState({
      ...state,
      visible: false
    })
  }
  const handleDelete = (id) => {
    fetch('/api/removeTask?id=' + id).then(res => res.json()).then(data => {
      getData()
    })
  }
  const handleUpdate = () => { }
  const changeTab = (n) => {
    setState({
      ...state,
      selected: n
    })
  }
  let { visible,
    confirmLoading, loading, selected } = state;
  const getData = () => {
    fetch('/api/getTaskList').then(res => res.json()).then(data => {
      console.log(data)
      props.updateList(data.list)
    })
  }
  useEffect(() => {
    // 只触发一次
    getData()
  }, [])
  return (
    <div className="task-box">
      <header className="head-box">
        <h2 className="title">TASK OA 任务管理系统</h2>
        <Button type="primary" onClick={() => setState({
          ...state,
          visible: true
        })}>新增任务</Button>
      </header>

      <section className="tag-box">
        {['全部', '未完成', '已完成'].map((item, index) => {
          return <Tag key={index}
            color={index === selected ? '#108ee9' : ''}
            onClick={changeTab.bind(null, index)}>
            {item}
          </Tag>;
        })}
      </section>

      <Table
        columns={columns}
        dataSource={props.list}
        loading={loading}
        pagination={false}
        rowKey="id" />

      {/* 新增任务弹框 */}
      <Modal
        confirmLoading={confirmLoading}
        keyboard={false}
        maskClosable={false}
        okText="提交信息"
        title="新增任务窗口"
        visible={visible}
        onOk={submit}
        onCancel={cancel}>
        <Form layout="vertical" form={formObj} initialValues={{ task: '', time: '' }}>
          <Form.Item label="任务描述" name="task" validateTrigger="onBlur" rules={[
            { required: true, message: '任务描述是必填项!' },
            { pattern: /^[\w\W]{6,}$/, message: '输入的内容不少于6位!' }
          ]}>
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item label="任务预期完成时间" name="time" validateTrigger="onBlur" rules={[{ required: true, message: '完成时间是必填项!' }]}>
            <DatePicker showTime />
          </Form.Item>
        </Form>
      </Modal>
    </div >
  )
}
Task = connect((rootState) => {
  return {
    list: rootState.tableData.list
  }
}, (dispatch) => {
  return {
    updateList(list) {
      // list新的列表数据
      dispatch({ type: 'updateTableList', list })
    }
  }
})(Task)

// class Task2 extends React.Component {
//   render() {
//     return (
//       <div className="task-box">
//         <header className="head-box">
//           <h2 className="title">TASK OA 任务管理系统</h2>
//           <Button type="primary" onClick={() => setVisible(true)}>新增任务</Button>
//         </header>

//         <section className="tag-box">
//           {['全部', '未完成', '已完成'].map((item, index) => {
//             return <Tag key={index}
//               color={index === selected ? '#108ee9' : ''}
//               onClick={changeTab.bind(null, index)}>
//               {item}
//             </Tag>;
//           })}
//         </section>

//         <Table columns={columns} dataSource={data} loading={loading} pagination={false} rowKey="id" />

//         {/* 新增任务弹框 */}
//         <Modal confirmLoading={confirmLoading} keyboard={false} maskClosable={false} okText="提交信息" title="新增任务窗口" visible={visible} onOk={submit} onCancel={cancel}>
//           <Form layout="vertical" form={formObj} initialValues={{ task: '', time: '' }}>
//             <Form.Item label="任务描述" name="task" validateTrigger="onBlur" rules={[
//               { required: true, message: '任务描述是必填项!' },
//               { pattern: /^[\w\W]{6,}$/, message: '输入的内容不少于6位!' }
//             ]}>
//               <TextArea rows={4} />
//             </Form.Item>
//             <Form.Item label="任务预期完成时间" name="time" validateTrigger="onBlur" rules={[{ required: true, message: '完成时间是必填项!' }]}>
//               <DatePicker showTime />
//             </Form.Item>
//           </Form>
//         </Modal>
//       </div >
//     )
//   }
// }

export default Task