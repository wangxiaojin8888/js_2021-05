import * as TYPE from '../action-types';
const voteAction = {
    support() {
        // 方法执行返回的是dispatch派发需要的action对象
        return {
            type: TYPE.VOTE_SUP
        };
    },
    oppose() {
        return {
            type: TYPE.VOTE_OPP
        };
    }
};
export default voteAction;