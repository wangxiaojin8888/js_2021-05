import * as TYPE from '../action-types';

const initial = {
    supNum: 10,
    oppNum: 5
};
export default function voteReducer(state = initial, action) {
    state = { ...state };
    switch (action.type) {
        case TYPE.VOTE_SUP:
            state.supNum++;
            break;
        case TYPE.VOTE_OPP:
            state.oppNum++;
            break;
    }
    return state;
};