/* 
合并各个版块的REDUCER
  合并后的公共状态:按照设定的模块名分别管理公共状态「避免各版块间状态冲突」
  state = {
      vote:{
          supNum:10,
          oppNum:5
      },
      task:{
          taskList:null
      }
  }
*/
import { combineReducers } from 'redux';
import voteReducer from './voteReducer';
import taskReducer from './taskReducer';

export default combineReducers({
    vote: voteReducer,
    task: taskReducer
});