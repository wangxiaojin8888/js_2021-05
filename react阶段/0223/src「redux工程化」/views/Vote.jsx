import React, { Component } from 'react';
import { TeamOutlined } from '@ant-design/icons';
import VoteMain from './VoteMain';
import VoteFoot from './VoteFoot';
import './Vote.less';
import Theme from '../store/ThemeContext';

export default class Vote extends Component {
    static contextType = Theme;
    render() {
        const { store } = this.context;
        let { supNum, oppNum } = store.getState().vote;
        return <div className="vote-box">
            <div className="head">
                <h3 className="title">good good study,day day up!</h3>
                <div className="count">
                    <TeamOutlined />
                    {supNum + oppNum}
                </div>
            </div>
            <VoteMain />
            <VoteFoot />
        </div>;
    }

    componentDidMount() {
        const { store } = this.context;
        store.subscribe(() => {
            this.forceUpdate();
        });
    }
};