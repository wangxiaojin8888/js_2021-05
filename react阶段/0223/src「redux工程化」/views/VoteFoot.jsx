import React, { Component } from "react";
import { Button } from 'antd';
import Theme from "../store/ThemeContext";
import actions from '../store/actions';

export default class VoteFoot extends Component {
    static contextType = Theme;
    render() {
        const { store } = this.context;
        return <div className="foot">
            <Button type="primary" onClick={() => {
                store.dispatch(actions.vote.support());
            }}>支持</Button>

            <Button danger onClick={() => {
                store.dispatch(actions.vote.oppose());
            }}>反对</Button>
        </div>;
    }
};