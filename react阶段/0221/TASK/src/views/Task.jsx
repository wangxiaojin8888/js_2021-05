import React, { useState, useEffect } from "react";
import { Button, Tag, Table, Modal, Form, Input, DatePicker, message, Popconfirm } from 'antd';
import { formatTime } from '../assets/utils';
import api from '../api';
import './Task.less';
const { TextArea } = Input;

export default function Task() {
    /* 表格列 & 数据 & Loading */
    const columns = [{
        title: '编号',
        dataIndex: 'id',
        align: 'center',
        width: '8%'
    }, {
        title: '任务描述',
        dataIndex: 'task',
        width: '50%'
    }, {
        title: '状态',
        dataIndex: 'state',
        align: 'center',
        width: '10%',
        render: text => +text === 1 ? '未完成' : '已完成'
    }, {
        title: '完成时间',
        dataIndex: 'time',
        align: 'center',
        width: '15%',
        render: (_, record) => {
            let { state, time, complete } = record;
            if (+state === 2) time = complete;
            return formatTime(time, '{1}-{2} {3}:{4}');
        }
    }, {
        title: '操作',
        render: (_, record) => {
            let { id, state } = record;
            return <>
                <Popconfirm title="您确定要删除此任务吗?"
                    onConfirm={handleDelete.bind(null, id)}>
                    <Button type="link">删除</Button>
                </Popconfirm>

                {+state !== 2 ? <Popconfirm title="您确定要修改此任务吗?"
                    onConfirm={handleUpdate.bind(null, id)}>
                    <Button type="link">完成</Button>
                </Popconfirm> : null}
            </>;
        }
    }];
    let [data, setData] = useState([]),
        [loading, setLoading] = useState(false),
        [selected, setSelected] = useState(0);
    // 向服务器发送请求,获取当前选中状态的任务,渲染到Table中
    const query = async () => {
        setLoading(true);
        let { code, list } = await api.queryList(selected);
        if (+code !== 0) list = [];
        setData(list);
        setLoading(false);
    };
    // 页卡切换
    const changeTab = index => {
        if (selected === index) return;
        // 合成事件绑定中,setSelected方法执行是异步的,所以在选中状态还没更新的情况下,我们就发送数据请求,此时获取的还是上次选中状态的结果...
        setSelected(index);
    };
    useEffect(query, [selected]);

    /* 新增窗口 */
    let [confirmLoading, setConfirmLoading] = useState(false),
        [visible, setVisible] = useState(false),
        [formObj] = Form.useForm();
    // 确定提交
    const submit = async () => {
        try {
            // 经过表单校验,获取到其收集的信息
            let { task, time } = await formObj.validateFields();
            time = time.format('YYYY-MM-DD HH:mm:ss');
            // 向服务器发送请求:新增成功需要关闭窗口&重新获取数据
            setConfirmLoading(true);
            let { code } = await api.addTask({ task, time });
            if (+code !== 0) {
                message.error('新增任务失败,请稍后再试~');
                return;
            }
            message.success('恭喜您,新增任务成功!');
            cancel();
            query();
            setConfirmLoading(false);
        } catch (_) {
            setConfirmLoading(false);
        }
    };
    // 取消按钮
    const cancel = () => {
        setVisible(false);
        formObj.resetFields();
    };

    /* 删除和完成 */
    const handleDelete = async id => {
        try {
            let { code } = await api.removeTask(id);
            if (+code !== 0) {
                message.error('很遗憾,删除失败了!');
                return;
            }
            message.success('恭喜您,删除成功了!');
            query();
        } catch (_) { }
    };
    const handleUpdate = async id => {
        try {
            let { code } = await api.completeTask(id);
            if (+code !== 0) {
                message.error('很遗憾,修改失败了!');
                return;
            }
            message.success('恭喜您,修改成功了!');
            query();
        } catch (_) { }
    };

    return <div className="task-box">
        <header className="head-box">
            <h2 className="title">TASK OA 任务管理系统</h2>
            <Button type="primary" onClick={() => setVisible(true)}>新增任务</Button>
        </header>

        <section className="tag-box">
            {['全部', '未完成', '已完成'].map((item, index) => {
                return <Tag key={index}
                    color={index === selected ? '#108ee9' : ''}
                    onClick={changeTab.bind(null, index)}>
                    {item}
                </Tag>;
            })}
        </section>

        <Table columns={columns} dataSource={data} loading={loading} pagination={false} rowKey="id" />

        {/* 新增任务弹框 */}
        <Modal confirmLoading={confirmLoading} keyboard={false} maskClosable={false} okText="提交信息" title="新增任务窗口" visible={visible} onOk={submit} onCancel={cancel}>
            <Form layout="vertical" form={formObj} initialValues={{ task: '', time: '' }}>
                <Form.Item label="任务描述" name="task" validateTrigger="onBlur" rules={[
                    { required: true, message: '任务描述是必填项!' },
                    { pattern: /^[\w\W]{6,}$/, message: '输入的内容不少于6位!' }
                ]}>
                    <TextArea rows={4} />
                </Form.Item>
                <Form.Item label="任务预期完成时间" name="time" validateTrigger="onBlur" rules={[{ required: true, message: '完成时间是必填项!' }]}>
                    <DatePicker showTime />
                </Form.Item>
            </Form>
        </Modal>
    </div >;
};