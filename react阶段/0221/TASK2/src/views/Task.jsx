import React from "react";
import { Button, Tag, Table, Modal, Form, Input, DatePicker, message, Popconfirm } from 'antd';
import { formatTime } from '../assets/utils';
import api from '../api';
import './Task.less';

export default class Task extends React.Component {
    // 定义表格列的数据
    columns = [{
        title: '编号',
        dataIndex: 'id',
        align: 'center',
        width: '8%'
    }, {
        title: '任务描述',
        dataIndex: 'task',
        width: '50%'
    }, {
        title: '状态',
        dataIndex: 'state',
        align: 'center',
        width: '10%',
        render: text => +text === 1 ? '未完成' : '已完成'
    }, {
        title: '完成时间',
        dataIndex: 'time',
        align: 'center',
        width: '15%',
        render: (_, record) => {
            let { state, time, complete } = record;
            if (+state === 2) time = complete;
            return formatTime(time, '{1}-{2} {3}:{4}');
        }
    }, {
        title: '操作',
        render: (_, record) => {
            let { id, state } = record;
            return <>
                <Popconfirm title="您确定要删除此任务吗?"
                    onConfirm={this.handle.bind(null, 'remove', id)}>
                    <Button type="link">删除</Button>
                </Popconfirm>
                {+state !== 2 ? <Popconfirm title="您确定要修改此任务吗?"
                    onConfirm={this.handle.bind(null, 'update', id)}>
                    <Button type="link">完成</Button>
                </Popconfirm> : null}
            </>;
        }
    }];

    // 基于REF获取FORM组件实例
    form = React.createRef();

    // 初始化状态
    state = {
        data: [],
        loading: false,
        selected: 0,
        confirmLoading: false,
        visible: false
    };

    // 页卡切换
    change = index => {
        let { selected } = this.state;
        if (index === selected) return;
        this.setState({
            selected: index
        }, this.query);
    };

    // 获取指定状态数据
    query = async () => {
        this.setState({ loading: true });
        let { code, list } = await api.queryList(this.state.selected);
        if (+code !== 0) list = [];
        this.setState({
            data: list,
            loading: false
        });
    };

    // 取消弹框
    cancel = () => {
        this.form.current.resetFields();
        this.setState({
            confirmLoading: false,
            visible: false
        });
    };

    // 提交新任务
    submit = async () => {
        try {
            let { task, time } = await this.form.current.validateFields();
            this.setState({ confirmLoading: true });
            let { code } = await api.addTask({ task, time });
            this.setState({ confirmLoading: false });
            if (+code !== 0) {
                message.error('很遗憾,新增任务失败!');
                return;
            }
            message.success('恭喜您,新增任务成功!');
            this.cancel();
            this.query();
        } catch (_) {
            this.setState({ confirmLoading: false });
        }
    };

    // 删除&完成
    handle = async (type, id) => {
        let tip = '修改',
            func = api.completeTask;
        if (type === 'remove') {
            tip = '删除';
            func = api.removeTask;
        }
        try {
            let { code } = await func(id);
            if (+code !== 0) {
                message.error(`很遗憾,任务${tip}失败!`);
                return;
            }
            message.success(`恭喜你,任务${tip}成功!`);
            this.query();
        } catch (_) { }
    };

    componentDidMount() {
        // 第一次渲染完,从服务器获取全部任务
        this.query();
    }

    render() {
        let { data, loading, confirmLoading, visible, selected } = this.state;

        return <div className="task-box">
            <header className="head-box">
                <h2 className="title">TASK OA 任务管理系统</h2>
                <Button type="primary"
                    onClick={() => {
                        this.setState({ visible: true });
                    }}>
                    新增任务
                </Button>
            </header>

            <section className="tag-box">
                {['全部', '未完成', '已完成'].map((item, index) => {
                    return <Tag key={index}
                        color={index === selected ? '#108ee9' : ''}
                        onClick={this.change.bind(null, index)}>
                        {item}
                    </Tag>;
                })}
            </section>

            <Table pagination={false} rowKey="id"
                columns={this.columns}
                dataSource={data}
                loading={loading} />

            <Modal keyboard={false} maskClosable={false} okText="提交信息" title="新增任务窗口"
                confirmLoading={confirmLoading}
                visible={visible}
                onCancel={this.cancel}
                onOk={this.submit}>
                <Form layout="vertical"
                    initialValues={{ task: '', time: '' }}
                    ref={this.form}>
                    <Form.Item label="任务描述" name="task" validateTrigger="onBlur"
                        rules={[{ required: true, pattern: /^[\w\W]{6,}$/, message: '内容为必填且不少于6位!' }]}>
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    <Form.Item label="任务预期完成时间" name="time" validateTrigger="onBlur"
                        rules={[{ required: true, message: '完成时间是必填项!' }]}>
                        <DatePicker showTime />
                    </Form.Item>
                </Form>
            </Modal>
        </div>;
    }
};