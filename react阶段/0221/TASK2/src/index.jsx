import React from 'react';
import ReactDOM from 'react-dom';
import Task from './views/Task';
/* ANTD */
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import './assets/reset.min.css';
import 'antd/dist/antd.css';

// import './fetchDemo';

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Task />
  </ConfigProvider>,
  document.getElementById('root')
);