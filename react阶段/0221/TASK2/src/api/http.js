/*
 对FETCH进行封装,让其用起来更简单一些
   + 一些配置项的封装处理，例如：支持params实现URL问号传参
   + 针对请求主体的格式进行处理：如果用户传递的是普通对象,我们按照服务器要求把其变为指定格式的字符串,并且设置Content-Type
   + 类似于AXIOS的请求拦截器,把业务中需要的一些统一传递给服务器的内容(如Token)在封装中处理好
   + 类似于AXIOS的响应拦截器,请求成功返回响应主体信息(可以自定义格式),请求失败对失败原因进行统一的提示
   + ...

 http([url],[config])
   + method:'GET'
   + params:null 使用时候传递普通对象
   + body:null   设置的请求主体信息:传递的是普通对象,我们把其变为URLENCODED格式,设置Content-Type
   + signal:null 请求中断的信号器
   + responseType:'json' 预设服务器返回结果的转换格式json/text/formData/buffer/blob
   + headers:null  可以传递普通对象或者Headers实例对象
   ----
   .then(value=>{ 请求成功获取的响应主体信息 })
   .catch(reason=>{ 经过了失败的统一处理提示 })

 http.get/delete/head([url],[config])
 http.post/put([url],[body],[config])
 */
import { isPlainObject } from '../assets/utils';
import qs from 'qs';
import { message } from 'antd';

/* 工具类方法 */
// 迭代对象中的每一项
const eachObj = (obj, callback) => {
    if (!isPlainObject(obj)) return;
    if (typeof callback !== "function") return;
    let keys = Reflect.ownKeys(obj);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i],
            value = obj[key];
        let res = callback(value, key);
        if (res === false) break;
    }
};

/* 核心处理 */
const http = function http(url, config) {
    // 格式校验 & 配置项合并
    if (typeof url !== "string") throw new TypeError("请求的URL地址书写错误!");
    if (!isPlainObject(config)) config = {};
    let { method, params, body, signal, responseType, headers } = Object.assign({
        method: 'GET',
        params: null,
        body: null,
        signal: null,
        responseType: 'json',
        headers: null
    }, config);
    method = method.toUpperCase();

    // 针对HEADERS的特殊处理:最后都要变为Headers类的实例
    if (headers === null) {
        headers = new Headers();
    } else if (isPlainObject(headers)) {
        let temp = new Headers();
        eachObj(headers, (value, key) => {
            temp.append(key, value);
        });
        headers = temp;
    }

    // 针对于PARAMS的处理:把其拼接到URL末尾
    if (isPlainObject(params)) {
        params = qs.stringify(params);
        url += `${url.includes('?') ? '&' : '?'}${params}`;
    }

    // 针对于BODY请求主体的处理:根据当前项目需求,我们要把它变为URLENCODED格式字符串
    if (isPlainObject(body)) {
        body = qs.stringify(body);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }

    // 类似于AXIOS中的请求拦截器:根据当前项目需求
    const token = localStorage.getItem('token');
    if (token) headers.append('authorzation', token);

    // 开始发送请求
    config = {
        method,
        headers,
        signal,
        cache: 'no-cache',
        credentials: 'include'
    };
    if (/^(POST|PUT|PATCH)$/i.test(method)) config.body = body;
    return fetch(url, config).then(response => {
        let { status, statusText } = response;
        if (status >= 200 && status < 400) {
            // 请求成功:按照responseType转换为想要的格式
            let result;
            switch (responseType.toLowerCase()) {
                case 'text':
                    result = response.text();
                    break;
                case 'formdata':
                    result = response.formData();
                    break;
                case 'buffer':
                    result = response.arrayBuffer();
                    break;
                case 'blob':
                    result = response.blob();
                    break;
                default:
                    result = response.json();
            }
            return result.catch(err => Promise.reject({ code: 30, message: err.message }));
        }
        return Promise.reject({ code: 10, status, statusText });
    }).catch(reason => {
        // 失败的统一处理:类似于AXIOS中的响应拦截器
        let code = reason?.code;
        switch (code) {
            case 10:
                message.error(`服务器返回的HTTP状态码错误:${reason.status}`);
                break;
            case 20:
                message.error('当前请求被中断!');
                break;
            case 30:
                message.error(`无法把服务器返回的信息正常转换为${responseType}格式的数据!`);
                break;
            default:
                message.error('当前网络繁忙,请稍后再试~');
        }
        return Promise.reject(reason);
    });
};

/* 快捷应用 */
['GET', 'DELETE', 'HEAD'].forEach(name => {
    http[name.toLowerCase()] = function (url, config) {
        if (!isPlainObject(config)) config = {};
        config.method = name;
        return http(url, config);
    };
});
['POST', 'PUT'].forEach(name => {
    http[name.toLowerCase()] = function (url, data, config) {
        if (!isPlainObject(config)) config = {};
        config.method = name;
        config.body = data;
        return http(url, config);
    };
});

export default http;