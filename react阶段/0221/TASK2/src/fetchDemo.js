/* 
实现前后端数据通信的方案：
   @1 XMLHttpRequest 「最常用、最经典」
     + ajax
     + JQ: $.ajax() 基于回调函数方式实现对ajax的封装
     + axios 基于Promise实现对ajax的封装
     + ...
   @2 跨域请求处理
     + JSONP
     + postMessage
     + ...
   @3 webscoket实现客户端和服务器端“实时”通信
   @4 fetch
   ......

fetch是ES6中内置的API，它基于不同于XMLHttpRequest的方式，实现客户端和服务器之间的数据通信
  + 浏览器内置API，不需要像axios一样安装第三方插件
  + 本身请求也是基于Promise管理，请求结果是一个promise实例...
  + ...

fetch([url],[config])
  + [url]是请求的地址：fetch不具备axios中params的效果(把params对象中的每一项，动态拼接到URL末尾)；如果需要问号传参，则需要自己把信息，“手动”拼接到URL的末尾！！
  + [config]
    + method:'GET' 请求方式
    + cache:'default' 是否应用缓存,一般设置为'no-cache'
    + credentials:'same-origin' 是否允许携带资源凭证,一般设置为'include'{跨域和同源都可以携带}
    + headers:普通对象/Headers实例对象  设置请求头信息{注意:在POST系列请求中,请求主体传递不同的数据格式给服务器,此时我们需要设置Content-Type请求头,来标注传递数据格式的MIME类型}
      + JSON字符串  Content-Type:'application/json'
      + URLENCODED字符串  Content-Type:'application/x-www-form-urlencoded'
      + 普通字符串  Content-Type:'text/plain'
      + Form-Data格式对象  Content-Type:'multipart/form-data'
      + ...
    + body 设置请求主体信息(格式限制),但是仅限POST系列请求,GET系列请求设置它会报错
    + ...
  + fetch默认不支持 设置请求超时时间 & 中断请求 & 监听上传进度 等操作的:fetch的相关机制是不如XMLHttpRequest完善的！！
  + fetch不兼容IE！！

目前有个草案阶段的API可以实现fetch请求的中断！！
  AbortController
 */

let control = new AbortController();

fetch('/static/js/bundle.js', {
    signal: control.signal
}).then(response => {
    /*
     response是Response类的实例对象
       + status/statusText 服务器返回的HTTP状态码及其描述
       + headers 存储响应头信息「它是Headers类的实例，如果想获取某个响应头信息，需要基于Headers类提供的相关方法处理」
       + body 存储响应主体信息「它并没有直接存储服务器返回的响应主体信息，而是存储着一个“可读流”对象(ReadableStream类的实例)，如果想获取我们想要的格式，需要基于Response.prototype上提供的方法去读取出来，例如：response.json()就是把可读流转换为JSON对象！」
       + ...
       -----
       + arrayBuffer/blob/formData/json/text...
       执行这些方法会返回新的promise实例：在把可读流转换为我们想要数据格式的时候，不一定能成功转换(例如服务器返回的是xml格式内容，我们执行json方法，想要把其转换为JSON对象，结果一定是失败的)
     
     fetch有一点不同于axios，axios是服务器响应状态码以2开始才算请求成功(可以自己配置)，但是fetch不是，fetch是：只要服务器有返回信息，不论状态吗是多少，都认为是请求成功，返回的promise实例都是fulfilled状态！！
     */
    let { headers, status, statusText } = response;
    if (status >= 200 && status < 400) {
        // 真正的成功
        return response.json().catch(err => {
            return Promise.reject({
                code: 30,
                message: err.message
            });
        });
    }
    // 请求失败
    return Promise.reject({
        code: 10,
        status,
        statusText
    });
}).then(value => {
    // 从服务器获取数据成功 & 成功把响应主体信息转换为JSON对象
    console.log('成功:', value);
}).catch(reason => {
    // 各种失败的情况:
    // + 没有和服务器通信成功,服务器没有响应任何内容(例如：断网、超时、中断...)
    //   + 中断请求 {code:20,meaasge:'The user aborted a request'}
    //   + 断网 其余知道情况的错误处理完，剩下的都可以统一归结于断网
    // + 服务器有响应，但是HTTP状态码不是我们想要的 {code:10,status,statusText}
    // + 把服务器返回可读流转换为想要的格式，但是转换失败了!! {code:30,message}
    console.dir(reason);
});

// control.abort();

/*
 Headers内置构造函数：基于它可以处理请求头和响应头
   Headers.prototype
   + append
   + delete
   + forEach
   + get
   + has
   + ...
 */
/* 
// 请求头处理：可以基于请求头对象管理，也可以使用普通对象处理
let headersRequest = new Headers();
headersRequest.append('Content-Type', 'application/json');
headersRequest.append('Token', 'xxx');
fetch('/api/xxx', {
    // headers: headersRequest
    headers: {
        'Content-Type': 'application/json',
        'Token': 'xxx'
    }
}); 
*/