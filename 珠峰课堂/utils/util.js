const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

function http(option){
  let baseURL = 'http://localhost:3000'
  let {url,method='get',data={},header={}} = option;
  return new Promise((res,rej)=>{
    wx.request({
      url: baseURL +url,
      method,
      data,
      header,
      success(data){
        res(data.data)
      },
      fail(err){
        rej(err)
      }
    })
  })
}
http.get = function(url,data){
  return http({
    method:'get',
    url:url,
    data:data
  })
}
http.post = function(url,data){
  return http({
    method:'post',
    url:url,
    data:data
  })
}

module.exports = {
  formatTime,
  http
}
