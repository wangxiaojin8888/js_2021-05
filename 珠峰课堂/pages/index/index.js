// index.js
// 获取应用实例
const app = getApp()
let {http} = require('../../utils/util')

Page({
  data: {
    background: [],
    indicatorDots: true,
    autoplay: true,
    interval: 2000,
    duration: 500,
    data1:{
      //  第一个视频框框的数据
      bg:"http://www.zhufengpeixun.cn/skin/20142/img/zfBg2.jpg",
      v_src:'http://img.zhufengpeixun.cn/zfcctv.mp4'
    },
    data2:{
      //  第一个视频框框的数据
      bg:"http://www.zhufengpeixun.cn/skin/20142/img/zfBg.jpg",
      v_src:'http://img.zhufengpeixun.cn/zf10.mp4'
    },
    list:[]
  },
  // 事件处理函数

  onLoad() {
    this.getBannerList()
    this.getPublic()
  },
  getBannerList(){
    http.get('/banner').then(data=>{
      console.log(data.data)
      // this.background = data.data
      this.setData({
        background:data.data
      })
    })
  },
  getPublic(){
    let arr = [
      "http://www.zhufengpeixun.cn/gongkaike/2021-09-22/1394.html",
      "http://www.zhufengpeixun.cn/gongkaike/2021-08-27/1393.html",
      "http://www.zhufengpeixun.cn/gongkaike/2021-08-13/1392.html",
      "http://www.zhufengpeixun.cn/gongkaike/2021-08-07/1391.html"
    ]
    http.get('/publicList').then(data=>{
      console.log(data)
      this.setData({
        list:data.data.map((item,index)=>{
          return {
            ...item,
            url:arr[index]
          }
        })
      })
    })
  },
  fn(e){
    // console.log(e.currentTarget.dataset.url)
    let url = e.currentTarget.dataset.url;
    app.webUrl = url;
    wx.navigateTo({
      url: '../web-view/webview?url='+url,
    })
  }

})
