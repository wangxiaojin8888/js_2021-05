// components/home1/home1.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title:{
      type:String
    },
    data:{
      type:Object
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShowVideo:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    showVideo(){
      // console.log("弹框展示")
      this.setData({
        isShowVideo:true
      })
    },
    hide(){
      this.setData({
        isShowVideo:false
      })
    }
  }
})
