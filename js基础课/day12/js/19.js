/* 
变量提升：

var a----12
var b---12-----13
function fn----AAAFFF111
*/

console.log(a,b);// undefined undefined
var a=12,
b=12;
function fn(){
    /* 
     私有执行上下文：
     变量提升：
        var a--------私有的----13
    */
    console.log(a,b); // undefined  12
    var a=b=13;
    console.log(a,b);// 13 13
}
fn();
console.log(a,b);//12 13 
