/* 
 同一个变量只声明一次，但是可以多次赋值
 声明num-------undefined---2--3

*/

// var num=2;
// console.log(num);
// var num=3;
// console.log(num);


/* 
 变量提升：

  fn: ---------1----2

*/
fn();
function fn(){
    console.log(1);
}
function fn(){
    console.log(2);
}
function fn(){
    console.log(3);
}
fn();