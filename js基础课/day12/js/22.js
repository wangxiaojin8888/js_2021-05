//-----es6 中没有变量提升

// console.log(a);
// let a=3;


//-----------es6 阻断了与window的关系
// var a=3;
// console.log(window.a);
// "a" in window;// true

// let b=100;
// "b" in window;// false

//-------同一个作用域不能重复声明： Identifier 'a' has already been declared
// let  a=3;
// let a=100;


// let a=3;

// {
//     let a=100;
//     console.log(a);
// }
// console.log(a);


//----------暂时性死区：


// console.log(a);
// let a=3;


// console.log(typeof b); // undefined

// let b=3;


//--- const 不能修改值，并且得有初始值
//const c;
//c=500;

// let a;
// a=100;
// c=300;

/* 
 在代码自上而下执行之前，虽然没有变量提升，但是有一个自我的词法检测机制，

*/


let a=3;
console.log(a);
let a=10;
console.log(a);