/* 
全局作用域：
 f 
 g

*/

f=function(){
    return true;
};
g=function(){
    return false;
};
~function(){
    /* 
     私有执行上下文：
        变量提升： g ------f true 私有的
    */
 if(g()&&[]==![]){//  []==false  Number([])==Number(false)  0==0 true
     f=function(){return false;}; // 把全局下的f 改值了
     function g(){
         return true;
     }
 }
}();
console.log(f());
console.log(g());