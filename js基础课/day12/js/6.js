/* 
变量提升：
  function fn  -----undefined----function fn(){console.log("ok")}

*/

console.log(fn); // undefined
if(1==1){
   console.log(fn); // 函数fn
   function fn(){
       console.log("ok");
   }
}
console.log(fn);// 函数fn