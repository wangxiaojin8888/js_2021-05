/* 

全局：
a=10
b=10
fn----AAAFFF111
*/
let a=10,
b=10;
let fn=function(){
    /* 
    私有执行上下文
    
    */
    //console.log(a); //
    let a=b=20;//  a 是私有的
    console.log(a,b);//20 20
};
fn();
console.log(a,b)
