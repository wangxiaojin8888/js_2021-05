/* 
变量提升阶段：
fn: 1---2--3---4----100
代码自上而下运行：



*/
fn();
function fn(){
    console.log(1);
}
function fn(){
    console.log(2);   
}
fn();
function fn(){
    console.log(3);
}
fn();
fn=100;//----------
function fn(){
    console.log(4);   
}
fn();// 100();