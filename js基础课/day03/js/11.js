// 四、在js中用来检测数据类型的四种方式
//  typeof
//  instanceof
//  constructor
//  Object.prototype.toString.call()
/* 
typeof 数据
作用：检测数据类型
返回结果首先就是字符串，类型：
    + "number"
    + "string"
    + "boolean"
    + "undefined"
    + "symbol"
    + "bigint"
    + "object" -----null 
    + "function"


*/

typeof 1
'number'
typeof "1"
'string'
typeof false
'boolean'
typeof null
'object'
typeof undefined
'undefined'
typeof Symbol()
'symbol'
typeof 11111n
'bigint'
typeof []
'object'
typeof {}
'object'
typeof /\d/
'object'
typeof function(){}
'function'