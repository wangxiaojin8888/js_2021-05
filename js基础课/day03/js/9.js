/* 
● continue：  结束本次循环，继续下一轮循环
● break：       强制结束整个循环
*/

for(var i=0;i<10;i++){// i=0 2 4 6 8
    if(i<5){
       i++;//i:1 3 5
       continue;
    }
    if(i>7){
       i+=2; //i:10
       break;
    }
    i+=1;//7
}
console.log(i);