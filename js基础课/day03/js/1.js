var obj={"name1":"lili","age":18};
var obj={name1:"lili",age:18,0:100};

obj.name1;
obj["name1"];//"lili"
obj[name1]; // name1 变量，但是没有此变量，报错
//---
var name1="zhufeng"
obj[name1]; //  undefined
//---
var name1="name1"
obj[name1]; //  "lili"

//obj.0;// 报错
obj[0];
obj["0"];

//--------修改和添加
obj["job"]="全栈开发工程师";
obj["job"]="aa";

//--------删除
obj[0]=null;
delete obj[0];

