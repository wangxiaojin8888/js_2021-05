//-------------------1

// var flag=3;

// if(flag==1){
//    console.log("放天假");
// }else if(flag==0){
//    console.log("看电影") 
// }else if(flag==2){
//     console.log("好好学习")
// }else{
//     console.log("运动")
// }

//----------2


var flag=2;

// if(flag>0){
//    console.log(1)
// }

// if(flag>1){
//     console.log(2)
//  }

//------只要满足一个条件，不会再走入另一个了
// if(flag>0){
//     console.log(1)
// }else if(flag>1){
//     console.log(2)
// }else{
//     console.log(3)
// }

//---------逻辑运算符：
//&& 表示并且，左右两边的条件必须同时满足
//||   表示或，左右两边的条件只要满足其一即可


var flag=2;
var open=0;

// if(flag>0&&open>0){
//     console.log("1");
// }

// if(flag>0||open>0){
//     console.log("2");
// }

//------条件最终都是转为布尔
// if(0){
//     console.log("1")
// }
// if(1){
//     console.log("2")
// }
//0 &&open>2  0
// if(flag&&open>2){

// }
/* ---用且进行赋值的时候，先看第一个值转为布尔如果是true 就等于第二个值 
      如果第一个值转为布尔是false，就是第一个值

      用或进行赋值的时候，先看第一个值转为布尔，如果是true,就是第一个值，如果第一个值转为
      布尔是false,就是第二个值
  */

      var a=1&&2;// 2
      var a=0&&1; //0

      var b=1||2;// 1
      var b=0||1;// 1

            //2&&3=3  3&&0
      var c=1&&2&&3&&0;


    var aa;
    
    var bb=aa||"默认值"


