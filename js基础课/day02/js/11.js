
var a=1;
var flag=2;
// if(flag>0){
//    a=a+1;//1+1  a=2
 //     console.log(2);
// }else{
//    a=a-1;
// }
/* 
  三元运算
 语法：条件？成立执行的语句:条件不成立的语句
      如果有多条语句，需要用小括号包起来，并且语句和语句之间用逗号分隔
      如果只有一个if，改成三元的时候，可以用下面的任意一个占位：
       + void 0
       + null
       + undefined

*/
a=flag>0?(a+1,console.log(2)):a-1;


// var a=3;
// if(flag>0){
//   a+=1;// a=a+1;
// }

//------占位；
flag>0?a+=1:void 0;
flag>0?a+=1:null;
flag>0?a+=1:undefined;