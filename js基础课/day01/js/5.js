//-----------数字
var a=0;
var a=5;
var b=-1;
var c=1.1;
// NaN :not a number   它虽然不是一个有效数字，但是属于数字类型


//---------字符串

var name="文爽";
var name='文爽';
var name='蓝青';
var str="1";  // 字符串

//-------布尔
var flag=true;
var flag=false;

// --- null undefined  空

//--symbol (唯一的)

Symbol("1")==Symbol("1");// false
"1"=="1" ;// true

//----bigInt
// js 中关于数字有一个安全范围：
Number.MAX_SAFE_INTEGER;//9007199254740991
Number.MIN_SAFE_INTEGER;//-9007199254740991
console.log(90071992547409911);//90071992547409900
console.log(90071992547409911n);
//----------引用数据类型
//-------对象
// 普通对象
var people={
    name:"汉卿",
    age:22,
    job:"web 前端工程师"
}
//----数组
var ary=["people1","people2","people3"];
var ary=[];// 空数组
var ary=[1,2,3];

// ---正则
/\d/g

//---时间对象
new Date()
//Mon Nov 08 2021 15:38:39 GMT+0800 (中国标准时间)
new Date().getFullYear();//2021
new Date().getMonth();//10
new Date().getDate();//

// ----Math 数学对象
Math.random();//[0-1) 之间的随机小数

//---------函数


function fn(){
    console.log(1);
}


//------

NaN;
"1";
true;
false;
null;
undefined;
var a=Symbol();
22222n;
{};
[];

function fn(){};
