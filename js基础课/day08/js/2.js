// 【时间字符串处理】把下面的字符串变成 "2019年08月18日 12时32分18秒"
var str="2019-11-18 12:32:18";

var res=str.split(" ");//["2019-8-18","12:32:18"]
//res[0]:"2019-8-18"
var strLeft=res[0].split("-");//["2019","8","18"]
var strRight=res[1].split(":");//["12","32","18"]

var result=strLeft[0]+"年"+addZero(strLeft[1])+"月"+addZero(strLeft[2])+"日"+" "+addZero(strRight[0])+"时"+addZero(strRight[1])+"分"+addZero(strRight[2])+"秒";



// function addZero(n){
//     if(n<10){
//        return "0"+n;
//     }else{
//         return n
//     }

// }

//---
// function addZero(n){
//     if(n<10){
//        return "0"+n;
//     }
//     return n
// }

function addZero(n){
   return n<10?"0"+n:n;
}

