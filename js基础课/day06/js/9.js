/* 
ary.splice(n,m,x1,x2..)
作用：从索引n开始删除m项，并且把删除的项用x1，x2代替

*/

var ary=[0,1,2,3,4,5,6];
var res=ary.splice(0,2,7,8);//[7,8,2,3,4,5,6]

//---------删除ary.splice(n,m);从索引n开始删除m项
// 改变原数组，返回值是数组，里面是删除的每一项

var ary=[0,1,2,3,4,5,6];
var res=ary.splice(0,3);//[3, 4, 5, 6];
ary.splice(0,0);// 返回值是一个空数组

//----新增ary.splice(n,0,x1,x2) 把x1 和x2添加到n的前面
var ary=[0,1,2,3,4,5,6];
var res=ary.splice(0,0,66,77);//  ary:[66, 77, 0, 1, 2, 3, 4, 5, 6]


//----------删除数组中的最后一项
var  ary=[0,1,2,3];
//---1   ary.pop();
//---2   ary.splice(ary.length-1,1)
//---3   ary.length--
//---4   ary.splice(-1,1)

//--------往数组的后面添加一项
var  ary=[0,1,2,3];
//----1 ary.push(6);
//----2 ary.splice(ary.length,0,添加的项)----ary.splice(ary.length,1,添加的项)
//---- 3 ary[ary.length]=100
