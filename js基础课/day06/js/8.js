var ary=[0,1,2,3,4];
/* 
  作用：从索引n开始复制到索引m结束（但是不包含m这一项）
  ary.slice(n,m)
  ary.slice(n) 代表的含义：从索引n开始复制到最后
*/
var res1=ary.slice(0,3);//[0,1,2]
var res2=ary.slice(1);//[1,2,3,4]

var res3=ary.slice(0);//[0,1,2,3,4]
var res3=ary.slice();//[0,1,2,3,4]

//-----slice 的索引可以是负数， 最后一项的索引就是-1
//  字符的长度+负数索引=正数
var res5=ary.slice(2,4);//[2,3]
var res4=ary.slice(-3,-1);//[2,3]
var res4=ary.slice(-3);//[2,3,4]

