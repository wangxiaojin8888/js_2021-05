// --------------- 函数的定义
// function total(){
//     console.log(1+1);
// }

// 调用函数,每次函数执行的时候，都会重新开辟一个新的栈内存
// total();
// total();


//---------接收实参的两种形式：1、形参 2、arguments

// function total(x,y){
//     //console.log(x,y);
//     console.log(arguments[0]);

// }
// total(1,2,3,4,5)

//------------return  ：
//1:让函数有一个返回值，如果说不写 或者return 后面啥都不跟，默认就是undefined
// 2：return 下面的代码不再运行  （用来阻断函数的）

// function total(x,y){
//    x+y;
//    console.log("2222");
//    return ;
//    console.log("1111")
// }
// var res=total(1,2);


var  x=2;
var b=200;
function total(x,y){
   // var x=100;
    console.log(x);
   // return a;
}
//console.log(x);
//console.log(a);//在外面的话，拿不到函数里面的东西，想拿的化，可以通过return
// 1、函数里面有形参，但是在调用的时候没传，形参默认的值就是undefined
// 2、声明了一个变量，没赋值，默认也是undefined
// 3、函数没写返回值 函数执行的结果也是undefined
// 4、获取对象的属性名对应的属性值的时候，如果没有此属性，默认也是undefined