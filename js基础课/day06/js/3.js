/* 
  arguments.callee 指的是函数本身
  arguments.callee.caller 指的是函数执行时候的宿主环境，如果在全局执行，就是null,如果在其它函数里面执行了，那就是其它函数

*/

// function fn(){
    
//     //console.log(arguments.callee);//  代表的是函数本身
//     console.log(arguments.callee.caller);//null
 
// }
// function B(){
//    fn();
// }
// B();


"use strict";
//Uncaught TypeError: 'caller', 'callee', and 'arguments' properties may not be accessed on strict mode 
// var num=3;

// (function(){
//     if(num<=0){
//        return ;
//     }
//     num--;
//     console.log(num);
//     arguments.callee();
// })();


// 匿名函数具名化

var num=3;

(function fn(){
    // 私有执行上下文里面是可以访问到变量fn的，在外面访问不了
    //fn=3;  相当于是const  不允许再次修改值了
    if(num<=0){
       return ;
    }
    num--;
    console.log(num);
    fn();
})();

//console.log(fn);  : fn is not defined


