var total=function(x,y){
    return x+y;
}

var total=(x,y)=>{
    return x+y;
}

// 只有一个参数的时候，小括号也可以省去不写
var total=x=>{
    return x+1;
}
// 如果只有一句代码，需要return ，  花括号和return 都可以省去不写
var total=x=>x+1;


//------箭头函数里面注意两点：1、没有arguments  2没有this
// 箭头函数里面没有this,如果写this，指的是上级作用域中的this

var total=(x,y)=>{
    // console.log(arguments);
     console.log(this);
     return x+y;
 }
 
 total(1,2,3,4);
