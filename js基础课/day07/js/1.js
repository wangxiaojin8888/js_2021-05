/* 
 依次拿出数组中的每一项和它后面所有的项逐一进行比较，如果有相等的就说明重复，此时我从数组中把这一项删除掉
*/



var ary2 = [11, 1, 1, 22, 3, 22, 4];

// 去重
function noRepeat(ary) {
  //ary.length-1  因为最后一项后面没数据了，所以最后一项不用再拿出来了
  for (var i = 0; i < ary.length - 1; i++) {
    //console.log(ary[i]);
    var getItem = ary[i];// i=0  ary[i] 1
    for (var j = i + 1; j < ary.length; j++) {
      //如果相等就说明数组这项重复了
      if (getItem == ary[j]) {
        // 删除一项后，数组的长度就减少了
        ary.splice(j, 1);
        j--;
      }
    }
  }
  return ary;

}
var ary = [1, 1, 1, 2, 3, 2, 4];
var res = noRepeat(ary);
var res2 = noRepeat([1,2,1,3]);