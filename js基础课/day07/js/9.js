// 求1到100中同时能被2整除又能被3整除的所有数之和

function total(n){
    if(n>100){
        return 0;
    }
    if(n%6==0){
        // return 6+ 12+ total(13)
        return n+total(n+1);
    }
    return total(n+1);
}
var res=total(1);

