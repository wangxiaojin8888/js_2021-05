/* 
  去重第二种方法：利用给对象添加属性名和属性值 做的去重
  创建一个空对象，遍历数组中的每一项，给空对象添加属性名和属性值，但是在添加的过程中，如果发现此属性名对应的已经有此属性值，就说明重复了
  var ary=[1,2,1,3];
  var item=1;  2
  var obj={1:1,2:2,};
  obj[]
  obj[item]=item
 
*/
var ary=[2,3,2,3,4];

function unique(ary){
     var obj={};
     for(var i=0;i<ary.length;i++){
         var item=ary[i];// 每一项
         // 说明重复了
         if(obj[item]==item){
            ary.splice(i,1);
            i--;
            continue;
         }
         obj[item]=item
     }
     return ary;
}
var res=unique(ary);