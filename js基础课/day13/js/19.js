/* 
全局执行上下文：
  变量提升：
   var foo----undefined----“hello”
   代码自上而下运行：




*/

var foo="hello";
(function(foo){
    /* 
      私有执行上下文：
      foo："hello"----"hello"
      变量提升：
      var foo,但此时已经声明过foo 不再声明了
      代码自上而下运行：
    
    */
  console.log(foo); //"hello"
  var foo=foo||"word";// foo=foo||"word"----foo="hello"||"word";
  console.log(foo);//"hello"
})(foo);//("hello")
console.log(foo);// 全局的foo,----"hello"