/* 
全局：
  变量提升：
  var  a---undefined-----12---13
  function fn-----AAAFFF111


*/
console.log(a);//undefined
var a=12;
function fn(){
    /* 
    私有执行上下文：
      
    
    */
    console.log(a);//a 不是私有的，向上一级作用域找  12
     a=13;// 把全局的a 改成了13
}
fn();
console.log(a);//13
