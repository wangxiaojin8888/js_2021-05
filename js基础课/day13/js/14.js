/* 
全局执行上下文：
   变量提升：
   var foo------1
   function bar--------AAAFFF111
   ----------



*/

var foo=1;
function bar(){
    /* 
    私有执行上下文：
      变量提升：
        var foo ---undefined---10
    
    
    */
  if(!foo){  // !undefined
    var foo=10; // 私有的
  }
  console.log(foo);//10
}
console.log(bar());//undefined