var a=10,b=11,c=12;
function text(a){
    /* 
     私有执行上下文：
        a=10  私有（形参）
        var b 私有
    */
  a=1;
  var b=2;
  c=3;// 把全局的c 改为了3
  
}
text(10);
console.log(a);
console.log(b);
console.log(c);