/* 
全局执行上下文：
  变量提升：
  var a---------9-------0-----------1----0-----1---2
  function fn
  var  f----------函数小b
-----------------------

*/


var a=9;
function fn(){
    /* 
     私有执行上下文：
     形参赋值：无
     变量提升：无
    
     私有执行上下文：
     形参赋值：无
     变量提升：无
    */
  a=0;// a不是私有的，把全局的a改为0
  // 函数小b
  return function(b){
      /* 
      私有执行上下文：
         形参赋值：b-----5 私有的

         私有执行上下文：
         形参赋值：b-----5 私有的

         私有执行上下文：
         形参赋值：b-----5 私有的
      
      */
     return b+a++; // 5+0=5 // 5+0=5 // 5+1=6
  }
}
var f=fn();
console.log(f(5));// 5
console.log(fn()(5));//5
console.log(f(5));
console.log(a);//2