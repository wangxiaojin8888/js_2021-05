
//  一般情况下，执行完毕就销毁了

// function fn(x,y){
//    console.log(x+y);
// }

// fn();

//---- 不销毁: 一个函数在执行的时候，里面有一个引用数据类型被外界占用形成不销毁的作用域

function fn(x,y){
   return function(){
       return x+y;
   }
}

var res=fn(1,2);
//-----------也是闭包
var ary=[];
function B(x,y){
    ary=function(){

    }
}
B();

// 不立即销毁

function fn(x){
   return function (y){
      return x+y;
   }
}
fn(1)(2);