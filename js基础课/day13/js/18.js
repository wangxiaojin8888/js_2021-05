/* 
全局执行上下文：
  变量提升:
    var a ----4--------
    function b

     


*/


var a=4;
function b(x,y,a){
    /* 
    形参赋值：
     x:1
     y:2
     a:3----10
    
    */
  console.log(a);//3
  arguments[2]=10;
  console.log(a);//10
}
a=b(1,2,3);// 因为函数里面没有return  a =undefined
console.log(a)

