/* 
全局执行上下文：
  VO:
  var n：undefiend---------------10
  function fn----AAAFFF111
  var x：undefined------------------fff
  
变量提升：
  var n：undefiend
  function fn----AAAFFF111
  var x：undefined


*/

var n=10;
function fn(){
    /* 
     私有执行上下文：
         AO:
          var n---------20 -----21------22----23
          function f-----ffff
        初始化作用域链：EC(fn)---EC(g)
        ....
        变量提升：
        var n
        function f-----ffff
    
    
    */
    var n=20;
    /* fff */
    function f(){
        /* 
         私有执行上下文：
         n 不是私有的
         私有执行上下文：
         私有执行上下文：
        */
        n++;
        console.log(n);//21 22 23
    }
    f();
    return f;
}
var x=fn();
x();
x();
console.log(n);//10