/* 
全局执行上下文：
   变量提升：
   var n ----0
   function a-----AAAFFF111
   var c-------函数小b
 代码自上而下运行：
*/
var n=0;
function a(){
    /* 
    私有执行上下文：
      变量提升：
      var n-------10------11----12
      function b-----
    
    */
    var n=10; 
    function b(){
        /* 
        私有执行上下文：
           形参赋值：无
           变量变量：无
        私有执行上下文：
           形参赋值：无
           变量变量：无   
        */
        n++;
        console.log(n);//11 12
    }
    b();
    return b;
}
var c=a();
c();
console.log(n);//0