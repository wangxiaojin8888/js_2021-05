下载git 
运行命令  git config --global   user.name  github账号/gitee账号/gitlab账号
         git config --global   user.email  github邮箱/gitee邮箱/gitlab邮箱
         这两个步骤是用来告诉本机 git 你是谁 一台电脑运行一次即可

git clone 项目地址     

git status  查看当前仓库中文件的状态
git add . 把所有文件提交到缓存区
git commit -m '注释'  把缓存去的修改提交到历史区
git push  把历史区的修改提交到远程仓库

git branch  查看当前仓库有哪些分支
git checkout  xxx 切换到xxx分支
git checkout -b xxx 新创建一个xxx分支 并且切换到这个分支

git merge xxx  把xxx分支合并到当前分支

git reset  回滚
