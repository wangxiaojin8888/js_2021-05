'use strict';
const {
	mpWxGetSessionKey,
	mpWxGetPhoneNo
} = require('wx-auth')
const moment = require('moment');
const axios = require('axios');
const db = uniCloud.database();
const userCollection = db.collection('th_user')
const ticketCollection = db.collection('th_ticket')
const orderCollection = db.collection('th_orders')
exports.main = async (event, context) => {
	const {
		operation,
		data
	} = event;
	switch (operation) {
		case "loginByWeixin": {
			return await loginByWeixin(data);
		}
		case "getPhoneByWeixin": {
			return await getPhoneByWeixin(data);
		}
		case "checkTicket": {
			return await checkTicket(data);
		}
		case "getTicket": {
			return await getTicket(data);
		}
		case "saveOrder": {
			return await saveOrder(data);
		}
		case "getExpress": {
			return await getExpress(data);
		}
		default: {
			throw new Error("未找到接口")
		}
	}
};
// 通过微信登录
async function loginByWeixin(data) {
	// 获取openid
	const wxUser = await mpWxGetSessionKey(data)
	console.log("wxUser", wxUser)
	if (wxUser.status == 0) {
		throw new Error(wxUser.msg)
	}
	// 通过openid查库里的数据
	const result = await userCollection.where({
		openId: wxUser.userInfo.openId
	}).get()
	let id = null;
	console.log("result", result.data[0])
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	// 如果已经有了，则更新库里的信息
	if (result.data.length > 0) {
		let user = result.data[0]
		const r = await userCollection.doc(user._id).update({
			nickName: wxUser.userInfo.nickName,
			gender: wxUser.userInfo.gender,
			avatar: wxUser.userInfo.avatar,
			city: wxUser.userInfo.city,
			language: wxUser.userInfo.language,
			updateTime: now
		})
		return {
			code: 200,
			data: {
				id: user._id,
				nickName: wxUser.userInfo.nickName,
				gender: wxUser.userInfo.gender,
				avatar: wxUser.userInfo.avatar,
				city: wxUser.userInfo.city,
				language: wxUser.userInfo.language,
				sessionKey: wxUser.session_key
			}
		};
	} else {
		// 若没有，则插入该用户数据
		const r = await userCollection.add({
			nickName: wxUser.userInfo.nickName,
			gender: wxUser.userInfo.gender,
			avatar: wxUser.userInfo.avatar,
			openId: wxUser.userInfo.openId,
			avatar: wxUser.userInfo.avatar,
			city: wxUser.userInfo.city,
			language: wxUser.userInfo.language,
			createTime: now,
			updateTime: now
		})
		return {
			code: 200,
			data: {
				id: r.id,
				nickName: wxUser.userInfo.nickName,
				openId: wxUser.userInfo.openId,
				gender: wxUser.userInfo.gender,
				avatar: wxUser.userInfo.avatar,
				city: wxUser.userInfo.city,
				language: wxUser.userInfo.language,
				sessionKey: wxUser.session_key
			}
		}
	}
}
//获取微信授权手机号码
async function getPhoneByWeixin(data) {
	var res = await mpWxGetPhoneNo(data);
	return {
		code: 200,
		data: res
	}
}
//校验提货券
async function checkTicket(data) {
	const ticket = await ticketCollection.where({
		ticketNo: data.ticketNo
	}).get()
	console.log("ticket", ticket)
	if (ticket.data.length == 0) {
		throw new Error("礼券编号不正确")
	}
	if (ticket.data[0].ticketPwd != data.ticketPwd) {
		throw new Error("礼券密码不正确")
	}
	return {
		code: 200,
		data: {
			status: ticket.data[0].status,
			id: ticket.data[0]._id,
		}
	}
}
//获取提货券
async function getTicket(data) {
	var ticket = await ticketCollection.doc(data.id).get();
	console.log("ticket", ticket)
	console.log("ticket.data[0]", ticket.data[0])
	if (ticket.data.length == 0) {
		throw new Error("礼券编号不正确")
	}
	if (ticket.data[0].status != 10) {
		throw new Error("礼券状态不正确")
	}
	return {
		code: 200,
		data: ticket.data[0]
	}
}
//保存提货订单
async function saveOrder(data) {
	var ticket = await ticketCollection.doc(data.ticketId).get();
	console.log("ticket", ticket)
	if (ticket.data.length == 0) {
		throw new Error("礼券编号不正确")
	}
	if (ticket.data[0].status != 10) {
		throw new Error("礼券状态不正确")
	}
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	//插入订单
	await orderCollection.add({
		ticketId: ticket.data[0]._id,
		ticketNo: ticket.data[0].ticketNo,
		productId: ticket.data[0].productId,
		productName: ticket.data[0].productName,
		personName: data.personName,
		city: data.city,
		address: data.address,
		phoneNo: data.phoneNo,
		expressCode: "",
		createTime: now
	})
	//更新礼券状态
	await ticketCollection.doc(data.ticketId).update({
		status: 20
	})
}
/**
 * 获取物流信息
 * @params params 数据
 */
async function getExpress(data) {
	var orders = await orderCollection.where({
		ticketId: data.id
	}).get();
	console.log("orders", orders.data[0])
	if(!orders.data[0].expressCode){
		throw new Error("未发货")
	}
	return new Promise((resolve, reject) => {
		axios({
			method: 'get',
			url: "https://cexpress.market.alicloudapi.com/cexpress?no=" + orders.data[0].expressCode,
			headers: {
				'Authorization': "APPCODE 11cfb65aed664ac2abecbe36a07727b8"
			}
		}).then(res => {
			console.log("getExpressdata", res.data)
			resolve({
				code: 200,
				data: res.data
			})
		})
	})
}
