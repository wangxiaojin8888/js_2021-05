import Vue from 'vue'
import App from './App'
import store from './store'
import * as util from '@/common/js/util.js'
import uView from "uview-ui";
Vue.use(uView);
// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare);
Vue.prototype.$util = util
Vue.prototype.$store = store

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
