// 防抖 input的连续输入 和  按钮的连续点击
// let timer;
// input.oninput = function (e) {
//   clearTimeout(timer)
//   timer = setTimeout(() => {
//     console.log('业务逻辑')
//   }, 1000);
// }
// 防抖函数
// const debounce = (fn, delay) => {
//   let timer;
//   return function () {
//     clearTimeout(timer)
//     timer = setTimeout(() => {
//       fn.call(this, ...arg)
//     }, delay);
//   }
// }
// let f = debounce(() => { console.log('业务逻辑') }, 1000)
// input.oninput = f





// 节流 scroll touchmove  mousemove
let flag = true;
let timer;
window.onscroll = function () {
  if (!flag) return;
  flag = false
  timer = setTimeout(() => {
    flag = true
    clearTimeout(timer)
  }, 100);
  console.log("业务逻辑")
}


// 针对数组和对象的深克隆
function deepClone(option) {
  if (typeof option == 'object') {
    if (Array.isArray(option)) {
      // 数组
      return option.map(item => deepClone(item))
    } else {
      // 普通对象
      let obj = {}
      Object.keys(option).forEach(key => {
        // key代表option这个对象中的每一个属性名
        obj[key] = deepClone(option[key])
      })
      return obj
    }
  } else if (typeof option == 'function') {
    return new Function('return ' + option.toString()).call()
  } else {
    // option 是值类型
    return option
  }
}
let obj = {
  q: { e: [1, 2, { e: 6 }], r: { t: 666 } }
}
// let obj2 = deepClone(obj)

let obj3 = { ...obj }
obj3.q.e = 888;
console.log(obj.q.e)
console.log(obj3.q)


// 
let cb = [];
function on(f) { cb.push(f) }
function emit(f) { cb.forEach(f => f()) }
function off(f) { cb = cb.filter(t => t != f) }



// ary  instanceof Number


function f() {
  console.log(this)
  console.log(arguments)
}
// f.call([], 1, 2, 3)
// f.apply([], [1, 2, 3])
// let f2 = f.bind([], 1, 2, 3)
// f2(4,5,6)
Function.prototype.mycall = function (_this, ...arg) {
  // this --> f
  let f = this;
  let key = Symbol()
  _this[key] = f;
  let res = _this[key](...arg)
  delete _this[key]
  return res
}
let res = f.mycall({ qq: 333 }, 1, 2, 3)

Function.prototype.mybind = function (_this, ...arg) {
  // this -- f
  let f = this;
  return (...arg2) => {
    return f.call(_this, ...arg, ...arg2)
  }
}
let f2 = f.mybind([], 1, 2, 3)
let res2 = f2(4, 5, 6)

Object.mycreate = function (proto) {
  // let obj = {};
  // obj.__proto__ = proto
  // return obj
  function F() { }
  F.prototype = proto;
  return new F()
}
Object.mycreate([])
// let obj = Object.create(null) 纯对象


let p1 = Promise.resolve(1)
let p2 = Promise.resolve(2)
let p3 = Promise.resolve(3)
let p4 = Promise.resolve(4)


let ary = [p1, p2, p4, p3]
Promise.all(ary).then((data) => {
  console.log(data) // [1,2,4,3]
}).catch(() => {

})


Promise.myall = function (ary) {
  let resAry = [];
  let length = ary.length;
  let count = 0;
  return new Promise((res, rej) => {
    ary.forEach((p, index) => {
      p.then(data => {
        resAry[index] = data;
        count++;
        if (count == length) {
          res(resAry)
        }
      }).catch(err => {
        rej(err)
      })
    })
  })
}

Promise.myany = function (ary) {
  let count = 0;
  return new Promise((res, rej) => {
    for (i = 0; i < ary.length; i++) {
      ary[i].then(data => {
        res(data)
      }).catch(err => {
        count++;
        if (count == ary.length) {
          rej()
        }
      })
    }
  })
}

//  /([^?=&]+)=([^?=&]+)/g  replace

/*
  a='34';b='1234567'; // 返回 2
  a='35';b='1234567'; // 返回 -1
  a='355';b='12354355'; // 返回 5
  isContain(a,b);

*/
function isContain(a, b) {
  if (a.length > b.length) return -1;
  let length = a.length;
  for (let i = 0; i < b.length; i++) {
    if (b[i] == a[0]) {
      if (b.slice(i, i + length) == a) {
        return i
      }
    }
  }
  return -1
}