import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// vue3 + vuex4 + vueRouter4

let vm = createApp(App)
vm.use(store)
vm.use(router)
vm.mount('#app')
