import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

fetch('/qqq')
Vue.config.productionTip = false
let qqq = require.context('./globalcomponent', false, /\.vue$/);

qqq.keys().forEach(r => {
  console.log(qqq(r))
  let obj = qqq(r).default;
  Vue.component(obj.name, obj)
});
let vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// 只有vm的$options 中有 router
