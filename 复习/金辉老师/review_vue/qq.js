setTimeout(function () {
  console.log(1);
}, 0);
new Promise(function executor(resolve) {
  console.log(2);
  for (var i = 0; i < 10000; i++) {
    resolve();
  }
  console.log(3);
}).then(function () {
  console.log(4);
});
console.log(5);



// 闭包。说结果，然后分别使用Promise和async改写成每隔1s打印1个数字的形式
function print(n) {
  // for (var i = 0; i < n; i++) {
  //   setTimeout(console.log, 1000, i);
  // }
  let i = 0;
  async function fn() {
    if (i >= n) return
    let p = new Promise(res => {
      setTimeout(() => {
        res(i)
        ++i
      }, 1000);
    })
    await p.then(qq => console.log(qq))
    fn()
  }
  fn()
}
print(10);

/* 


*/
function fn(n) {
  return new Promise((res) => {
    setTimeout(() => {
      res(n + 100)
    }, 1000);
  })
}
async function qqq() {
  for (let i = 0; i < 10; i++) {
    let q = await fn(i)
    console.log(q)
  }
}





var a = 20;
function bar() {
  console.log(a);
}
function foo(fn) {
  var a = 10;
  fn();
}
foo(bar);






function bar() {
  var a = 20;
  return function () {
    console.log(a);
  }
}


var foo = bar();
var a = 10;
foo();




const promise = new Promise((resolve, reject) => {
  console.log(1);
  resolve(5);
  console.log(2);
}).then((res) => {
  console.log(res);
})

promise.then(() => {
  console.log(3);
})
console.log(4)
setTimeout(() => {
  console.log(6)
})











async function async1() {
  console.log('async1 start');
  await async2();
  console.log('async1 end');
}
async function async2() {
  console.log('async2 start');
}
console.log('script start');
setTimeout(function () {
  console.log('setTimeout');
}, 0);
async1();
new Promise(function (resolve) {
  console.log('promise1');
  resolve();
}).then(function () {
  console.log('promise2');
}).then(function () {
  console.log('promise3');
});
console.log('script end');
