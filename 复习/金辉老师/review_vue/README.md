# 组件通信的方式有哪些?
  自定义属性+props 父传子
  自定义事件+$emit  子串父
  vuex/localStorage vuex有响应式

  $attrs/$listeners // $attrs 没有被props接收的那些属性
  provide/inject  父亲设置 所有的后代都能使用

  eventBus:  $on $emit $off;

    ref  获取组件实例的方式去 使用子组件的数据
    $children $parent

    <!-- 父组件怎么使用子组件的methods方法 -->

  路由传参实现组件参数传递

  $root



# computed和watch的区别
  计算属性适用于有明显以来关系的情况 而且计算属性是一个新增的属性（不适用异步依赖）
  vuex中的数据在组建中一般都是使用计算属性接收的
  侦听器 是用来监听已经存在的哪些属性
   - watch能监听computed的属性吗？
    能；deep immediate

# vue的响应式原理
  vue2 Object.defineProperty
  vue3 Proxy
  new Vue   Observe 数据劫持 ,劫持数据的时候会给每一个属性添加一个Dep实例
              用来操作属性对应Watcher实例，Dep实例有一个subs用来存储Watcher
              Watcher是当这个属性被调用的时候，会触发这个属性的get函数，这个时候
              就会通过new 去创建一个watcher实例，然后把这个实例存储Dep实例的subs
              数组中，一旦这个属性更新的时候，会触发这个属性的set函数，set函数一执行的话
              就会触发Dep实例的notify执行，notify一执行 就会让subs中的所有的Watcher实例执行
              update方法 来更新视图
            Compile  模板编译  

  

# nextTick原理  
   异步+发布订阅
   callbacks是个数组，用来存储nextTick执行传进来的回调函数
   pending 用来控制当前的任务是否正在执行
   timerFun(能用微任务就是用微任务，不行就宏任务)创造一个异步函数

   当nextTick执行的时候 会把传进来的回调函数套一层函数存储到callbacks中，然后看是否有异步正在执行 也就是看pending是不是true,不是true的时候，把pending改成true，然后执行timerFun,这个函数一执行，异步执行flushCallbacks（这个函数是用来让事件池中的所有事件执行，并清空事件池，然后pending改成false）


# v-model 原理
  :value
  @input
  功能类似于 .sync修饰符
    .sync修饰符 除了传递了一个行内属性，还默认传递了一个 update:xxx的事件，这样的话 在子组件中我们就可以通过$emit来触发这个事件 从而修改父组件传进来的这个属性
