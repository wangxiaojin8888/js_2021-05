(function () {
    /* 核心 */
    function Promise(executor) {
        // 检测格式
        var self = this;
        if (!(self instanceof Promise)) throw new TypeError("undefined is not a promise");
        if (typeof executor !== "function") throw new TypeError("Promise resolver is not a function");

        // 设置私有属性 && 修改状态和值
        self.state = "pending";
        self.result = undefined;
        self.onfulfilledCallbacks = [];
        self.onrejectedCallbacks = [];
        var change = function change(state, result) {
            if (self.state !== "pending") return;
            self.state = state;
            self.result = result;
            // 状态/值修改后，通知指定集合中存储的onfulfilled/onrejected执行
            setTimeout(function () {
                var callbacks = self.state === "fulfilled" ? self.onfulfilledCallbacks : self.onrejectedCallbacks;
                for (var i = 0; i < callbacks.length; i++) {
                    var callback = callbacks[i];
                    callback(self.result);
                }
            });
        };

        // 执行executor：基于resolve/reject/是否报错，修改实例的状态和值
        try {
            executor(function resolve(value) {
                change("fulfilled", value);
            }, function reject(reason) {
                change("rejected", reason);
            });
        } catch (err) {
            change("rejected", err);
        }
    }

    /* 原型 */
    Promise.prototype = {
        constructor: Promise,
        then: function then(onfulfilled, onrejected) {
            // THEN的穿透性
            if (typeof onfulfilled !== "function") {
                onfulfilled = function onfulfilled(value) {
                    return value;
                };
            }
            if (typeof onrejected !== "function") {
                onrejected = function onrejected(reason) {
                    throw reason;
                };
            }

            // 基于实例的状态决定执行的方法「为了兼容，我们基于定时器代替异步微任务」
            var self = this;
            switch (self.state) {
                case "fulfilled":
                    setTimeout(function () {
                        onfulfilled(self.result);
                    });
                    break;
                case "rejected":
                    setTimeout(function () {
                        onrejected(self.result);
                    });
                    break;
                default:
                    self.onfulfilledCallbacks.push(onfulfilled);
                    self.onrejectedCallbacks.push(onrejected);
            }
        }
    };

    /* 静态私有属性方法 */
    Promise.resolve = function resolve(value) {
        return new Promise(function (resolve) {
            resolve(value);
        });
    };
    Promise.reject = function reject(reason) {
        return new Promise(function (_, reject) {
            reject(reason);
        });
    };

    // 检测是否为promise实例「按照promiseA+规范」
    var isPromise = function isPromise(x) {
        if (x !== null && /^(object|function)$/i.test(typeof x)) {
            try {
                var then = x.then;
                if (typeof then === "function") return true;
            } catch (_) { }
        }
        return false;
    };
    Promise.all = function all(promises) {
        if (!Array.isArray(promises)) throw new TypeError("promises is not a array");
        return new Promise(function (resolve, reject) {
            // 根据promises集合中每一项的状态，基于resolve/reject，控制返回实例的状态
            var num = 0,
                results = [],
                len = promises.length;
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var promise = promises[i];
                    // 如果这一项不是promise实例，我们要把其变为“状态成功、值是本身”的实例
                    if (!isPromise(promise)) promise = Promise.resolve(promise);
                    promise.then(
                        function onfulfilled(value) {
                            // 有一项成功:计数器累加、存储当前成功的结果到results中、验证是否都成功
                            num++;
                            results[i] = value;
                            if (num >= len) resolve(results);
                        },
                        function onrejected(reason) {
                            // 有一项失败，整体就是失败
                            reject(reason);
                        }
                    );
                })(i);
            }
        });
    };
    Promise.any = function any(promises) {
        if (!Array.isArray(promises)) throw new TypeError("promises is not a array");
        return new Promise(function (resolve, reject) {
            var num = 0,
                len = promises.length;
            for (var i = 0; i < len; i++) {
                var promise = promises[i];
                if (!isPromise(promise)) promise = Promise.resolve(promise);
                promise.then(
                    function onfulfilled(value) {
                        resolve(value);
                    },
                    function onrejected(reason) {
                        num++;
                        if (num >= len) reject("All promises were rejecte");
                    }
                );
            }
        });
    };
    Promise.race = function race(promises) {
        if (!Array.isArray(promises)) throw new TypeError("promises is not a array");
        return new Promise(function (resolve, reject) {
            var len = promises.length;
            for (var i = 0; i < len; i++) {
                var promise = promises[i];
                if (!isPromise(promise)) promise = Promise.resolve(promise);
                promise.then(resolve, reject);
            }
        });
    };
    // 支持自己定义失败的数量：只有集合中失败的数量符合limit，才算失败！！
    Promise.allLimit = function allLimit(promises, limit) {
        if (!Array.isArray(promises)) throw new TypeError("promises is not a array");
        var len = promises.length,
            num1 = 0,
            num2 = 0,
            results = [];
        limit = +limit;
        if (isNaN(limit)) limit = 1;
        limit = limit < 1 ? 1 : (limit > len ? len : limit);

        return new Promise(function (resolve, reject) {
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var promise = promises[i];
                    if (!isPromise(promise)) promise = Promise.resolve(promise);
                    promise.then(
                        function onfulfilled(value) {
                            num1++;
                            results[i] = value;
                            if (num1 >= len) resolve(results);
                        },
                        function onrejected(reason) {
                            num2++;
                            num1++;
                            results[i] = null;
                            if (num2 >= limit) reject("Already has " + limit + " items failed");
                        }
                    );
                })(i);
            }
        });
    };

    /* 暴露API */
    if (typeof window !== "undefined") window.Promise = Promise;
    if (typeof module === "object" && typeof module.exports === "object") module.exports = Promise;
})();

/* console.log(1);
let p = new Promise((resolve, reject) => {
    console.log(2);
    setTimeout(() => {
        reject(0);
        console.log(3);
    }, 1000);
});
p.then(value => {
    console.log('成功:', value);
}, reason => {
    console.log('失败:', reason);
});
p.then(value => {
    console.log('成功:', value);
}, reason => {
    console.log('失败:', reason);
});
console.log(4); */

let p1 = new Promise(resolve => {
    setTimeout(() => {
        resolve(1);
    }, 3000)
});
let p2 = Promise.reject(2);
let p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject(3);
    }, 1000);
});
let p4 = 4;

Promise.allLimit([p1, p2, p3, p4], 4).then(values => {
    console.log('成功:', values);
}, reason => {
    console.log('失败:', reason);
});