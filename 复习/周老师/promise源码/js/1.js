/*
 创建promise实例的方式：
   @1 let p = new Promise([executor])
     + Promise不能当做普通函数执行 TypeError: undefined is not a promise
     + [executor]必须是一个函数，不传也不行 TypeError: Promise resolver xxx is not a function
     + new Promise((resolve,reject)=>{})
       + [executor]函数中一般处理异步编程的代码，new Promise的时候会立即把其执行
       + resolve/reject是用来修改实例的状态和值的
       + [executor]函数执行报错，则实例状态也是失败的，值是报错原因
     + 一但状态被修改为成功或者失败，就不能再修改为其他状态了

   @2 let p2 = p.then([onfulfilled],[onrejected])  
     + 每一次执行THEN会创建一个全新的promise实例“p2”「目的：实现出THEN链机制」
     + onfulfilled或者onrejected如果没有传递或者传递的不是函数，这样会顺延至下一个THEN中，同等状态应该执行的方法中执行「THEN的穿透/顺延机制」
     + p2的状态和值是是由当前THEN中传递的[onfulfilled]（或者[onrejected]）执行决定
       + 执行函数报错，则p2是失败的，值是报错原因
       + 再看执行的返回结果
         + 如果返回了一个新的promie实例“@P”，则@P的状态和值决定了p2的状态和值
         + 如果返回的不是新实例，则p2状态是成功，值是函数的返回值
     + p.catch([onrejected]) -> p.then(null,[onrejected])
       p.then([onfulfilled])
        .then([onfulfilled])
         ...
        .catch([onrejected])
   
   @3 Promise.resolve([value]) && Promise.reject([reason])
     创建状态是成功或者失败的实例(实例值就是传递的实参值)

   @4 let p = Promise.all/any/race([promises]);
     + promises是包含一到多个实例的集合「如果集合中的某一项不是promise实例，则默认转换为状态为成功，值是本身的实例」
     + all：集合中每个实例状态都是成功，则p才是成功，值是按照顺序依次存储每个实例值的数组；如果其中有一个实例失败，则p就是失败的，值是当前项失败的原因，后续还没处理的也无需处理了！！
     + any：集合中只要有是一个实例成功，则p就是成功的，值是当前成功的结果...
     + race：以集合中最快处理出来的那一项实例为主...

 关于Promise中的异步编程：
   @1 promise.then([onfulfilled],[onrejected])
     执行THEN方法的时候，此时promise的状态不同，则处理方式也是不同的
     + 已知promise的状态(成功/失败)：但是也不是立即把[onfulfilled]/[onrejected]执行，而是创建一个“异步微任务”(放在EventQueue中排队等待)，同步操作处理完，对应的方法才会执行!!
     + promise的状态还是pending：此时我们把[onfulfilled]/[onrejected]存储起来，当以后修改实例状态后，再把事先存储好的方法执行！！

   @2 在executor函数中，执行resolve/reject的时候
     + 立即修改实例的状态和值
     + 通知之前基于TEHN存储的对应方法执行，但也不是立即执行，而是创建一个“异步微任务”

 Promise是ES6新增的API，所以不兼容IE浏览器
   + 我们在webpack中基于 babel-preset(语法包) && babel-loader 等，只能把ES6的语法编译为ES5（例如：可以把let编译为var...），但是对于ES6内置的API方法（例如：Promise、Reflect、Fetch...）是无法编译的！
   + 此时我们需要 @babel/polyfill 插件处理「原理：插件中把常用的ES6内置API，基于ES5的语法进行了重写」
   + ...
 */


/* 
console.log(1);
let p = new Promise(resolve => {
    console.log(2);
    setTimeout(() => {
        resolve(100);
        console.log(3, p);
    }, 2000);
});
p.then(value => {
    console.log('成功:', value);
});
console.log(4);
// 1 2 4
// 3 成功:100 
*/

/* 
console.log(1);
Promise.resolve(100)
    .then(value => {
        console.log('成功:', value);
    });
console.log(2); 
// 1 2 成功:100 
*/

/* Promise.all([
    Promise.resolve(1),
    2 //=>Promise.resolve(2)
]) */

/* Promise.resolve(100)
    .then(/!*value=>{
        return 100;
    }*!/)
    .then(value => {
        console.log('成功:', value); //成功 100
        return Promise.reject(10);
    }, reason => {
        console.log('失败:', reason);
        return 20;
    })
    .then(value => {
        console.log('成功:', value);
        console.log(a);
    }/!*,reason=>{
        throw reason;
    }*!/)
    .then(value => {
        console.log('成功:', value);
    }, reason => {
        console.log('失败:', reason); //失败 10
    }); 
*/