/* 
 面试中所有问题的回答，都切记“背书式”，加一些过渡词/故事
   例如：回答的时候不离开项目 “我之前项目开发中，常用的是...，我私下也看过很多文章，了解到还有...，只不过项目中不常用！！”
   例如：学会讲故事，故事的起因一般都是项目中遇到了问题（很困扰），突出自己“熬夜”去整理了很对资料，在恭维一下面试官...

 面试中要擅于把面试官“引导”到自己精心准备的问题上

 面试的时候一定要录音，根据录音做面试总结
   + 剧本精神：把面试官问的问题，以及如何回答，都写成剧本！！
   + ...

 面试带上笔和本，遇到不会的面试题，当着面试官的面，让他给你讲讲，并且记下来「嘟囔一下晚上回去搞定」

 装X技巧：面试带本书 React/Node...

 投递简历：批量海投，每天定时刷新
   + 不上课了，也不要犯懒
   + BOSS、拉钩、智联、51job、脉脉...
   + 相互帮助模式
 */

/* 
 Iterator迭代器：是一种接口机制，为不同的数据结构提供循环/迭代操作「基于for of循环」
   + 如果某一种数据结构想要基于for of进行循环，则必须部署Iterator机制
   + 并不存在Iterator这个内置类，它只是提供了一种规范
     + 必须具备next方法
     + 执行next会依次遍历数据结构中每一项成员，返回下述对象
       {done:false,value:xxx}
       + done记录是否遍历完成
       + value当前遍历这一项的值

 部署迭代器规范的数据结构（这些结构可以使用for of循环）：Symbol.iterator 值是函数
   + 数组
   + 字符串
   + arguments「部署到自己的私有属性上」
   + new Set
   + new Map
   + HTMLCollection
   + NodeList
   + ...

 标准普通对象是没有Symbol.iterator这个属性的(也就是不具备迭代器规范，不能使用for of进行遍历),如何让其能？
 */
/* 
class Iterator {
  constructor(assemble) {
    this.assemble = assemble;
    this.index = 0;
  }
  next() {
    let { assemble, index } = this,
      value = assemble[index];
    if (index > assemble.length - 1) {
      // 迭代结束
      return {
        done: true,
        value
      };
    }
    this.index++;
    return {
      done: false,
      value
    };
  }
}
let itor = new Iterator([10, 20, 30, 40]);
console.log(itor.next()); //{done:false,value:10}
console.log(itor.next()); //{done:false,value:20}
console.log(itor.next()); //{done:false,value:30}
console.log(itor.next()); //{done:false,value:40}
console.log(itor.next()); //{done:true,value:undefined} 
*/

/* 
// for of循环进行迭代，获取的是每一项的值，而不是索引(键)
let arr = [10, 20, 30, 40, 50];
arr[Symbol.iterator] = function () {
  let self = this, //arr
    index = 0;
  return {
    next() {
      if (index > self.length - 1) {
        return {
          done: true,
          value: undefined
        };
      }
      let temp = index;
      index += 2;
      return {
        done: false,
        value: self[temp]
      };
    }
  };
};
for (let value of arr) {
  // 首先把迭代器接口函数执行 let itor = arr[Symbol.iterator]();
  // 而for of的每一轮迭代都是在执行 itor.next() => {done:xxx,value:xxx}
  //   + 根据done的值是true/false来决定循环是否继续
  //   + 把对象中的value赋值给声明的变量value
  console.log(value);
} 
*/


/*
 JS中的循环操作很多，其它都可以用，唯独for in循环“慎用”「原因：因为其性能差、可能会迭代公共可枚举属性、拿不到属性名是Symbol类型的...」
   + for & while
   + 数组的forEach/map等方法
   + for of
   + for in
   + ... 
 对于普通对象来讲，for、while、forEach等循环都是不适合它的迭代的（除非对象是个类数组），而其还不能直接用for of；所以我们一般处理对象的迭代：
   + 让其可以使用for of「快速获取每一项的值」
   + 获取所有的私有属性，然后手动按照属性进行迭代
 */
/* 
let obj = {
  name: '珠峰',
  age: 13,
  beautiful: '洪靖',
  handsome: '邵宇',
  [Symbol('AA')]: 100
};
Object.prototype[Symbol.iterator] = function () {
  let self = this,
    keys = Reflect.ownKeys(self),
    index = 0;
  return {
    next() {
      if (index > keys.length - 1) {
        return {
          done: true,
          value: undefined
        };
      }
      return {
        done: false,
        value: self[keys[index++]]
      };
    }
  };
};
for (let value of obj) {
  console.log(value);
} 
*/

/* 
let obj = {
  0: 10,
  1: 20,
  2: 30,
  length: 3
};
obj[Symbol.iterator] = Array.prototype[Symbol.iterator];
for (let value of obj) {
  console.log(value);
} 
*/

// 封装一个可以迭代数组/对象的循环方法
const each = function each(obj, callback) {
  // 格式校验
  if (typeof callback !== "function") throw new TypeError("callback is not a function!");
  if (obj === null || typeof obj !== "object") throw new TypeError("obj must be a array or object!");
  // 数组的迭代处理:每一轮迭代都会把回调函数执行，但是如果回调函数执行返回false则结束循环
  if (Array.isArray(obj)) {
    for (let i = 0; i < obj.length; i++) {
      let item = obj[i];
      if (callback(item, i) === false) break;
    }
    return obj;
  }
  // 对象的迭代
  let keys = Reflect.ownKeys(obj);
  for (let i = 0; i < keys.length; i++) {
    let key = keys[i],
      value = obj[key];
    if (callback(value, key) === false) break;
  }
  return obj;
};

let arr = [10, 20, 30, 40];
let obj = {
  name: '珠峰',
  age: 13,
  beautiful: '洪靖',
  handsome: '邵宇',
  [Symbol('AA')]: 100
};
/* each(arr, (item, index) => {
  console.log(item, index);
  if (index >= 2) return false;
}); */
each(obj, (value, key) => {
  console.log(key, value);
});