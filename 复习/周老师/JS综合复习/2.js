/*
 Generator生成器：正常函数名前面加“*”即可
   let itor = generator(param1,param2,...);
    + 此时函数体中的代码并没有被执行
    + 它只是创建了一个符合迭代器规范(有next方法)的对象：此对象是GeneratorFunction/Generator类的实例
      + itor.next()
      + itor.return()
      + itor.throw()
      + ...
   
   itor.next():会控制函数体中的代码进行执行「遇到yield/return结束」，返回拥有done/value属性的对象
     done:记录代码是否执行完 true/false
     value:存储yield后的值或者return返回的值
 */
/* 
function* generator(x, y) {
    console.log('OK', x + y);
    yield 10;
    console.log('OK2');
    yield 20;
    console.log('OK3');
    return 30;
}
let itor = generator(10, 20);
console.log(itor.next()); //输出OK/30 {done:false,value:10}
console.log(itor.next()); //输出OK2 {done:false,value:20}
console.log(itor.next()); //输出OK3 {done:true,value:30}
console.log(itor.next()); //{done:true,value:undefined} 
*/

/* 
function* generator() {
    console.log('OK1');
    yield 10;
    console.log('OK2');
    yield 20;
    console.log('OK3');
    return 30;
}
let itor = generator();
console.log(itor.next()); //输出OK1 {done:false,value:10}
// console.log(itor.return(-1)); //{done:true,value:-1}  直接结束代码的执行
// console.log(itor.next()); //{done:true,value:undefined}
// -----
// console.log(itor.throw(-1)); //直接抛出异常，不会有返回值，下面代码也不能再执行了
// console.log(itor.next()); 
*/

/* 
// 每一次next执行传递进来的值：是给上一次yield操作，赋值结果的「所以第一次执行next传啥都没用」
function* generator() {
    console.log('OK1');
    let x = yield 10;
    console.log('OK2', x);
    let y = yield 20;
    console.log('OK3', y);
    return 30;
}
let itor = generator();
console.log(itor.next(100)); //输出OK1 {done:false,value:10}
console.log(itor.next(200)); //输出OK2 x=200 {done:false,value:20}
console.log(itor.next(300)); //输出OK3 y=300 {done:true,value:30}
console.log(itor.next(400)); //{done:true,value:undefined} 
*/

// 需求：ajax的串行操作
const query = interval => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(interval);
        }, interval);
    });
};

// ES8中新增的Async/Await是 Generator + Promise 的语法糖！！
(async function () {
    let x = await query(1000);
    console.log('成功1', x);

    x = await query(2000);
    console.log('成功2', x);

    x = await query(3000);
    console.log('成功3', x);
})();

/* 
function* generator() {
    let x = yield query(1000);
    console.log('成功1', x);

    x = yield query(2000);
    console.log('成功2', x);

    x = yield query(3000);
    console.log('成功3', x);
};
let itor = generator();
// console.log(itor.next()); //{done:false,value:第一个请求的promise实例}
itor.next().value.then(x => {
    // x:第一次请求成功的结果
    itor.next(x).value.then(x => {
        // x:第二次请求成功的结果
        itor.next(x).value.then(x => {
            // x:第三次请求成功的结果
            itor.next(x);
        });
    });
}); 
*/

/* 
query(1000)
    .then(value => {
        console.log('成功1:', value);
        return query(2000);
    })
    .then(value => {
        console.log('成功2:', value);
        return query(3000);
    })
    .then(value => {
        console.log('成功3:', value);
    }); 
*/