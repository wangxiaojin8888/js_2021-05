(function () {
  let box = document.querySelector('.box');
  let ul = document.querySelector('ul')
  let arrow_box = document.querySelector('.arrow_box')
  let left = arrow_box.querySelector('.left_btn')
  let right = arrow_box.querySelector('.right_btn')
  let dots = box.querySelectorAll('span')
  let index = 0; // 0代表第一张
  let timer;
  function next() {
    index++

    if (index > 5) {
      ul.style.transform = `translateX(0px)`
      ul.style.transition = 'all 0s'
      //  以上两行实现了闪到第一张 
      //  紧接着要往第二张移动
      let q = (ul.offsetLeft) // 逼迫浏览器先把上边的设置渲染出来
      index = 1
      ul.style.transform = `translateX(-${400 * index}px)`
      ul.style.transition = 'all 0.3s'
    } else {
      ul.style.transform = `translateX(-${400 * index}px)`
      ul.style.transition = 'all 0.3s'
    }
    //  index改了
    dotLight(index > 4 ? 0 : index)
  }
  function prev() {
    index--
    console.log(index)
    //  当走到左边界之后  要直接闪到最后一张(伪第一张) 紧接着项倒数第二章移动
    if (index < 0) {
      ul.style.transform = `translateX(-2000px)`
      ul.style.transition = 'all 0s'
      ul.offsetHeight
      index = 4
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    } else {
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    }
    //  index改了

    dotLight(index > 4 ? 0 : index)
  }
  function dotLight(n) {
    dots.forEach(item => {
      item.classList.remove('current')
    })
    dots[n].classList.add('current')
  }
  function move() {
    timer = setInterval(() => {
      next()

    }, 1000);
  }
  move()


  box.onmouseenter = function () {
    //  划入 清除定时器
    clearInterval(timer)
    arrow_box.classList.add('active')
  }
  box.onmouseleave = function () {
    // 离开重启定时器
    move()
    arrow_box.classList.remove('active')
  }
  right.onclick = function () {
    next()
  }
  left.onclick = function () {
    prev()
  }
  dots.forEach((item, i) => {

    item.onclick = function () {
      index = i;
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
      dotLight(i)
    }

  })
})()