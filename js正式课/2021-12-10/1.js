/*
 面试题1：去除字符串首尾空格，不允许使用trim函数
 */
/*
String.prototype.trim = function trim() {
    // this -> Object(str)
    return this.replace(/(^\s+|\s+$)/g, "");
};
let str = "   今天阳光 明媚哈哈     ";
console.log(str);
console.log(str.trim()); //默认进行“装箱”的操作：把原始值转换为非标准特殊对象 Object(str)
// console.log(new Number(10) + 10); //首先会把对象基于 Symbol.toPrimitive->valueOf->toString... 这套流程进行转换，目的是获取其原始值，然后再进行运算；我们把这个操作叫做“拆箱”
*/

/*
 面试题2：JS如何获取网页传输过来的参数a
 http://192.168.0.1/index.html?a=100&b=200#video
 */
/* const queryURLParams = function queryURLParams(url, attr) {
    if (typeof url !== "string") throw new TypeError("url is not a string!");
    let obj = {};
    // 考虑情况
    //  + 问号和井号都没有：“”、“”
    //  + 只有问号：“askIn+1~末尾”，“”
    //  + 只有井号：“”、“wellIn+1~末尾”
    //  + 问号和井号都有
    //    + 问号在前：“askIn+1~wellIn(不含)”、“wellIn+1~末尾”
    //    + 井号在前：“askIn+1~末尾”、“wellIn+1~askIn(不含)”
    let askIn = url.indexOf("?"),
        wellIn = url.indexOf("#"),
        askText = "",
        wellText = "";
    if (askIn > -1 && wellIn > -1) {
        if (askIn > wellIn) {
            askText = url.substring(askIn + 1);
            wellText = url.substring(wellIn + 1, askIn);
        } else {
            askText = url.substring(askIn + 1, wellIn);
            wellText = url.substring(wellIn + 1);
        }
    } else if (askIn > -1) {
        askText = url.substring(askIn + 1);
    } else if (wellIn > -1) {
        wellText = url.substring(wellIn + 1);
    }
    // 设置键值对
    if (wellText) obj["HASH"] = wellText;
    if (askText) {
        // a=100&b=200
        askText.split("&").forEach(item => {
            let [key, value] = item.split("=");
            obj[key] = value;
        });
    }
    if (typeof attr === "undefined") return obj;
    return obj[attr] || "";
}; */
/* const queryURLParams = function queryURLParams(url, attr) {
    if (typeof url !== "string") throw new TypeError("url is not a string!");
    let obj = {};
    url.replace(/#([^?#=&]+)/, (_, $1) => obj["HASH"] = $1);
    url.replace(/([^?#=&]+)=([^?#=&]+)/g, (_, $1, $2) => obj[$1] = $2);
    if (typeof attr === "undefined") return obj;
    return obj[attr] || "";
};
let url = 'http://192.168.0.1/index.html#video?a=100&b=200';
console.log(queryURLParams(url)); //=>{a:100,b:200,HASH:'video'}
console.log(queryURLParams(url, 'a'));//=>100 */

/*
 面试题3：一个长度不超过5位的正整数，转换为对应的中文字符串
 例如：computed(20876) //返回 '贰万零捌佰柒拾陆'
 */
/* const computed = function computed(num) {
    num = +num;
    if (isNaN(num)) return num;
    let area1 = "零壹贰叁肆伍陆柒捌玖",
        area2 = ["元整", "拾", "佰", "仟", "万"],
        result = [];
    String(num).split("").reverse().forEach((x, i) => {
        x = +x;
        // x : 6 7 8 0 2
        // i : 0 1 2 3 4
        result.push(`${area1.charAt(x)}${x === 0 ? "" : area2[i]}`);
    });
    return result.reverse().join("");
};
console.log(computed(20876)); //贰万零捌佰柒拾陆 */


/*
 面试题4：格式化时间字符串
 例如：formatTime("2021/12/8 18:7:23") //返回 '12月08日 18:07'
 */
/* const formatTime = function formatTime(time, temp = "{0}年{1}月{2}日") {
    if (typeof time !== "string") throw new TypeError("time is not a string");
    // 获取年月日/时分秒这些数字
    let arr = time.match(/\d+/g); //['2021', '12', '8', '12', '7', '28']
    // 把其拼凑为我们想要的格式:按照模板解析的思想  每一次找到“{数字}”，拿数字作为索引，到arr中找到对应的值，把找到的结果替换本次匹配的内容!
    // let temp = "{1}月{2}日 {3}:{4}";
    // {1} -> arr[1] 12  "12月{2}日 {3}:{4}"
    // {2} -> arr[2] 8->08  "12月08日 {3}:{4}"
    // ...
    return temp.replace(/\{(\d+)\}/g, (_, $1) => {
        let val = arr[$1] || "00";
        if (val.length < 2) val = "0" + val;
        return val;
    });
};
console.log(formatTime("2021/12/8 12:7:28", "{1}月{2}日 {3}:{4}"));
console.log(formatTime("2021/12/8 12:7:28", "{3}:{4}:{5}")); */

/*
 面试题5：返回数组里出现次数最多的数字
 例如：mostNumber([1, 2, 2, 1, 3, 1, 1,2,2]); //返回 1
 */
/* const mostNumber = function mostNumber(arr) {
    if (!Array.isArray(arr)) throw new TypeError("arr is not an array");
    let obj = {},
        max = 1,
        result = [];
    arr.forEach(item => {
        if (obj.hasOwnProperty(item)) {
            obj[item]++;
            if (obj[item] > max) max = obj[item];
            return;
        }
        obj[item] = 1;
    });
    Reflect.ownKeys(obj).forEach(key => {
        if (obj[key] === max) result.push(key);
    });
    return `出现次数最多的字符：${result.toString()}，出现了：${max}次！`;
};
console.log(mostNumber([1, 2, 2, 1, 3, 1, 1, 2, 2])); */

/*
// 稀疏数组：数组中并不是每一项都有值(值是null也是有值)，在使用数组迭代方法的时候，如果这一项不存在，是不进行迭代处理的！！
// 密集数组：每一项都有值  数组.fill(x)把数组的每一项填充为x
// 需求：循环10次
// for (let i = 0; i < 10; i++) { }
// new Array(10).fill(null).forEach(item => { });
*/

/* // console.time(标识) & console.timeEnd(标识) ：测试出某段程序运行的时间「预估值，仅做参考，受此时电脑性能的影响」
let arr = new Array(9999999).fill(null);
console.time('AAA');
for (let i = 0; i < arr.length; i++) { }
console.timeEnd('AAA'); //大概 8ms

console.time('CCC');
arr.forEach(item => { });
console.timeEnd('CCC'); //大概 115ms

console.time('BBB');
for (let key in arr) { }
console.timeEnd('BBB'); //大概 2500ms */

// for in 循环的机制
//  + 从“私有”的“可枚举”的属性开始迭代，然后会按照__proto__一层层的向上查找，把所有可枚举的属性都迭代到，直到找到Object.prototype为止； 问题：慢
//  + 只能迭代可枚举的，不论公有私有「一般我们要迭代的应该只有私有的」
//  + Symbol类型的属性迭代不到
/* Object.prototype.haha = 200;
Array.prototype.hehe = 100;
let arr = [10, 20, 30, 40];
arr['name'] = '哈哈';
arr[Symbol('xx')] = '呵呵'; */
// 需求：只迭代私有属性，而且是可枚举的（包含Symbol类型的）
// console.log(Object.keys(arr)); //返回结果是数组，含：当前对象所有可枚举、非Symbol类型、私有的属性
// console.log(Object.getOwnPropertyNames(arr)); //相比较于Object.keys，它可以获取不可枚举的
// console.log(Object.getOwnPropertySymbols(arr)); //获取对象Symbol类型的私有属性(含不可枚举的Symbol类型属性)
// console.log(Reflect.ownKeys(arr)); //直接获取的就是当前对象所有私有属性「含：可枚举和不可枚举的、也含Symbol和非Symbol类型的」
/* Object.keys(arr).concat(Object.getOwnPropertySymbols(arr)).forEach(key => {
    console.log(key, arr[key]);
}); */
/* for (let key in arr) {
    if (!arr.hasOwnProperty(key)) break; //使用for/in循环，建议必加这句话
    console.log(key);
} */


/*
面试题6：JS实现对货币格式化函数
例如：formatMoney(1234567890); //返回 '1,234,567,890'
*/
const formatMoney = function formatMoney(num) {
    num = +num;
    if (isNaN(num)) return num;
    /* return String(num).replace(/\d{1,3}(?=(\d{3})+$)/g, (val) => {
        return val + ',';
    }); */
    return String(num).replace(/(\d{1,3})(?=(\d{3})+$)/g, '$1,');
};
console.log(formatMoney(1234567890)); //"1,234,567,890"