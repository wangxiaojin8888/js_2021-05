(function () {
    let fallsBox = document.querySelector('#fallsBox'),
        columns = Array.from(fallsBox.querySelectorAll('.column')),
        loadMoreBox = document.querySelector('.loadMoreBox');
    let data = [];

    // 获取数据
    const query = function query() {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', './data.json', false);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                data = JSON.parse(xhr.responseText);
            }
        };
        xhr.send();
    };

    // 数据绑定「瀑布流效果」
    const render = function render() {
        // 修改数据:把图片真实宽高，按照渲染宽度230，计算出渲染的高度
        data = data.map(item => {
            let { width, height } = item;
            item.width = 230;
            item.height = Math.round(230 / (width / height));
            return item;
        });
        // 瀑布流:每三个为一组进行循环  
        //  + group:当前拿出的三条数据(一组数据)
        //  + columns:存储三列
        for (let i = 0; i < data.length; i += 3) {
            let group = data.slice(i, i + 3);
            if (i >= 3) {
                group.sort((a, b) => a.height - b.height); //升序
                columns.sort((a, b) => b.offsetHeight - a.offsetHeight); //降序
            }
            // 按照顺序插入盒子中
            group.forEach((item, index) => {
                let { pic, title, height, link } = item;
                let box = document.createElement('div');
                box.className = 'item';
                box.innerHTML = `<a href="${link}">
                    <div class="pic-box" style="height: ${height}px;">
                        <img src="${pic}" alt="">
                    </div>
                    <p class="desc-box">${title}</p>
                </a>`;
                columns[index].appendChild(box);
            });
        }
    };

    // 加载更多
    /* const loading = function loading() {
        let html = document.documentElement,
            body = document.body,
            n = 0;
        // window.onscroll事件：当页面滚动中，这个事件会在浏览器最快反应时间内（谷歌:5~7ms）就触发一次，这样导致方法执行的频率太高了，性能消耗大！！ => 我们需要降低它的触发频率 “函数节流”
        window.onscroll = utils.throttle(function () {
            if (n >= 3) return;
            let A = html.scrollTop,
                B = html.clientHeight,
                C = body.scrollHeight;
            // 到达底部条件
            if ((A + B + 50) >= C) {
                query();
                render();
                n++;
            }
        });
    }; */
    const loading = function loading() {
        // IntersectionObserver:ES6新增的，监听DOM元素和可视窗口交叉信息的
        let n = 0;
        let ob = new IntersectionObserver(changes => {
            // changes是一个数组，记录每一个监听的元素和可视窗口交叉的信息
            //   + isIntersecting 是否出现在视口中
            //   + boundingClientRect 基于getBoundingClientRect获取的结果
            //   + target 当前监听的这个DOM元素
            let item = changes[0];
            if (item.isIntersecting) {
                // 到底部了
                query();
                render();
                n++;
                if (n >= 3) ob.unobserve(item.target);
            }
        });
        ob.observe(loadMoreBox);
    };

    query();
    render();
    loading();
})();