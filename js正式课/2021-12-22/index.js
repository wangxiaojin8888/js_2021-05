(function () {
  /* 
    audio 的属性：
      audio.currentTime 指的是 当前的播放时间
      audio.duration指的是 音频的总共时间
      audio.volume 指的是音频的声音大小 0-1
      audio.paused  指的是 当前音频是否处在播放状态

      audio.play() 是让音频方法
      audio.pause() 是让音频暂停
  
  
  */
  let isPlay = false; // 用这个属性取控制播放还是暂停
  let middle = document.querySelector('.middle');
  let line = document.querySelector('.line_box')
  let panBox = document.querySelector('.pan_box')
  let audio = document.querySelector('#audio')
  let index = 3;
  let img = document.querySelector('.img_box img')
  let listBox = document.querySelector('.list_box')
  let box = document.querySelector('.box')
  let leftBtn = document.querySelector('.left')
  let rightBtn = document.querySelector('.right')
  let progressBar = document.querySelector('.progress_bar')
  let ps = listBox.getElementsByTagName('p');
  let timer; // 存储定时器
  let moveH = 0; // 记录歌词移动的高度
  let count = 0;// 记录唱歌句数

  middle.onclick = function () {
    isPlay = !isPlay;
    playok(isPlay)
  }
  function playok(isok) {
    if (isok) {
      // 若是播放状态  
      line.classList.add('active')
      panBox.classList.add('active')
      middle.classList.remove('icon-bofang-copy3')
      middle.classList.add('icon-zantingtingzhi')
      // 上边是改样式的
      audio.play() // 让音频放出声音的
      renderBar()
      isPlay = true
    } else {
      line.classList.remove('active')
      panBox.classList.remove('active')
      middle.classList.add('icon-bofang-copy3')
      middle.classList.remove('icon-zantingtingzhi')
      audio.pause()
      clearInterval(timer) // 
      isPlay = false

    }
  }
  // fetch 是浏览器新增的一个用来请求数据的api 返回值就是一个promise实例
  fetch('./music.json').then(res => res.json()).then(data => {
    // console.log(data)
    render(data[index], false)
    bindEvent(data)
  })

  function render(item, flag) {
    //  图片得换了  歌词得换了  音频地址换了
    img.src = item.img;
    listBox.innerHTML = renderLyric(item.lyric)
    audio.src = item.music
    box.style.background = `url(${item.bg})`
    box.style.backgroundSize = `cover`
    // 切换的时候 需要把所有的状态改成暂停
    // audio.play()
    playok(flag)
    moveH = 0// 切换的时候 让偏移量重置0
    listBox.style.transform = `translateY(${-moveH}px)`
    count = 0;//
  }

  function bindEvent(data) {
    leftBtn.onclick = function () {
      index--;
      if (index < 0) {
        alert('已经是第一首了')
        index = 0 // 不能是-1
      } else {
        clearInterval(timer)
        render(data[index], true)
      }

    }
    rightBtn.onclick = function () {
      index++;
      if (index > data.length - 1) {
        alert('已经是最后一首了')
        index = data.length - 1;// 不能是5
      } else {
        clearInterval(timer)
        render(data[index], true)
      }

    }
  }

  function renderBar() {
    timer = setInterval(() => {
      progressBar.style.width = audio.currentTime / audio.duration * 100 + '%'
      // 
      changeLyric()
    }, 20);
  }
  function changeLyric() {
    let t = audio.currentTime * 1000;
    // 找到最后一个比这个t小的那个p标签
    [...ps].forEach((item, index) => {
      let pt = item.dataset.time
      let next_pt = ps[index + 1] ? ps[index + 1].dataset.time : Date.now()
      item.classList.remove('active') // 先清除所有的 再给满足条件的加上
      if (pt < t && t < next_pt) {
        item.classList.add('active')
        // 让盒子上移 item的高度
        if (!item.flag) {
          // 频繁触发  我们要的是一次
          item.flag = true
          count++;
          if (count > 3) {
            moveH += parseFloat(getComputedStyle(item).height)
            listBox.style.transform = `translateY(${-moveH}px)`
          }
        }

      }
    })
  }
  function renderLyric(str) {
    let html = '';
    // [00:12.233]和咯哦的风格色弱 \n [01:22:111]sdgsdg
    str.replace(/\[(\d+):(\d+)\.(\d+)\]([^\[]+)/g, function (a, b, c, d, e) {
      // console.log(b, c, d, e)
      let t = b * 60 * 1000 + c * 1000 + d * 1
      html += `<p data-time="${t}">${e}</p>`
    })
    return html
  }
})()