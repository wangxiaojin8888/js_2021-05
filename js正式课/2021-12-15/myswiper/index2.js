(function () {
  let box = document.querySelector('.box');
  let ul = box.querySelector('ul')
  let lis = ul.getElementsByTagName('li');
  let dotBox = box.querySelector('.dot_box')
  let dots = dotBox.getElementsByTagName('span');
  let left = box.querySelector('.left_btn')
  let right = box.querySelector('.right_btn')

  let index = 0;// 控制现实的索引
  let max = 0;
  let timer;
  // 请求数据
  function getData() {
    return new Promise((res, rej) => {
      let xhr = new XMLHttpRequest;
      xhr.open('get', './data.json');
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          let data = JSON.parse(xhr.response)
          res(data)
        }
      }
      xhr.send()
    })
  }
  getData().then(data => {
    console.log(data)
    render(data)
    max = data.length - 1;// 索引是最大到4
    bindDotEvent()
    move()
  })
  //  封装render
  function render(data = []) {
    let ulStr = '', dotStr = '';
    // [...data].reverse().forEach((item, i) => {
    //   // [...data] 浅拷贝是为了不影响原有的data
    //   ulStr += `<li><img src='${item.pic}' title=${item.pic} /></li>`
    // })
    data.forEach((item, i) => {
      ulStr += `<li  class='${i == 0 ? 'active' : ''}'><img src='${item.pic}' title=${item.pic} /></li>`
      dotStr += `<span class='${i == 0 ? 'current' : ''}'></span>`
    })
    ul.innerHTML = ulStr;
    dotBox.innerHTML = dotStr
  }
  function changeLi(n) {
    [...lis].forEach(item => {
      item.classList.remove('active')
    })
    lis[n].classList.add('active')
  }
  function changeDot(n) {
    [...dots].forEach(item => {
      item.classList.remove('current')
    })
    dots[n].classList.add('current')
  }
  function next() {
    index++;
    if (index > max) {
      index = 0
    }
    changeLi(index)
    changeDot(index)
  }
  function prev() {
    index--;
    if (index < 0) {
      index = max
    }
    changeLi(index)
    changeDot(index)
  }
  function move() {
    timer = setInterval(() => {
      next()
    }, 1000);
  }
  box.onmouseenter = function () {
    clearInterval(timer)
  }
  box.onmouseleave = function () {
    move()
  }
  left.onclick = function () {
    prev()
  }
  right.onclick = function () {
    next()
  }
  function bindDotEvent() {
    [...dots].forEach((item, i) => {
      item.onclick = function () {
        index = i;
        changeLi(index)
        changeDot(index)
      }
    })
  }
  /* 
    结构是把图片定位摞起来
    控制要展示的那个图的opacity 和 zIndex即可
  */
  // new  MySwiper('.box')
  // new  MySwiper('.box1')
  // new  MySwiper('.box2')
  // new  MySwiper('.box3')
})()