(function () {
  let box = document.querySelector('.box');
  let ul = document.querySelector('ul');
  let arrowBox = document.querySelector('.arrow_box');
  let left = arrowBox.querySelector('.left_btn');
  let right = arrowBox.querySelector('.right_btn');
  let dots = box.querySelectorAll('span')

  let index = 0; // 记录显示图片的索引
  let timer;

  function changeDot(n) {
    dots.forEach(item => {
      item.classList.remove('current')
    })
    dots[n].classList.add('current')
  }

  function next() {
    index++;
    if (index > 5) {
      // 闪到第一张
      ul.style.transform = `translateX(0px)`
      ul.style.transition = 'all 0s'
      ul.offsetHeight // 逼迫浏览器把上边的设置渲染出来
      // 紧接着项第二张移动
      index = 1
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    } else {
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    }
    //  处理小点
    changeDot(index > 4 ? 0 : index)
  }
  function prev() {
    index--;
    if (index < 0) {
      //闪到最后一张
      ul.style.transform = `translateX(${-2000}px)`
      ul.style.transition = 'all 0s'
      ul.offsetHeight // 逼迫浏览器把上边的设置渲染出来
      index = 4;
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    } else {
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    }
    //  处理小点
    changeDot(index > 4 ? 0 : index)
  }
  function move() {
    timer = setInterval(next, 1000);
  }
  move()


  box.onmouseenter = function () {
    // 停止播放
    clearInterval(timer)
    arrowBox.classList.add('active')
  }
  box.onmouseleave = function () {
    move()
    arrowBox.classList.remove('active')
  }
  left.onclick = function () {
    prev()
  }
  right.onclick = function () {
    next()
  }
  dots.forEach((item, i) => {
    item.onclick = function () {
      index = i;
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
      changeDot(i)
    }
  })
})()