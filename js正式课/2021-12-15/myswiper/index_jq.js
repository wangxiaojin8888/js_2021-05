/* 
  $.ready(function(){})
  window.onload = function(){}

*/
$(function () {
  let index = 0;
  let max = 0;
  let timer;
  $.get('./data.json', function (data) {
    console.log(data)
    max = data.length - 1
    render(data)
    bindDotEvent()
    move()
  })
  function render(data = []) {
    let ulStr = '', dotStr = '';
    data.forEach((item, i) => {
      ulStr += `<li class='${i == 0 ? 'active' : ''}'><img src='${item.pic}' /></li>`
      dotStr += `<span class='${i == 0 ? 'current' : ''}'></span>`
    })
    $('.box ul').html(ulStr)
    $('.box .dot_box').html(dotStr)
  }
  function next() {
    index++;
    if (index > max) {
      index = 0
    }
    $('.box ul li').eq(index).addClass('active').siblings().removeClass('active')
    $('.box .dot_box span').eq(index).addClass('current').siblings().removeClass('current')
  }
  function prev() {
    index--;
    if (index < 0) {
      index = max
    }
    $('.box ul li').eq(index).addClass('active').siblings().removeClass('active')
    $('.box .dot_box span').eq(index).addClass('current').siblings().removeClass('current')
  }
  function move() {
    timer = setInterval(() => {
      next()
    }, 2000);
  }
  $('.box').on('mouseenter', function () {
    clearInterval(timer)
  })
  $('.box').on('mouseleave', function () {
    move()
  })
  $('.left_btn').on('click', function () {
    prev()
  })
  $('.right_btn').on('click', function () {
    next()
  })
  function bindDotEvent() {
    $('.dot_box span').on('click', function () {
      let n = $(this).index() // 获取当前操作的元素所在集合中的索引
      index = n;
      $('.box ul li').eq(index).addClass('active').siblings().removeClass('active')
      $(this).addClass('current').siblings().removeClass('current')
    })
  }
})