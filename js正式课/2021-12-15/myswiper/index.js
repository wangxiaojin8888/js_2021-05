(function () {
  let box = document.querySelector('.box')
  let ul = box.querySelector('ul')
  let dotBox = box.querySelector('.dot_box')
  let arrowBox = box.querySelector('.arrow_box')
  let left = arrowBox.querySelector('.left_btn')
  let right = arrowBox.querySelector('.right_btn')
  // let dots = dotBox.querySelectorAll('span')
  let dots = dotBox.getElementsByTagName('span') // DOM映射


  let index = 0;// 记录显示的是第几张的索引
  let max = 0;// 右边界
  let timer;
  /* 
    先获取数据
  */
  async function init() {
    let data = await getData();
    max = data.length;// 获取数据之后  更新右边界 
    // 拿到数据之后 执行render
    render(data)
    // 渲染完成之后 执行动画
    move()
    // 更新 变量dots
    // dots = dotBox.querySelectorAll('span')
    //dots更新之后 执行绑定的操作
    bindDotEvent()
  }
  init()
  /*  getData().then(data => {
     max = data.length;// 获取数据之后  更新右边界 
     // 拿到数据之后 执行render
     render(data)
     // 渲染完成之后 执行动画
     move()
     // 更新 变量dots
     dots = dotBox.querySelectorAll('span')
     //dots更新之后 执行绑定的操作
     bindDotEvent()
   }) */
  function getData() {
    return new Promise((res, rej) => {
      let xhr = new XMLHttpRequest;
      xhr.open('get', './data.json')
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          let data = JSON.parse(xhr.response)
          res(data)
        }
      }
      xhr.send()
    })



  }

  function render(ary) {
    // 根据数据渲染视图的
    //  渲染俩 拼接li扔到ul盒子里   拼接span扔到dotBox里边
    let str1 = '', str2 = '';
    ary.forEach((item, i) => {
      str1 += `<li> <img src='${item.pic}'/></li>`
      str2 += `<span class='${i == 0 ? 'current' : ''}'></span>`
    })
    str1 += `<li><img src='${ary[0].pic}'/></li>`
    ul.innerHTML = str1;
    dotBox.innerHTML = str2;
    // max 对应的是数据的个数， 但是我们补过一张图，所以得用max+1
    ul.style.width = (max + 1) * 400 + 'px'

  }

  //  封装一个move 让图片动起来 什么时候才能执行？
  // 得再数据渲染完成之后执行
  function move() {
    timer = setInterval(() => {
      next()
    }, 2000);
  }

  function changeDot(n) {
    // 这个函数是负责让对应的索引的点 变成蓝
    [...dots].forEach(item => {
      item.classList.remove('current')
    })
    dots[n].classList.add('current')
  }

  //  让图片往下一张走
  function next() {
    //  transition 是过度； animation是动画； transform是位移
    index++ // 控制的显示的张的索引
    if (index > max) { // max:5
      // 闪到第一张
      ul.style.transform = `translateX(0px)`
      ul.style.transition = `all 0s`
      ul.offsetHeight

      // 紧接着向第二张移动
      index = 1;
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = `all 0.3s`
    } else {
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = `all 0.3s`
    }

    changeDot(index > max - 1 ? 0 : index)

  }
  // 让图片向上一张移动
  function prev() {
    index--;
    if (index < 0) {
      // 闪到最后一张
      ul.style.transform = `translateX(${-max * 400}px)`
      ul.style.transition = 'all 0s'
      ul.offsetHeight
      // 紧接着向倒数第二张移动
      index = max - 1;
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    } else {
      ul.style.transform = `translateX(${-400 * index}px)`
      ul.style.transition = 'all 0.3s'
    }
    changeDot(index > max - 1 ? 0 : index)
  }

  box.onmouseenter = function () {
    // 鼠标进入 停止定时器
    clearInterval(timer)
    //  让左右按钮出现  给arrow盒子添加active
    arrowBox.classList.add('active')
  }
  box.onmouseleave = function () {
    // 鼠标离开 重启定时器
    move()
    // classList H5新增的一个操作元素类名的属性
    arrowBox.classList.remove('active')
  }
  right.onclick = function () {
    // 点击右按钮 就是要显示要一张图了
    next()
  }
  left.onclick = function () {
    prev()
  }

  // 给点绑定点击事件
  function bindDotEvent() {
    // dots更新之后才能执行bindDotEvent
    [...dots].forEach((item, i) => {
      item.onclick = function () {
        // 点击原点的时候  得让index 变成点击的那个点对应的图的索引
        //  还得让 对应的图展示出来
        // 还得让对应的点 变蓝
        index = i;
        ul.style.transform = `translateX(${-400 * index}px)`
        ul.style.transition = 'all 0.3s'
        changeDot(i)
      }
    })
  }



})()