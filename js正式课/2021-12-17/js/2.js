/*
 promise.then(onfulfilled,onrejected)
   @1 如果此时已经知道 promise 的状态
     + 也不是立即通知onfulfilled/onrejected执行
     + 它是一个异步的微任务「先放入WebAPI中监听 -> 发现可以执行，继续放入到EventQueue中排队等着 ->当同步代码执行完，它才可以执行」

   @2 如果此时还不知道 promise 的状态
     + 把onfulfilled/onrejected存储起来，后期知道状态后再执行
     + 后期resolve/reject方法执行，会同步立即修改实例状态和值，但通知预先存储的方法执行，是异步微任务

 await只对当前上下文中await下面的代码产生影响，对其它上下文中的代码执行，不会产生任何的作用
    + await会把当前上下文中，它下面的代码设置为异步的微任务「进入到WebAPI中监听」；当await后面的promise实例状态为成功，之前的微任务才能确定为可执行「进入到EventQueue中排队等待」；当同步代码都执行完，主线程空闲下来，再把其拿到栈中去执行！！
 */

/* let body = document.body;
body.addEventListener('click', function () {
    Promise.resolve().then(() => {
        console.log(1);
    });
    console.log(2);
});
body.addEventListener('click', function () {
    Promise.resolve().then(() => {
        console.log(3);
    });
    console.log(4);
}); */

/* async function async1() {
    console.log('async1 start');
    await async2();
    console.log('async1 end');
}
async function async2() {
    console.log('async2');
}
console.log('script start');
setTimeout(function () {
    console.log('setTimeout');
}, 0)
async1();
new Promise(function (resolve) {
    console.log('promise1');
    resolve();
}).then(function () {
    console.log('promise2');
});
console.log('script end'); */


/* (async function () {
    console.log(1);
    await Promise.resolve('OK');
    console.log(2);
    await Promise.resolve('TURE');
    console.log(3);
})();
console.log(4);
// 1 4 2 3 */

/* let p1 = new Promise(resolve => {
    setTimeout(() => {
        console.log(1);
        resolve(100);
        console.log(2);
    }, 1000);
});
console.log(3);
p1.then(value => {
    console.log(4);
});
console.log(5);
// 3 5 {等待1000ms} 1 2 4 */

/* let p1 = new Promise(resolve => {
    console.log(1);
    resolve(100); //立即把实例状态修改为fulfilled，值改为100
    console.log(2);
});
console.log(3);
p1.then(value => {
    console.log(4);
});
console.log(5);
// 1 2 3 5 4 */
