/*
 DOM0级事件绑定  box.onclick=function(){}
   + 给元素对象的某个事件型私有属性赋值
   + 只能给当前元素的某个事件行为绑定一个方法，绑定多个也是以最后一个为主
   + 如果不具备某一种事件的私有属性，是无法实现事件绑定的
     例如：DOMContentLoaded事件(DOM TREE 生成完成后触发)
     window.onDOMContentLoaded=function... 这样是无效的，因为并不具备onDOMContentLoaded属性
   + 操作简单，性能也相对好一些
     元素.onxxx=function...
     元素.onxxx=null

 DOM2级事件绑定  EventTarget.prototype.addEventListener/removeEventLister
   box.addEventListener(事件类型,方法,捕获/冒泡)
   + 原理：基于浏览器事件池机制完成的，基于addEventListener就是向事件池中加入一条记录「含：哪一个元素、哪一个事件类型、执行哪个方法、传播方式...等信息」；当事件行为触发，浏览器会把事件池中所有符合要求的方法都拿出来依次执行...
   + 可以给当前元素的某个事件行为，绑定多个不同的方法，事件触发，方法会按照绑定的顺序依次执行！！
   + 所有浏览器支持的事件行为都可以基于这种方式完成事件绑定

但是DOM2事件绑定只支持浏览器内置的事件行为处理(或者说浏览器内置的事件池，只支持这些内置事件)，如果想自己定义一些事件行为，也向上面一样进行管理和触发，此时我们就需要 “自定义事件池”，而且把放入事件池、移除事件池、通知事件池中的方法执行等机制都自己都实现一遍！ => 发布订阅设计模式
   + 所有设计模式的诞生，都仅仅是为了优化和有效管理代码，并不是非用不可；不用，需求可以想办法实现，只是代码的开发和维护都不便利，而使用设计模式，会让代码更好的维护和管理！！
 */
(function () {
    // 自定义事件池
    let listeners = {};

    // 向事件池中加入自定义事件&方法
    const on = function on(name, func) {
        if (!listeners.hasOwnProperty(name)) {
            // 从来没有：则加入一个自定义事件类型
            listeners[name] = [func];
            return;
        }
        // 自定义事件已经存在：获取现有数组，加入新的方法「不要忘记去重」
        let arr = listeners[name];
        if (arr.indexOf(func) > -1) return;
        arr.push(func);
    };

    // 从事件池中移除方法
    const off = function off(name, func) {
        if (!listeners.hasOwnProperty(name)) return;
        let arr = listeners[name],
            index = arr.indexOf(func);
        if (index === -1) return;
        arr.splice(index, 1);
    };

    // 通知指定自定义事件中的方法执行
    const emit = function emit(name, ...params) {
        if (!listeners.hasOwnProperty(name)) return;
        let arr = listeners[name];
        arr.forEach(item => {
            if (typeof item === "function") {
                item(...params);
            }
        });
    };

    // 暴露API
    const sub = {
        on,
        off,
        emit
    };
    if (typeof window !== "undefined") window.$sub = sub;
    if (typeof module === "object" && typeof module.exports === "object") module.exports = sub;
})();

setTimeout(() => {
    $sub.emit('黄蓉结婚', 100, 200);
}, 1000);

setTimeout(() => {
    $sub.emit('黄蓉结婚', 2000, 3000);
}, 3000);

const fn1 = (x, y) => {
    console.log('邵宇是我的司仪', x, y);
};
$sub.on('黄蓉结婚', fn1);

const fn2 = (x, y) => {
    console.log('杨妮是我的伴娘', x, y);
    // 把计划1和2移除掉
    $sub.off('黄蓉结婚', fn1);
    $sub.off('黄蓉结婚', fn2);
};
$sub.on('黄蓉结婚', fn2);

const fn3 = () => {
    console.log('颖辉回来跳舞');
};
$sub.on('黄蓉结婚', fn3);

const fn4 = () => {
    console.log('江江会来参加婚礼');
};
$sub.on('黄蓉结婚', fn4);

const fn5 = () => {
    console.log('山森也要跟着来');
};
$sub.on('黄蓉结婚', fn5);






/* // 事件：黄蓉元旦结婚，嫁给靖哥哥
// 1. 请杨妮做伴娘
// 2. 请邵宇做司仪
// 3. 请颖辉跳舞
// ...
const plain = {
    // ....
    on(){},
    off(){},
    fire(){}
};

// A.js
const fn1 = () => { };
const fn2 = () => { };
plain.on(fn1);
plain.on(fn2);

//B.js
setTimeout(() => {
    plain.fire();
}, 1000);

//C.js
const fn3 = () => { };
plain.on(fn3); */