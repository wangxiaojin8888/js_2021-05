/*
 Promise是ES6新增的内置类，它基于“承诺者”设计模式，有效管理异步编程，解决回调地狱问题
    如何创建实例？实例具备哪些私有属性？
      let p1 = new Promise([executor]);
       + [executor]必须是一个函数，如果不是函数则报错
       + 而且new Promsie的时候，会把[executor]立即执行，并且Promise内部为[executor]传递两个实参，我们一般都基于 resolve/reject 两个形参变量来接收传递的实参信息！！实参值是两个函数！！
        + resolve([value])：把实例的状态改为fulfilled，值改为[value]
        + reject([reason])：把实例的状态改为rejected，值改为[reason]
        + [executor]函数中的代码，自上而下执行报错，也会修改实例的状态为rejected，值是报错原因
        PS：一但实例的转态从pending改为别的状态，则不能再次修改为其他状态了
       ----
       实例具备的私有属性：
        + [[PromiseState]]: "pending"  实例状态  pending准备  fulfilled成功  rejected失败
        + [[PromiseResult]]: undefined 实例值(结果)

    Promise.prototype提供一些供实例调用的公共属性方法 「实例.xxx()」
      + Symbol(Symbol.toStringTag): "Promise"
      + then
      + catch
      + finally

    本身做为一个普通对象，拥有一些静态私有属性方法 「Promise.xxx()」
      + all
      + any
      + race
      + reject：Promise.reject(xxx) 快速创建一个状态是rejected失败，值是xxx的promise实例
      + resolve：Promise.resolve(xxx) 创建一个状态是fulfilled成功，值是xxx的promise实例

    promise.then([onfulfilled],[onrejected])
      + [onfulfilled]方法会在promise实例状态为fulfilled的时候触发执行
      + [onrejected]方法会在promise实例状态为rejected的时候触发执行
      + 都会把实例的值(结果)传递给两个函数
      -----
      每一次执行then方法，都会返回一个全新的promise实例「例如：p2」
        新实例的状态和值，由上一个“实例.then”传递进来的[onfulfilled]或者[onrejected]方法执行决定
        + 方法执行如何报错了，则新实例「p2」状态就是失败，值就是报错原因
        + 如果方法执行并没有报错，则再看方法执行的返回值：
          + 如果返回结果是一个promise实例「例如：@P」: @P的状态和值，直接影响p2的状态和值「一样的」
          + 如果返回的并不是promise实例 : 则新实例(p2)的状态是fulfilled，值是return返回的值「如果么有写返回值，则promise实例的值是undefined」
      Promise之所以这样设计(.then返回新的的promise实例)，就是为了实现“THEN链”「.then().then()...」
      -----
      “THEN链”具备“穿透/顺延”机制：执行then方法的时候，[onfulfilled]或者[onrejected]可以不传，如果不传递，则顺延到下一个等同状态的方法上！！

    promise.catch([onrejected])：等同于promise.then(null,[onrejected])
      真实项目中，我们一般都是then中只传递onfulfilled方法，在then链的末尾加一个catch处理失败的事情，这样不论哪一级出现了失败的promise实例，都会顺延到最后的catch中！！
      如果出现失败的promise实例，而我们并没有基于catch等方法对其进行处理，则会在控制台“抛红”，不影响代码的执行，但是不好看！！

    Promise.all/any/race([promises])：执行方法，传递包含零到多个promise实例的集合（promises），返回的结果是一个全新的promise实例「别名：@P」！！promises集合中，如果某项并不是promise实例，则会把其默认转换为状态为成功，值就是本身的实例！！
      + all ：集合中每一项都成功，@P才是成功（值：和原始集合相同的顺序，记录每一项成功的结果），只要有一项是失败，则@P就是失败（值：当前失败项的值）
      + any ：集合中只要有一项成功，@p就是成功的（值：成功项的值），只有都是失败的，@P才是失败
      + race ：集合中谁先确定成功/失败状态的，则@P就以谁的结果为主（哪怕是失败的）

    Promise+Generator的语法糖：async/await  让promise代码用起来更简单
      async对函数进行修饰
        + 让函数的返回值变为一个promise实例
        + 想在函数中使用await「想要使用await，那么函数必须经过async的修饰」

      let value=await [promise];
        + await后面必须跟一个promise实例，如果不是，则默认转换为状态成功、值是本身的实例
          await 10; -> await Promise.resolve(10);
        + await会监听后面promise实例的状态
          + 只有状态是fulfilled成功，才会把实例值赋值给value，并且当前上下文中，await下面代码，才会继续执行「或者理解为：“等待”后面的实例是成功态，代码再继续执行」
          + 如果后面promise实例状态是失败的，则啥都不处理了
        + await异常处理：如果后面的promise实例是失败，我们不做任何处理，控制台就会抛红
          基于：try catch 即可
 */

/* // try尝试执行某些代码，如果执行报错，则catch会捕捉异常信息，但是控制台不会抱错了，下面代码可以继续执行
try {
    console.log(a);
} catch (err) {
    console.log(err);
} finally {
    // 不论是否报错都会执行
}
let n = 12;
console.log(n); */

/* console.log(a); //Uncaught ReferenceError: a is not defined 代码运行报错，后面不在执行
let n = 12;
console.log(n); */


/* const http1 = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject('第一个请求');
        }, 2000);
    });
};
const http2 = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('第二个请求');
        }, 1000);
    });
};
(async function () {
    try {
        console.log(1);
        let value = await http1();
        console.log(value);
        value = await http2();
        console.log(value);
        console.log(2);
    } catch (err) {
        // ...
    }
})(); */

/* async function fn() {
    console.log(a);
    return 10;
}
console.log(fn()); //promise实例 状态：rejected 值：报错信息 */

/* async function fn() {
    return 10;
}
console.log(fn()); //promise实例 状态：fulfilled 值：10 */

//==========================================
/* let p1 = Promise.resolve(1);
let p2 = Promise.reject(2);
let p3 = new Promise(resolve => {
    setTimeout(() => {
        resolve(3)
    }, 2000);
});
let p4 = new Promise(resolve => {
    setTimeout(() => {
        resolve(4)
    }, 1000);
});
let p5 = 5; //-> 转换为 Promise.resolve(5)

let p = Promise.any([p3, p1, p4]);
p.then(value => {
    console.log('成功：', value);
}).catch(reason => {
    console.log('失败：', reason);
}); */

//==========================================
/* Promise.resolve(100).then(value => {
    console.log('成功：', value);
    return Promise.reject(value / 10);
}).then(value => {
    console.log('成功：', value);
    return value / 10;
}).then(value => {
    console.log('成功：', value);
    return value / 10;
}).catch(reason => {
    console.log('失败：', reason);
}); */

/* Promise.reject(100)
    .then(/!* value => {
        return value;
    } *!/ /!* ,reason=>{
        throw reason;
    } *!/)
    .then(value => {
        console.log('成功：', value);
    }, reason => {
        console.log('失败：', reason);
    });
*/

/* new Promise(resolve => {
    resolve(100);
})
    .then(value => {
        console.log('成功：', value); //=>成功：100
        return Promise.reject(value / 10); //返回失败实例、值10
    }, reason => {
        console.log('失败：', reason);
        return Promise.resolve(reason * 10);
    })
    .then(value => {
        console.log('成功：', value);
        console.log(a);
        return value / 10;
    }, reason => {
        console.log('失败：', reason); //=>失败 10
        return reason * 10;
    })
    .then(value => {
        console.log('成功：', value); //=>成功 100
    }, reason => {
        console.log('失败：', reason);
    }); */


/* let p1 = new Promise((resolve, reject) => {
    // 一般在这里都写异步操作的代码
    setTimeout(() => {
        resolve(100);
    }, 1000);
});
let p2 = p1.then(value => {
    console.log('成功：', value);
}, reason => {
    console.log('失败：', reason);
});
p2.then(value => {
    console.log('成功：', value);
}, reason => {
    console.log('失败：', reason);
}); */

//============================================
/* // 基于Promsie设计模式可以解决回调地狱
const http1 = () => {
    return new Promise(resolve => {
        $.ajax({
            url: '/api1',
            success: resolve
        });
    });
};
const http2 = () => {
    return new Promise(resolve => {
        $.ajax({
            url: '/api2',
            success: resolve
        });
    });
};
const http3 = () => {
    return new Promise(resolve => {
        $.ajax({
            url: '/api3',
            success: resolve
        });
    });
};

(async function () {
    let value = await http1();
    console.log('第一个请求成功：', value);

    value = await http2();
    console.log('第二个请求成功：', value);

    value = await http3();
    console.log('第三个请求成功：', value);
})();

http1()
    .then(value => {
        console.log('第一个请求成功：', value);
        return http2();
    })
    .then(value => {
        console.log('第二个请求成功：', value);
        return http3();
    })
    .then(value => {
        console.log('第三个请求成功：', value);
    }); */


/* // ajax请求串行：多个请求之间存在依赖，上一个请求成功，才能发送下一个请求 「如果是基于回调函数的方式管理，很容易产生回调地狱」
$.ajax({
    url: '/api1',
    success(value) {
        console.log('第一个请求成功：', value);

        $.ajax({
            url: '/api2',
            success(value) {
                console.log('第二个请求成功：', value);

                $.ajax({
                    url: '/api3',
                    success(value) {
                        console.log('第三个请求成功：', value);
                    }
                });
            }
        });
    }
}); */

/* // ajax请求并行：多个请求之间没有任何依赖，可以同时发送多个请求，谁先请求回来先处理谁「特殊需求：监听所有请求都成功，统一做一些事情」
let n = 0;
const handle = function handle() {
    // ...请求都成功处理的事情
};
$.ajax({
    url: '/api1',
    success(value) {
        console.log('第一个请求成功：', value);
        n++;
        if (n === 3) handle();
    }
});
$.ajax({
    url: '/api2',
    success(value) {
        console.log('第二个请求成功：', value);
        n++;
        if (n === 3) handle();
    }
});
$.ajax({
    url: '/api3',
    success(value) {
        console.log('第三个请求成功：', value);
        n++;
        if (n === 3) handle();
    }
}); */