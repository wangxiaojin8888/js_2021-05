/* ready 和  原生的 onload */
/* 
  $.ready(function(){})
  window.onload = function(){} 
  onload 页面加载完成  ready 是DOM节点加载完成
*/
$(function () {
  let flag = false;
  // 获取数据
  function getData() {
    $.get('./data.json', function (data) {
      render(data)
      flag = false;
      lazyLoad()
    })
  }
  function render(data) {
    data = data.map(item => {
      item.width = 230;
      item.height = item.height * (230 / item.width)
      return item
    })
    data.forEach(item => {
      let { link,
        height,
        pic,
        title } = item;
      let box = document.createElement('div');
      box.className = 'item';
      let str = `<a href="${link}">
                    <div class="pic-box" style="height: ${height}px;">
                        <img src="" data-src="${pic}" alt="">
                    </div>
                    <p class="desc-box">${title}</p>
                </a>`
      box.innerHTML = str;
      let columnBox = [...$('.column')]
      columnBox = columnBox.sort((a, b) => {
        return a.clientHeight - b.clientHeight
      })
      $(columnBox[0]).append(box)
    })
  }
  getData()

  let h = document.documentElement.clientHeight || document.body.clientHeight;
  window.onscroll = function () {
    // console.log($('.loadMoreBox').offset())
    if ($('.loadMoreBox').offset().top < document.documentElement.scrollTop + h) {
      if (flag) return;
      flag = true
      getData()
    }
    lazyLoad()
  }

  function lazyLoad() {
    [...$('.item')].forEach(item => {
      if ($(item).offset().top < document.documentElement.scrollTop + h) {
        //  图片露出
        let $img = $(item).find('img')
        $img.attr('src', $img.attr('data-src'))
        $img.css('opacity', 1)
      }
    })
  }
})