(function () {
    let fallsBox = document.querySelector('#fallsBox'),
        columns = Array.from(fallsBox.querySelectorAll('.column')),
        loadMoreBox = document.querySelector('.loadMoreBox'),
        imgBoxs = null;
    let data = [];

    // 获取数据
    const query = function query() {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', './data.json', false);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                data = JSON.parse(xhr.responseText);
            }
        };
        xhr.send();
    };

    // 数据绑定「瀑布流效果」
    const render = function render() {
        data = data.map(item => {
            let { width, height } = item;
            item.width = 230;
            item.height = Math.round(230 / (width / height));
            return item;
        });
        for (let i = 0; i < data.length; i += 3) {
            let group = data.slice(i, i + 3);
            if (i >= 3) {
                group.sort((a, b) => a.height - b.height);
                columns.sort((a, b) => b.offsetHeight - a.offsetHeight);
            }
            group.forEach((item, index) => {
                let { pic, title, height, link } = item;
                let box = document.createElement('div');
                box.className = 'item';
                box.innerHTML = `<a href="${link}">
                    <div class="pic-box" style="height: ${height}px;">
                        <img src="" data-src="${pic}" alt="">
                    </div>
                    <p class="desc-box">${title}</p>
                </a>`;
                columns[index].appendChild(box);
            });
        }
        // 获取图片所在的盒子 & 延迟加载
        imgBoxs = fallsBox.querySelectorAll('.pic-box:not([loaded])');
        lazying();
        window.onscroll = utils.throttle(lazying);
    };

    // 加载更
    const loading = function loading() {
        let n = 0;
        let ob = new IntersectionObserver(changes => {
            let item = changes[0];
            if (item.isIntersecting) {
                query();
                render();
                n++;
                if (n >= 3) ob.unobserve(item.target);
            }
        });
        ob.observe(loadMoreBox);
    };

    // 图片延迟加载
    const lazying = function lazying() {
        [].forEach.call(imgBoxs, (imgBox) => {
            // 处理过的无需再次处理
            if (imgBox.getAttribute('loaded') === 'done') return;
            let A = imgBox.getBoundingClientRect().bottom,
                B = document.documentElement.clientHeight;
            if (A <= B) {
                // 完全出现在视口中
                let img = imgBox.querySelector('img');
                img.src = img.getAttribute('data-src');
                img.onload = () => {
                    // 真实图片加载成功
                    img.style.opacity = 1;
                };
                // 已经处理过的，我们设置一个自定义属性
                imgBox.setAttribute('loaded', 'done');
            }
        });
    };

    query();
    render();
    loading();
})();