(function () {
    let fallsBox = document.querySelector('#fallsBox'),
        columns = Array.from(fallsBox.querySelectorAll('.column')),
        loadMoreBox = document.querySelector('.loadMoreBox'),
        imgBoxs = null;
    let data = [];

    // 获取数据
    const query = function query() {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', './data.json', false);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                data = JSON.parse(xhr.responseText);
            }
        };
        xhr.send();
    };

    // 数据绑定「瀑布流效果」
    const render = function render() {
        data = data.map(item => {
            let { width, height } = item;
            item.width = 230;
            item.height = Math.round(230 / (width / height));
            return item;
        });
        for (let i = 0; i < data.length; i += 3) {
            let group = data.slice(i, i + 3);
            if (i >= 3) {
                group.sort((a, b) => a.height - b.height);
                columns.sort((a, b) => b.offsetHeight - a.offsetHeight);
            }
            group.forEach((item, index) => {
                let { pic, title, height, link } = item;
                let box = document.createElement('div');
                box.className = 'item';
                box.innerHTML = `<a href="${link}">
                    <div class="pic-box" style="height: ${height}px;">
                        <img src="" data-src="${pic}" alt="">
                    </div>
                    <p class="desc-box">${title}</p>
                </a>`;
                columns[index].appendChild(box);
            });
        }
        imgBoxs = fallsBox.querySelectorAll('.pic-box:not([loaded])');
        lazying();
    };

    // 加载更多
    const loading = function loading() {
        let n = 0;
        let ob = new IntersectionObserver(changes => {
            let item = changes[0];
            if (item.isIntersecting) {
                // 等待一秒再加载更多数据
                setTimeout(() => {
                    query();
                    render();

                    // 无数据处理
                    n++;
                    if (n >= 5) {
                        ob.unobserve(item.target);
                        loadMoreBox.innerHTML = `--- 已经到底了 ---`;
                    }
                }, 1000);
            }
        });
        ob.observe(loadMoreBox);
    };

    // 图片延迟加载
    const ob = new IntersectionObserver(changes => {
        changes.forEach(item => {
            // item:每一个监听的盒子和可视窗口交叉的信息
            let { isIntersecting, target } = item;
            if (isIntersecting) {
                // 当前盒子完全出现在视口中
                let img = target.querySelector('img');
                img.src = img.getAttribute('data-src');
                img.onload = () => {
                    img.style.opacity = 1;
                };
                target.setAttribute('loaded', 'done');
                ob.unobserve(target);
            }
        });
    }, { threshold: [1] });
    const lazying = function lazying() {
        [].forEach.call(imgBoxs, imgBox => {
            ob.observe(imgBox);
        });
    };

    query();
    render();
    loading();
})();