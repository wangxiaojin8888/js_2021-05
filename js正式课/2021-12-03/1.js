/* let obj = {
    x: 1, y: [10, 20]
};
let obj2 = obj;
let obj3 = {
    ...obj2
};
obj2.x = 100;
obj2.y[1] = 30;
obj3.x = 200;
obj3.y[2] = 40;
obj = obj3.y = {
    x: 0, y: [1, 2]
};
console.log(obj, obj2, obj3); */

//===================================
/*
  思路一：利用==比较会进行数据类型转换，而把对象转换为数字，会按照以下步骤处理
    对象转数字/字符串：Number(对象)/String(对象)
      1. 首先看对象是否存在Symbol.toPrimitive属性方法，如果有则执行这个方法
         Number(对象) => 对象[Symbol.toPrimitive]('number')
         String(对象) => 对象[Symbol.toPrimitive]('string')
         对象+1 => 对象[Symbol.toPrimitive]('default')
      2. 没有这个方法，则再次执行“对象.valueOf()”获取原始值；如果获取的结果是原始值，则以这结果为主；
      3. 如果获取的不是原始值，则继续执行“对象.toString()”方法
      4. 如果是需要转换为数字，则把获取的字符串再转换为数字
*/
/*
let a = {
    i: 0,
    [Symbol.toPrimitive]() {
        // this -> a
        return ++this.i;
    }
};
// a==1  =>  Number(a)==1
//   a[Symbol.toPrimitive]()
//   a.valueOf()
//   a.toString()
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
}
*/

/* let a = [1, 2, 3];
a.toString = a.shift;
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
} */


/*
 方案二：在全局上下文“a==1”，获取a的值(或者说:获取window.a的值)；然后利用数据劫持，在获取window.a属性值的时候，触发get函数；而在函数中返回需要的1/2/3即可！！
 */
/* let i = 0;
Object.defineProperty(window, 'a', {
    get() {
        return ++i;
    }
});
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
} */


//==================
/*
 parseInt([value])
   只能处理字符串，所以必须保证[value]是个字符串(不是就要先基于String([value])转换为字符串)
   从字符串左边第一个字符开始查找，把找到的有效数字字符转换为数字，直到遇到一个非有效数字字符结束！如果一个有效字符都没有找到，结果是NaN！
   它和Number([value])的处理规则完全不一样！！

 parseInt([value],[radix])：假设[radix]是2，从[value]字符串左侧开始查找，找到符合2进制的内容(遇到一个不符合的则结束查找)，把找到的2进制内容转换为10进制!!
   [radix]如果不写(或者写0)，默认是10（特殊：如果value字符串是以“0x”开始的，则默认是16）
   [radix]有效范围是2~36之间，如果设置的值不在这个区间，则结果返回NaN

 扫盲：N进制转10进制 => “按权展开求和”
 */
/* console.log(parseInt('1011233')); //-> parseInt('1011233',10) -> '1011233' -> 1011233
console.log(parseInt('1011233', 2)); //-> '1011' 把这个二进制值转换为十进制
// 1*2^3+0*2^2+1*2^1+1*2^0 = 8+0+2+1 = 11 */

/* arr = arr.map(function (item,index) {
    // 数组有几项，就迭代几次，每一次迭代，都会把回调函数执行，而且还会给回调函数传递一些值
    //   + item:当前迭代的这一项
    //   + index:当前迭代这一项的索引
    // 回调函数中返回的是个啥，就把数组中当前迭代这一项替换成啥「原始数组不变，以新数组返回」
    // 12.5,0   100,1   0013,2 ...
    return item * index;
}); */

// let arr = [12.5, 100, 0013, '27px', 456];
// arr = arr.map(parseInt);
// console.log(arr);
/*
  parseInt(12.5,0)
     parseInt('12.5',10) -> '12' -> 12
  parseInt(100,1) -> NaN
  parseInt(0013,2)
     以0开始的“数字”是八进制的，浏览器要把其先转换为十进制，然后再做其它的处理
        0*8^3+0*8^2+1*8^1+3*8^0 = 0+0+8+3 = 11
     parseInt('11',2) -> '11' 把二进制转换为十进制
        1*2^1+1*2^0 = 2+1 = 3
  parseInt('27px',3)
     -> '2' 把三进制转换为十进制
     2*3^0 = 2
  parseInt(456,4)
     parseInt('456',4) -> '' -> NaN
 */

// '112.211'
// 1*3^2+1*3^1+2*3^0 = 9+3+2 = 14
// 2*3^-1+1*3^-2+1*3^-3 = 18/27+3/27+1/27 = 22/27

// 进制转换：十进制转二进制 => [number value].toString(2)
//  整数：除以2，得到一个商数和余数，再拿商数继续除以2...直到商为0，按照返序，把所有余数拼接起来即可
//  小数：乘以2取整，直到没有小数为止「这样很容易出现无限循环的情况，但是计算机肯定会有最长位数限制(例如:64位)，超出的部分会自动截取，这样导致值是不准确的 => “计算机浮点数计算精准度问题”」
/* 
0.1 十进制转2进制
  0.1*2 = 0.2 -> 0
  0.2*2 = 0.4 -> 0
  0.4*2 = 0.8 -> 0
  0.8*2 = 1.6 -> 1
  0.6*2 = 1.2 -> 1
  0.2*2 = 0.4 -> 0
  ....
  00011001100110011...
 */
// console.log(0.1 + 0.2 === 0.3); //false
// 0.1 + 0.2 = 0.30000000000000004
// 0.1 + 0.7 = 0.7999999999999999
// 0.2 + 0.4 = 0.6000000000000001
// 0.1 + 0.3 = 0.4
// JS中所有数据类型值，在计算机底层都是按照“二进制”来存储的；这种情况下，部分浮点数存储的二进制值本身就是失去精准度的；数学运算也是按照二进制来运算，把运算的结果最后转换为十进制，这样其实有可能也是丢失了一些精准度；

/* 
// 懒人方案
const plus = function plus(n, m) {
    n = +n;
    m = +m;
    return +((n + m).toFixed(3));
}; 
*/

// 浮点数计算存在精准度丢失问题，那么我们就把每个数字乘以“系数”变为整数运算，最后再除以系数即可
const coeffic = function coeffic(num) {
    num += '';
    let [, char = ''] = num.split('.');
    return Math.pow(10, char.length);
};
const plus = function plus(n, m) {
    n = +n;
    m = +m;
    let max = Math.max(coeffic(n), coeffic(m));
    return (n * max + m * max) / max;
};

// ES6中的解构赋值：快速获取对象/数组中的某些内容;让左侧出现和右侧相同的结构，然后定义一些变量，快速获取和右侧值相对应位置的内容；
// let arr = [10, 20, 30];
// let [a, ...b] = arr;
// console.log(a, b);
/* let a = arr[0],
    b = arr.slice(1); */

//-----
// let [, a] = arr;
// console.log(a);

//-----
// let [a, b, c] = arr;
/* let a = arr[0],
    b = arr[1],
    c = arr[2]; */
// console.log(a, b, c);