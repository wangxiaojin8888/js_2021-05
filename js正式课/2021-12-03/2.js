/*
var x = 2;
function func() {
    return function (y) {
        console.log(y + (++x));
    }
}
var f = func(1);
f(2);
func(3)(4);
f(5);
console.log(x);
*/



//=================
// 匿名函数：自执行函数、函数表达式(把函数作为值赋值给变量或者事件绑定)、回调函数...
// 匿名函数具名化：原本是没有函数名字的，我们非要设置一个「和真正的实名函数还是有区别的」
//   @1 设置的名字在函数所处上下文(作用域)中未被声明过，所以直接拿来使用，获取的不是函数本身，而是会报错「好处：即使自己设置了名字，也不会和所处上下文中的其他变量冲突」
//   @2 但是在自己执行产生的私有上下文中，这个函数名存储的就是“当前函数本身”；而且修改它的值是没用的；
//   @3 但是如果在私有上下文中，我们基于任何一种方式声明过这个名字，那么他是私有变量，存储的值也是我们手动赋的值，和函数本身就没有任何的关系了！！
// 优势：更符合官方语法规范、可以让匿名函数实现递归操作
/* "use strict";
let result = (function (x) {
    if (x === 10) return x;
    return x + arguments.callee(x + 1); //Uncaught TypeError: 'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them
})(1);
console.log(result); */

/* "use strict";
let result = (function sum(x) {
    if (x === 10) return x;
    return x + sum(x + 1);
})(1);
console.log(result); */

// (function func(x) {
//     console.log(func); //undefined
//     var func = 100;
// })(10);

// (function func(x) {
//     func = 100;
//     console.log(func);//函数本身
// })(10);

// (function func(x) {
//     console.log(func); //函数本身
// })(10);
// console.log(func); //Uncaught ReferenceError: func is not defined


/* var b = 10;
(function b() {
    b = 20;
    console.log(b); //函数
})();
console.log(b); //10 */