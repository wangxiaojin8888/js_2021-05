// @1 创建一个Ajax实例对象：
// xhr「基于http请求回来一个xml格式数据(很早以前是这样的，现在都是获取JSON格式数据)」
// ajax：async javascript and xml 
let xhr = new XMLHttpRequest;

// @2 打开一个请求连接「发送请求之前的相关配置」
// xhr.open([请求方式],[请求地址],[同步/异步],[账号],[密码])
//   + [请求方式]:
//     GET系列(GET、HEAD、DELETE、OPTIONS) 
//     POST系列(POST、PUT、PATCH)
//   + [请求地址]:服务器端提供的接口请求地址(API地址)
//   + [同步/异步]:默认是true代表异步请求、false是同步请求(别用)
xhr.open('GET', './data.json', true);

/* 
@3 创建ajax监听器：监听每一个阶段处理的事情和结果
  + xhr.readyState：ajax的状态(代表当前请求的不同阶段) 
    0 UNSENT 未发送(创建实例完成后的默认状态)  
    1 OPENED 已打开(已经执行了OPEN方法 )   
    2 HEADERS_RECEIVED 响应头信息已经返回给客户端(客户端此时可以获取响应头信息) 
    3 LOADING 响应主体信息正在返回中 
    4 DONE 响应主体信息已经返回(到此为止，当前本次请求就结束了)
  + xhr.status：HTTP响应状态码「服务器基于不同的状态码告知客户端相应的结果」
    以2开始的基本上都是成功
    + 200 OK 标准成功
    + 206 Partial Content 断点续传
    以3开始的虽然也获取到数据了，但是中间会经历一些特殊的处理
    + 301 Moved Permanently 永久转移(永久重定向) => 域名迁移
    + 302(307) Move Temporarily 临时转移(临时重定向) => 服务器负载均衡
    + 304 Not Modified 服务器资源未更新(协商缓存)
    以4开始的一般都是客户端错误
    + 400 Bad Request 参数错误
    + 401 Unauthorized 权限有问题
    + 403 Forbidden 服务器拒绝你的请求，但是为啥不告诉你，因为原因可能很多
    + 404 Not Found 地址错误
    + 405 Method Not Allowed 请求的方式不被允许
    + 408 Request Timeout 请求超时
    以5开始的一般都是服务器错误
    + 500 Internal Server Error 未知服务器错误
    + 502 Bad Gateway 网管或者代理服务器出现了问题
    + 503 Service Unavailable 超负荷
 */
xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
        console.log(xhr.responseText);
    }
};

// @4 发送请求
// xhr.send([请求主体信息])
xhr.send();