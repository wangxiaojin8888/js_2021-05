// 异步获取数据
const query = () => {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', `./data.json?_=${+new Date()}`, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                resolve(data);
            }
        };
        xhr.send();
    });
};

/* let submit = document.querySelector('#submit'),
    running = false;
submit.onclick = async function () {
    if (running) return;
    running = true;
    let data = await query();
    console.log('请求成功：', data);
    running = false;
}; */

// 需求：上述代码，每一次点击按钮都是向服务器重新发送请求获取数据；但是对于“数据不经常更新的接口”，如果我连续发送多次请求，拿回来的数据都是一样的，这样很没有必要「消耗性能、浪费时间、增加了服务器的压力」；所以我们期望可以自己实现一套“数据的临时缓存机制”：
// 每一次发送请求之前，都先观察一下本地是否存储了对应的数据，以及是否过期「有效期自己可以设置」
//   + 有数据存储且未过期：直接从本地获取即可，无需再向服务器发送请求
//   + 本地没有数据或者失效了：重新向服务器发送请求，获取最新的数据，把最新的数据再次存储到本地...
// 非常类似于资源文件的“强缓存”，只不过强缓存是服务器设置，客户端浏览器执行，不需要我们写代码，但是数据缓存需要我们自己基于代码去实现这套机制！！

/*
 cacheData:实现前后端数据通信的临时缓存方法
   @params
     query:函数，执行这个函数可以发送对应的数据请求，要求返回的结果是promise实例「请求成功实例为成功」
     storageName:字符串，本地存储设置的键名
     limit:数字，限定的有效周期，默认值是 3600000ms「一小时」
   @return
     新的promise实例，不论从服务器还是从本地缓存获取的数据，只要获取到，则实例为成功，值是获取的信息
 */
const cacheData = function cacheData(query, storageName, limit = 3600000) {
    if (typeof query !== "function") throw new TypeError("query is not a function");
    if (typeof storageName !== "string") throw new TypeError("storageName is not a string");
    if (typeof limit !== "number" || isNaN(limit)) limit = 3600000;
    return new Promise(async resolve => {
        let storage = localStorage.getItem(storageName);
        if (storage) {
            storage = JSON.parse(storage);
            if ((+new Date()) - storage.time < limit) {
                // 在有效期内
                resolve(storage.data);
                return;
            }
        }
        // 存储的数据失效
        let result = await query();
        localStorage.setItem(storageName, JSON.stringify({
            time: +new Date(),
            data: result
        }));
        resolve(result);
    });
};

let submit = document.querySelector('#submit'),
    running = false;
submit.onclick = async function () {
    if (running) return;
    running = true;
    let data = await cacheData(query, 'senge');
    console.log(data);
    running = false;
};


/* let submit = document.querySelector('#submit'),
    running = false;
submit.onclick = async function () {
    if (running) return;
    running = true;
    // 在每一次发送请求之前：先看本地是否有存储以及是否过期
    let senge = localStorage.getItem('senge');
    if (senge) {
        senge = JSON.parse(senge);
        if ((+new Date()) - senge.time < 3600000) {
            // 还在有效期内
            console.log('请求成功：', senge.data);
            running = false;
            return;
        }
    }
    let data = await query();
    console.log('请求成功：', data);
    // 每一次从服务器获取后：把获取的信息基于localStorage进行存储「设定存储的时间」
    localStorage.setItem('senge', JSON.stringify({
        data,
        time: +new Date()
    }));
    running = false;
}; */

/*
 基于JS管理的本地存储方案: 控制台->Application可以查看「都是以明文形式存储，所以重要隐秘的信息尽可能不要存储；非要存储一定要加密处理！ 存储到本地的信息都是以字符串形式存储的！！」
    @1 cookie
      操作：document.cookie 实现获取和设置
      + 具备有效期：我们在设置cookie信息的时候可以设置有效期；在有效期内，不论页面是刷新还是关闭重新打开，存储的cookie信息都在
      + 受“源”和“浏览器”限制：cookie信息只允许同源访问、而且更换浏览器后也无法获取
      + 存储大小有限制：同源下最多只允许存储4KB
      + 不稳定：清理电脑垃圾(或者清除浏览器历史记录)可以选择性把存储的cookie都干掉、浏览器隐私模式(无痕浏览器模式)下是禁止设置cookie的
      + 和服务器之间有“猫腻儿”：服务器在响应头中携带set-cookie字段，客户端浏览器会自动设置对应的cookie；客户端只要本地有cookie信息，不论服务器是否需要，都会基于请求头Cookies字段，把cookie传递给服务器！「所以：本地cookie存储的越多，每一次向服务器发送请求传送的东西也就越多，速度也就越慢！！」

    @2 localStorage
      操作：localStorage.setItem([key],[value])  localStorage.getItem([key])  localStorage.removeItem([key])  localStorage.clear()清除所有 ...
      + 持久化存储：存储的信息只要不手动移除，会一直存在
      + 受“源”和“浏览器”限制
      + 存储大小有限制：同源下最多可以允许存储5MB
      + 稳定：清除电脑垃圾或者历史记录对localStorage存储的信息没有影响，而且无痕模式下也可以设置信息！
      + 和服务器之间毫无关联：除非自己手动的把本地存储的信息传递给服务器，否则和服务器没关系

    @3 sessionStorage
      和localStorage只有一个区别
      + localStorage是持久存储，页面刷新或者关闭，存储的信息还在
      + sessionStorage是会话存储，页面刷新存储的信息在，但是页面一但关闭(会话结束)，存储的信息都会释放

    @4 IndexDB
    @5 webSQL
    ......
    ---------
    以上本地存储方案，都是存储到计算机物理内存中的；但是还有一些存储方案是存储在虚拟内存中的：
    特点：页面刷新（或者页面关闭），之前存储的信息都会被释放掉
    + 全局变量
    + vuex / redux
 */