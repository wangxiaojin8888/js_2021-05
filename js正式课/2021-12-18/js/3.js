/*
 事件基础知识「DOM操作」 
   1.什么是事件?
     浏览器天生具备的行为「或者说浏览器为DOM元素内置赋予的行为」，即使不编写任何的代码，只要行为触发，事件也都会被相继触发！！
     http://web.h3399.cn/Events.htm

   2.事件绑定
     给当前元素的某个事件行为绑定方法，目的是当事件触发的时候，可以做点啥事！！
     + DOM0事件绑定   elem.onxxx=function...
     + DOM2事件绑定   elem.addEventListener('xxx',function...,true/false)
   
   3.事件对象
     事件触发，不仅会把绑定的方法执行，还会给方法传递一个实参信息，我们把基于ev来接收，这个ev接收的值就是事件对象！！ 事件对象：记录当前本次操作的相关信息！！
     elem.onclick=function(ev){
        //ev 存储的就是事件对象
     };
     -------
     鼠标事件对象  MouseEvent/PointerEvent
       + clientX/clientY  距离可视窗口
       + pageX/pageY   距离BODY
       + target/srcElement  事件源
       + type 事件类型
       + stopPropagation() 阻止冒泡传播
       + preventDefault() 阻止默认行为
       + ...
     键盘事件对象  KeyboardEvent
       + altKey:false
       + ctrlKey
       + metaKey
       + shiftKey
       + keyCode/which 键盘码
         enter 13 
         space 32
         sift 16
         ctrl 17
         alt 18
         win(command) 91
         backup 8
         delete 46
         左/上/右/下  37~40
         ...
     其它事件对象
   
   4.事件传播机制  Event.prototype
    AT_TARGET: 2   目标阶段「把当前事件源的相关行为触发」
    BUBBLING_PHASE: 3 冒泡阶段 「把事件源所有祖先元素(一直到window)，按照从内到外的顺序，把相关事件行为依次触发，如果绑定了方法也会被触发执行」
    CAPTURING_PHASE: 1  捕获阶段「捕捉路径，为后期的冒泡传播做准备」
    NONE: 0
    ------
    mouseenter VS mouseover
      + mouseenter默认阻止了冒泡传播，而mouseover具备冒泡传播机制
      + mouseenter是进入，对应的mouseleave是离开
      + mouseover是滑到上面(覆盖)，对应的mouseout是离开上面

   5.事件委托（事件代理） 
 */