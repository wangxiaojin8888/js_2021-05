let magnifier = document.querySelector('.magnifier'),
    abbre = magnifier.querySelector('.abbre'),
    mark = abbre.querySelector('.mark'),
    origin = magnifier.querySelector('.origin'),
    originImg = origin.querySelector('img');

// 计算MARK盒子的位置
const computedMark = function computedMark(ev) {
    let { clientX, clientY } = ev,
        { left, top } = abbre.getBoundingClientRect(),
        markT = 0,
        markL = 0;
    let minL = 0, minT = 0, maxL = 200, maxT = 200;
    // 计算MARK的位置
    markT = clientY - top - 50;
    markL = clientX - left - 50;
    // 边界判断
    markL = markL < minL ? minL : (markL > maxL ? maxL : markL);
    markT = markT < minT ? minT : (markT > maxT ? maxT : markT);
    // 设置样式
    mark.style.left = markL + 'px';
    mark.style.top = markT + 'px';
    // 控制大图等比例的移动  大图移动的距离/originImg = mark移动的距离/abbre
    originImg.style.left = -(markL / 300) * 1200 + 'px';
    originImg.style.top = -(markT / 300) * 1200 + 'px';
};

// 事件绑定
abbre.onmouseenter = function (ev) {
    mark.style.opacity = 1;
    origin.style.cssText = `opacity:1;z-index:10;`;
    computedMark(ev);
};
abbre.onmousemove = computedMark;
abbre.onmouseleave = function (ev) {
    mark.style.opacity = 0;
    origin.style.cssText = `opacity:0;z-index:-1;`;
};