(function () {
    class Sub {
        // 给实例设置私有属性
        listeners = [];

        // Sub.prototype
        on(func) {
            let listeners = this.listeners;
            if (listeners.includes(func)) return;
            listeners.push(func);
        }
        off(func) {
            let listeners = this.listeners,
                index = listeners.indexOf(func);
            if (index === -1) return;
            listeners[index] = null;
        }
        fire(...params) {
            let listeners = this.listeners,
                i = 0,
                item;
            for (; i < listeners.length; i++) {
                item = listeners[i];
                if (typeof item !== "function") {
                    listeners.splice(i, 1);
                    i--;
                    continue;
                }
                item(...params);
            }
        }
    }

    // 暴露API
    if (typeof window !== "undefined") window.Sub = Sub;
    if (typeof module === "object" && typeof module.exports === "object") module.exports = Sub;
})();

const fn1 = (x, y) => console.log('fn1', x + y);
const fn2 = () => console.log('fn2');
const fn3 = () => console.log('fn3');

let plain1 = new Sub;
plain1.on(fn1);
plain1.on(fn2);
setTimeout(() => {
    plain1.fire(10, 20);
}, 1000);

let plain2 = new Sub;
plain2.on(fn1);
plain2.on(fn3);
setTimeout(() => {
    plain2.fire(100, 200);
}, 2000);

