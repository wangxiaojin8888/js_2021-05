let slide = document.querySelector('.slide'),
    cover = slide.querySelector('.cover'),
    progress = slide.querySelector('.progress'),
    item = progress.querySelector('.item');

// 计算背景图和进度条
const computed = function computed(ev) {
    let A = slide.offsetWidth,
        B = ev.clientX - slide.getBoundingClientRect().left,
        ratio = B / A,
        cur = Math.ceil(8 * ratio);
    cur = cur < 1 ? 1 : (cur > 8 ? 8 : cur);
    slide.style.backgroundImage = `url('images/step${cur}.jpeg')`;
    item.style.width = `${ratio * 100}%`;
};

// 事件绑定
slide.onmouseenter = function (ev) {
    cover.style.display = 'none';
    progress.style.display = 'block';
    computed(ev);
};
slide.onmousemove = computed;
slide.onmouseleave = function (ev) {
    cover.style.display = 'block';
    progress.style.display = 'none';
    // 清空数据
    item.style.width = '0';
    slide.style.backgroundImage = 'none';
};