let zTree = document.querySelector('#zTree'),
    data = null;

// 异步获取数据
const query = function query() {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', './data.json');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                resolve(data);
            }
        };
        xhr.send();
    });
};

// 实现数据绑定
const render = function render(data) {
    let str = ``;
    data.forEach(item => {
        let { name, open, children } = item;
        str += `<li>
            <a href="javascript:;" class="title">${name}</a>
            ${Array.isArray(children) ? `
                <em class="icon ${open ? 'open' : ''}"></em>
                <ul class="level" style="display: ${open ? 'block' : 'none'};">
                    ${render(children)}
                </ul>
            ` : ``}
        </li>`;
    });
    return str;
};

// 基于事件委托实现事件绑定
zTree.onclick = function (ev) {
    let target = ev.target,
        targetTag = target.tagName,
        targetClass = target.className;
    // 点击的是“+/-”
    if (targetTag === 'EM' && targetClass.indexOf('icon') > -1) {
        let flag = targetClass.indexOf('open') > -1,
            next = target.nextElementSibling;
        if (!next) return;
        // flag:true 当前是展开的，我们让其隐藏；反之当前是隐藏的，我们让其展开；
        if (flag) {
            next.style.display = 'none';
            target.className = 'icon';
        } else {
            next.style.display = 'block';
            target.className = 'icon open';
        }
    }
};

query().then(value => {
    data = value;
    zTree.innerHTML = render(data);
});