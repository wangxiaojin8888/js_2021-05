(function () {
    let listeners = {};

    const on = function on(name, func) {
        if (!listeners.hasOwnProperty(name)) {
            listeners[name] = [func];
            return;
        }
        let arr = listeners[name];
        if (arr.indexOf(func) > -1) return;
        arr.push(func);
    };

    const off = function off(name, func) {
        if (!listeners.hasOwnProperty(name)) return;
        let arr = listeners[name],
            index = arr.indexOf(func);
        if (index === -1) return;
        // arr.splice(index, 1); //这样会产生数组塌陷，可能带来问题
        arr[index] = null;
    };

    const emit = function emit(name, ...params) {
        if (!listeners.hasOwnProperty(name)) return;
        let arr = listeners[name];
        for (let i = 0; i < arr.length; i++) {
            let item = arr[i];
            if (typeof item !== "function") {
                // 移除掉
                arr.splice(i, 1);
                i--;
                continue;
            }
            item(...params);
        }
    };

    // 暴露API
    const sub = {
        on,
        off,
        emit
    };
    if (typeof window !== "undefined") window.$sub = sub;
    if (typeof module === "object" && typeof module.exports === "object") module.exports = sub;
})();

setTimeout(() => {
    $sub.emit('黄蓉结婚', 100, 200);
}, 1000);

setTimeout(() => {
    $sub.emit('黄蓉结婚', 2000, 3000);
}, 3000);

const fn1 = (x, y) => {
    console.log('邵宇是我的司仪', x, y);
};
$sub.on('黄蓉结婚', fn1);

const fn2 = (x, y) => {
    console.log('杨妮是我的伴娘', x, y);
    // 把计划1和2移除掉
    $sub.off('黄蓉结婚', fn1);
    $sub.off('黄蓉结婚', fn2);
};
$sub.on('黄蓉结婚', fn2);

const fn3 = () => {
    console.log('颖辉回来跳舞');
};
$sub.on('黄蓉结婚', fn3);

const fn4 = () => {
    console.log('江江会来参加婚礼');
};
$sub.on('黄蓉结婚', fn4);

const fn5 = () => {
    console.log('山森也要跟着来');
};
$sub.on('黄蓉结婚', fn5);

