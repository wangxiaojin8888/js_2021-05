let box = document.querySelector('.top');
let adds, minus; // 定义了两个变量 没给值； 当渲染完成之后才给他们付了值
let totalBox = document.querySelector('#total')
let totalMoney = document.querySelector('#totalMoney')
let best = document.querySelector('#best')
let ary = [
  {
    id: 1,
    count: 0,
    price: 10000,//一万分  100快
  },
  {
    id: 2,
    count: 0,
    price: 13456,//一万分  100快
  },
  {
    id: 3,
    count: 0,
    price: 40300,//一万分  100快
  },
  {
    id: 4,
    count: 0,
    price: 21020,//一万分  100快
  },
  {
    id: 5,
    count: 0,
    price: 20000,//一万分  100快
  }
]

function render() {
  let str = '';
  ary.forEach(item => {
    str += `<div class="hang">
        <i class="minus" qqq=${item.id}></i>
        <span class="pronum">${item.count}</span>
        <i class="add" qqq=${item.id}></i>
        <span class="info">单价：${(item.price / 100).toFixed(2)}元 &nbsp;&nbsp; 小计：${(item.count * item.price / 100).toFixed(2)}元</span>
    </div>`
  })
  box.innerHTML = str // 每当render执行的时候 都会使用新的元素顶替老的元素
  adds = document.querySelectorAll('.add');
  minus = document.querySelectorAll('.minus')

  bindEvent()
  renderTotal()
}
render()
function bindEvent() {
  console.log(adds);
  // 这个函数是用来负责绑定点击事件的；
  [...adds].forEach(add => {
    // add 是每一个add标签
    add.onclick = function () {
      // add 是每一个 + 按钮
      // 没办法判断电机的是哪一个+
      // 再渲染阶段 我们每一个+标签 添加一个qqq的行内属性 值是数据对应的id
      // 点击的时候我们可以通过add的行内属性属性qqq来获取点击按钮对印的数据id
      // console.log(this.getAttribute('qqq'))
      // console.log("++++")
      let id = this.getAttribute('qqq'); // 对应的是那条数据的id
      ary.forEach(item => {
        if (item.id == id) {
          // 证明 item 就是想要的数据
          item.count++
        }
      })
      // 改好了数据  再去新的数据
      render()
    }
  });
  [...minus].forEach(min => {
    // min 是每一个add标签
    min.onclick = function () {
      let id = this.getAttribute('qqq'); // 对应的是那条数据的id
      ary.forEach(item => {
        if (item.id == id) {
          // 证明 item 就是想要的数据
          if (item.count > 0) {
            item.count--
          }

        }
      })
      // 改好了数据  再去新的数据
      render()
    }
  })

}

function renderTotal() {
  // 什么时候执行  每当数组发生改变的时候执行
  let total = 0;
  let allMoney = 0;
  ary.forEach(item => {
    total += item.count
    allMoney += item.count * item.price
  })
  totalBox.innerHTML = total
  totalMoney.innerHTML = (allMoney / 100).toFixed(2)
  // 怎么找到最贵的那个？
  // 先去数组中找出买过的商品
  // 再从买过的商品中跳出最贵的
  let arr = [];// 存储买过的商品数据
  ary.forEach(item => {
    if (item.count > 0) {
      arr.push(item)
    }
  })
  arr.sort((a, b) => {
    return b.price - a.price
  })
  // arr[0].price // 最贵的单价
  best.innerHTML = (arr[0] ? arr[0].price / 100 : 0).toFixed(2)

}