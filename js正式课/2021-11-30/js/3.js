
function fun() {
  this.a = 0;
  this.b = function () {
    alert(this.a);
  }
}

var qq = new fun();//  qq.__proto__ 是指向默认的原型
// 原型重定向
// 需要把constructor的指向改回来
fun.prototype = {
  constructor: fun,
  b: function () {
    this.a = 20;
    alert(this.a);
  },
  c: function () {
    this.a = 30;
    alert(this.a)
  }
};

var my_fun = new fun();
// my_fun.constructor--->
my_fun.b(); // this -->  my_fun  my_fun.a  0
qq.c() // this --> qq   30
my_fun.c()// this --> myfun  30
qq.b() // this-->qq  qq.a   30
my_fun.b()// this --> my_fun  my_fun.a  30








var fullName = "languge";
var obj = {
  fullName: 'javascript',
  prop: {
    getFullName: function () {
      return this.fullName;
    }
  }
};
console.log(obj.prop.getFullName());
// this --> obj.prop  obj.prop.fullName
// obj.prop.fullName  自己身上没有这个属性  就去obj.prop的所属类的原型上查找
// obj.prop的所属类 就是Object类(obj.prop是一个普通的对象)
// 基类的原型上没有这个属性 所以返回undefined
var test = obj.prop.getFullName;
console.log(test()); // this --> window  window.fullName --> languge








var name = "window";
var Tom = {
  name: "Tom",
  show: function () {
    console.log(this.name);
  },
  wait: function () {
    var fun = this.show;
    fun(); // this --> window
  }
};
Tom.wait();// this --> Tom 





// 赋值操作 永远是把右边的执行结果赋值给左边
var n = 2;// 4  8
var obj = {
  n: 3,// 6
  fn: (function (n) {
    n += 2;// 4  5  6  7
    this.n += 2;// 自执性函数的this是window  window.n += 2
    var n = 5;
    return function (m) {
      // 这个函数执行的时候形成的作用的的上级作用域是 自执行函数的作用域
      this.n *= 2;
      console.log(m + (++n));
    }
  })(2)
};
var fn = obj.fn;
fn(3);// this -->  window window.n *=2  m=3   9
obj.fn(3); // this  --> obj  obj.n *=2  m=3   10
console.log(n, obj.n)