function Fn() {
  this.x = 100;
  this.y = 200;
  this.getX = function () {
    console.log(this.x);
  }
}
Fn.prototype.getX = function () {
  console.log(this.x);
}
Fn.prototype.getY = function () {
  console.log(this.y);
}
var f1 = new Fn; // f1 是 通过 new Fn  得到的 Fn的实例
var f2 = new Fn;// f2 是 通过 new Fn  得到的 Fn的实例
console.log(f1.getX == f2.getX);//false
console.log(f1.getY == f2.getY);// 用的都是原型上的  true
console.log(f1.__proto__.getY == Fn.prototype.getY);// true
console.log(f1.__proto__.getX == f2.getX)//false
console.log(f1.getX === Fn.prototype.getX);//false
console.log(f1.constructor); // Fn
console.log(Fn.prototype.__proto__.constructor);// Object
f1.getX();// this --> f1  f1.x --> 100
f1.__proto__.getX();// this --> f1.__proto__ f1.__proto__.x // undefined
f2.getY();// this --> f2;  f2.y  200
Fn.prototype.getY();// this --> Fn.prototype    Fn.prototype.y  undefined