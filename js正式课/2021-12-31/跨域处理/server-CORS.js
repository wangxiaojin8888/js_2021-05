/*-CREATE SERVER-*/
const express = require('express'),
	app = express();
app.listen(1001, () => {
	console.log(`THE WEB SERVICE IS CREATED SUCCESSFULLY AND IS LISTENING TO THE PORT：1001`);
});

/*-MIDDLE WARE-*/
// 设置白名单
let safeList = ['http://127.0.0.1:5500', 'http://127.0.0.1:5501', 'http://127.0.0.1:8080'];
app.use((req, res, next) => {
	let origin = req.headers.origin || req.headers.referer || "";
	origin = origin.replace(/\/$/g, '');
	origin = !safeList.includes(origin) ? '' : origin;
	res.header("Access-Control-Allow-Origin", origin);
	res.header("Access-Control-Allow-Credentials", true);
	res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length,Authorization, Accept,X-Requested-With");
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS,HEAD");
	req.method === 'OPTIONS' ? res.send('OK') : next();
});

/*-API-*/
app.get('/list', (req, res) => {
	res.send({
		code: 0,
		message: 'zhufeng'
	});
});

/* STATIC WEB */
app.use(express.static('./'));