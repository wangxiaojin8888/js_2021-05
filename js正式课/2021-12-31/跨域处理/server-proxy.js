/*-CREATE SERVER-*/
const express = require('express'),
    app = express();
app.listen(1001, () => {
    console.log(`THE WEB SERVICE IS CREATED SUCCESSFULLY AND IS LISTENING TO THE PORT：1001`);
});

const request = require('request');
// 服务器接收到客户端发送过来的请求
app.get('/recommended', (req, res) => {
    // 向简书发送相同请求，从简书服务器获取想要的数据「不存在域的限制的」
    // 把获取的数据返回给客户端
    let jianURL = `https://www.jianshu.com/asimov/subscriptions/recommended_collections`;
    req.pipe(request(jianURL)).pipe(res);
});

/* STATIC WEB */
app.use(express.static('./'));