/* 
  封装一个jsonp方法「基于promise管理」,执行这个方法可以发送jsonp请求 
    jsonp([url],[options])
      options配置项
      + params:null/对象 问号参数信息
      + jsonpName:'callback' 基于哪个字段把全局函数名传递给服务器
      + ...
*/
(function () {
  const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
  };

  const stringify = function stringify(obj) {
    let keys = Reflect.ownKeys(obj),
      str = ``;
    keys.forEach(key => {
      str += `&${key}=${obj[key]}`;
    });
    return str.substring(1);
  };

  const jsonp = function jsonp(url, options) {
    // init params
    if (typeof url !== "string") throw new TypeError("url must be required");
    if (!isPlainObject(options)) options = {};
    let { params, jsonpName } = Object.assign({
      params: null,
      jsonpName: "callback"
    }, options);

    return new Promise((resolve, reject) => {
      // 创建全局函数:防止全局变量污染
      let name = `jsonp${+new Date()}`,
        script;
      window[name] = function (result) {
        // 请求成功：result存储服务器返回的数据
        clear();
        resolve(result);
      };

      // 清除函数
      const clear = () => {
        Reflect.deleteProperty(window, name);
        if (script) document.body.removeChild(script);
      };

      // 处理URL：把params问号传参的信息拼接好 & 全局函数名也要拼接上来
      if (isPlainObject(params)) {
        params = stringify(params);
        url += `${url.indexOf('?') > -1 ? '&' : '?'}${params}`;
      }
      url += `${url.indexOf('?') > -1 ? '&' : '?'}${jsonpName}=${name}`;

      // 基于SCRIPT发送请求
      script = document.createElement('script');
      script.src = url;
      script.async = true;
      script.onerror = function () {
        // 请求失败
        clear();
        reject();
      };
      document.body.appendChild(script);
    });
  };

  /* 暴露API */
  if (typeof module === 'object' && typeof module.exports === 'object') module.exports = jsonp;
  if (typeof window !== 'undefined') window.jsonp = jsonp;
})();