/*-CREATE SERVER-*/
const express = require('express'),
    app = express();
app.listen(1001, () => {
    console.log(`THE WEB SERVICE IS CREATED SUCCESSFULLY AND IS LISTENING TO THE PORT：1001`);
});

app.get('/user/list', (req, res) => {
    let { cb } = req.query;
    // 准备数据
    let result = {
        code: 0,
        data: ['张三', '李四']
    };
    // 返回给客户端指定的格式，例如：’函数名(数据)‘
    res.send(`${cb}(${JSON.stringify(result)})`);
});

/* STATIC WEB */
app.use(express.static('./'));