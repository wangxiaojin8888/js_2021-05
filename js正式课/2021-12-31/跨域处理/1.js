/* axios.get('https://www.jianshu.com/asimov/subscriptions/recommended_collections')
    .then(response => response.data)
    .then(value => {
        console.log(value);
    }); */
// Access to XMLHttpRequest at 'https://www.jianshu.com/asimov/subscriptions/recommended_collections' from origin 'http://127.0.0.1:5500' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
// 浏览器本身具备安全限制：默认是不允许ajax/fetch跨域访问的
//   + 但是对于某些操作放开了安全限制：<link href=''>、<script src=''>、<img src=''>...
// 服务器和服务器之间默认是没有跨域限制的，如果需要限制，则需要手动设置“白名单”！

/*
 解决跨域问题：
   @1 proxy跨域代理
     + 开发环境下，基于 webpack-dev-server 实现跨域代理
     + 生产环境下，基于 nginx 反向代理实现跨域
     原理：设置代理服务器，首先保证客户端和代理服务器是“同源访问”，客户端发送的所有请求都是给代理服务器{同源不存在限制}；代理服务器会向真实的服务器发送请求{服务器之间不存在源的限制}，把从真实服务器获取的内容返回给客户端！！

   @2 CROS跨域资源共享
     原理：服务器端设置允许源即可；默认浏览器不允许跨域，但是服务端只要在响应头中设置 Access-Control-Allow-Origin 字段，允许当前客户端访问，则浏览器也不会在限制了！！
     + 所以核心操作都在后端
       Access-Control-Allow-Origin它设置的值可以是：*(允许所有源) 或者 单一源
       如果想允许指定的几个源访问，则需要自己实现白名单的机制
     + 客户端如果想在跨域中携带资源凭证，需要设置 withCredentials:true，服务器端也设置 Access-Control-Allow-Credentials:true！

   @3 JSONP跨域请求
     原理：利用 <srcipt src=''> 不存在域的限制，实现数据请求；
       + 客户端创建一个“全局函数”
       + 发送请求的时候，基于callback(和后台协商)把全局函数名传递给服务器
       ----JSONP需要服务器的特殊支持
       + 后台获取传递的函数名
       + 准备客户端需要的数据
       + 最后返回这样格式的字符串 `函数名(数据)`
       ----
       + 浏览器获取这个格式字符串的时候，会把全局函数执行
       + 数据做为实参传递给函数
     JSONP只能发送GET请求！！一但服务器支持了JSONP的处理，那么所有客户端都可以向其发请求，不是很安全！！


   -----
   还有一些其它的方案，例如：
   + postMessage
   + document.domain + iframe
   + window.name + iframe
   + location.hash + iframe
   + ...
 */

/* axios.get('http://127.0.0.1:1001/list', {
    withCredentials: true
}).then(response => response.data).then(value => {
    console.log(value);
}); */