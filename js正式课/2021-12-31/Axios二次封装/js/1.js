/*
 @1 在axios内部，POST系列请求下，如果我们给data指定为一个普通的对象，则内部会把其变为JSON格式的字符串，传递给服务器，并且设置了 Content-Type:application/json；
 @2 axios内部会根据data传递的数据格式，自动设置Content-Type，能识别的格式：urlencoded、json、form-data...「但是这是axios自己内部处理的，自己基于ajax/fetch发送请求，需要自己去指定」
 @3 默认情况下,axios内部处理的时候，只有HTTP状态码是以2开始的才算成功，当然我们可以基于 validateStatus 自定义处理机制!!
 */
const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};

// const source = axios.CancelToken.source();
axios.post('/user/login', {
    account: '18310612838',
    password: 'e807f1fcf82d132f9bb018ca6738a19f'
}).then(value => {
    let { code, token } = value;
    if (+code === 0) {
        console.log('恭喜您登录成功！');
        localStorage.setItem('t', token);
        return;
    }
    console.log('账号密码不匹配！');
});
// source.cancel('这里是中断的原因');


axios.get('/user/list', {
    params: {
        departmentId: 0,
        search: ''
    }
}).then(value => {
    console.log('成功:', value);
});