/*
 axios的二次封装：把大部分请求需要提取的公共部分进行提取
   + 调用axios发送请求的代码更简洁、易维护、提高开发效率等
   + 还可以把一些统一要处理的逻辑拿进来处理
   + ...
 */
const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};
axios.defaults.baseURL = 'http://127.0.0.1:9999';
axios.defaults.timeout = 60000;
axios.defaults.withCredentials = true; //跨域请求中可以允许携带资源凭证
axios.defaults.transformRequest = data => {
    if (isPlainObject(data)) data = Qs.stringify(data);
    return data;
};
axios.defaults.validateStatus = status => {
    return status >= 200 && status < 400;
};
// 请求拦截器：配置项都准备好，准备发送请求之间，我劫持一下，目的是:把配置项再改改
axios.interceptors.request.use(config => {
    // 例如：携带Token信息
    let token = localStorage.getItem('t');
    if (token) config.headers['authorzation'] = token;
    return config;
});
// 响应拦截器：从服务器获取到结果和自己执行业务逻辑之间
axios.interceptors.response.use(response => {
    return response.data;
}, reason => {
    if (reason && typeof reason === "object") {
        let { response, code } = reason;
        if (response) {
            // 状态码出现了问题
            switch (+response.status) {
                case 404:
                    // ...
                    break;
                // ...
            }
        } else if (code === 'ECONNABORTED') {
            // 请求超时
        } else if (reason.hasOwnProperty('message')) {
            // 请求中断
        }
    }
    // 网络出现问题
    return Promise.reject(reason);
});