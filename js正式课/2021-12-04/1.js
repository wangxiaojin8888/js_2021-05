/* const fn = function fn(...params) {
    // params:[1,2]
    return function proxy(...args) {
        // args:[3]
        params = params.concat(args); //[1,2,3]
        let total = 0;
        params.forEach(item => total += item);
        return total;
    };
}; */
/* const fn = (...params) => (...args) => {
    let total = 0;
    params.concat(args).forEach(item => total += item);
    return total;
};
let res = fn(1, 2)(3);
console.log(res); */

// 数组求和的办法？
// let arr = [1, 2, 3];
/* let total = 0;
arr.forEach(item => {
    total += item;
});
console.log(total); */

// eval把字符串变为JS表达式执行「不推荐」
// console.log(eval(arr.join('+')));

// 可以使用数组的reduce方法？{先自己回去扩展  MDN}


/*
 for循环和forEach的区别？
    + for循环是命令式编程，而forEach是函数式编程
    + 在for循环中，我们可以很灵活的控制循环的逻辑「例如：控制间隔几项循环一次、循环到某个阶段让其结束...」，而forEach只能依次迭代数组每一项，无法控制中间阶段结束等！！
    + forEach开发效率高，使用起来方便，而for循环操作起来略显繁琐
    + 在我之前的项目中，大部分需求我都是基于forEach处理的，部分需求需要灵活控制循环的步骤，我才基于for循环处理！！
 */
// let arr = [10, 20, 30];
// 命令式编程：HOW，关注的是处理的步骤和逻辑，或者说自己掌控操作的步骤
//   + 操作步骤自己灵活掌控，想干啥就干啥
//   + 冗余代码多、开发效率低
/* for (let i = 0; i < arr.length; i++) {
    console.log(arr[i], i);
    if (i >= 1) {
        break;
    }
} */
// 函数式编程：WHAT，只关注结果，把程序处理的步骤封装到函数内部，我们执行这个函数的目的是获取想要的结果，无需考虑函数内部是咋做的！！
//   + 开发便捷、方便维护、减少冗余代码 「项目中推荐使用函数式编程」
//   + 无法掌控具体的操作步骤，无法灵活控制执行的逻辑
/* arr.forEach((item, index) => {
    console.log(item, index);
}); */

//================================
/* function Dog(name) {
    this.name = name;
}
Dog.prototype.bark = function () {
    console.log('wangwang');
}
Dog.prototype.sayName = function () {
    console.log('my name is ' + this.name);
} */

/* // 重写内置NEW
// Ctor:constructor 要new的构造函数
// params:数组，存储后期new构造函数时候，给构造函数传递的实参信息
function _new(Ctor, ...params) {
    // @1 创在Ctor这个类的一个实例对象「空对象、对象.__proto__===Ctor.prototype」
    let obj = {};
    obj.__proto__ = Ctor.prototype;
    // @2 把传递的构造函数当做普通函数执行，但是需要让函数中的this指向创建的实例对象
    let result = Ctor.call(obj, ...params);
    // @3 如果方法返回一个对象，则以自己返回的为主；否则，都把创建的实例对象返回！
    if (result !== null && (typeof result === "object" || typeof result === "function")) {
        return result;
    }
    return obj;
} */

/* // Object.create([object/null])：创建一个空实例对象"@@"，把[object/null]作为它的原型「@@.__proto__===[object/null]」
function _new(Ctor, ...params) {
    let obj,
        result,
        type;
    if (!Ctor.hasOwnProperty('prototype') || Ctor === Symbol || Ctor === BigInt) throw new TypeError('Ctor is not a constructor');
    obj = Object.create(Ctor.prototype);
    result = Ctor.call(obj, ...params);
    type = typeof result;
    if (result !== null && (type === "object" || type === "function")) return result;
    return obj;
}
let sanmao = _new(Dog, '三⽑');
sanmao.bark(); //=>"wangwang"
sanmao.sayName(); //=>"my name is 三⽑"
console.log(sanmao instanceof Dog); //=>true */


//================================
/* Array.prototype.push = function push(val) {
    // this->arr
    this[this.length] = val;
    this.length++;
    return this.length;
};
arr.push(100); //arr是Array的实例，arr基于原型链查找机制，找到Array.prototype.push方法，把这个方法执行「方法中的this是arr」；push方法执行的目的：把传递进来的100(val)加入到数组this的末尾，并且让数组长度累加1，返回新增后数组长度!!

arr做为数组类的实例，可以直接调取Array.prototype上的方法；但是类数组集合（arguments、HTMLCollection、NodeList...有索引有length，但是不是数组，都被称为类数组）不是Array的实例，不能直接调用 Array.prototype 上的方法，如果类数组也想用这些方法呢？
   @1 想用数组的哪个办法，就把其作为当前对象的一个私有属性
      let obj = {
         ...,
         push: Array.prototype.push,
         forEach: [].forEach
      };
      obj.push([val])
      obj.forEach([callback])  让方法中的this指向obj

    @2 直接把Array.prototype上的方法执行，基于call改变方法中的this是这个类数组，这样也相当于再操作这个类数组，也能实现对应的效果！！
       [].xxx.call([类数组],[参数])
       Array.prototype.xxx.call([类数组],[参数])

    @3 改变其原型指向，让其__proto__指向Array.prototype，这样就可以直接调用数组原型上的方法了
       Object.setPrototypeOf([obj], [prototype]) 修改[obj]对象的原型指向，让其指向[prototype]

    “鸭子类型”：长得像鸭子，但是不是鸭子，而我们期望它能具备鸭子的特点...
       例如：类数组就是“鸭子类型”，我们的目的是让类数组也能使用数组的方法
       类Promise...
*/
/* let obj = {
    0: 10,
    1: 20,
    length: 2
};
Object.setPrototypeOf(obj, Array.prototype);
obj.forEach((item, index) => {
    console.log(item, index);
}); */

/* let obj = {
    0: 10,
    1: 20,
    length: 2
};
[].forEach.call(obj, (item, index) => {
    console.log(item, index);
}); */

/* let obj = {
    2: 3, //1
    3: 4, //2
    length: 2, //3 4
    push: Array.prototype.push
};
obj.push(1);
//也是把Array.prototype.push执行，方法中的this->obj
// obj[obj.length] = 1;  -> obj[2]=1
// obj.length++;
obj.push(2);
// obj[obj.length] = 2;  -> obj[3]=2
// obj.length++;
console.log(obj); */

//==========================
/* function Foo() {
    getName = function () {
        console.log(1);
    };
    return this;
}
Foo.getName = function () {
    console.log(2);
};
Foo.prototype.getName = function () {
    console.log(3);
};
var getName = function () {
    console.log(4);
};
function getName() {
    console.log(5);
}
Foo.getName();
getName();
Foo().getName();
getName();
new Foo.getName();
new Foo().getName();
new new Foo().getName(); */

//==========================
/*
JS中THIS的五种情况：THIS是函数执行的主体(谁把他执行的)，和函数在哪创建及执行没有必然的关系
  @1 给元素的某个事件行为绑定方法，当事件触发，方法执行，方法中的THIS是当前操作元素本身
    box.onclick=function(){
        console.log(this); //box
    };
  @2 普通函数执行，看函数名前面是否有“点”
    + 没有：THIS是window(非严格模式)/undefined(严格模式)
    + 有：“点”前面是谁，this就是谁
    + 匿名函数(如自执行函数、回调函数等)中的this一般都是window，除非做过特殊的处理
    arr.push(100)  push中的this->arr
    arr.__proto__.push(100)  push中的this->arr.__proto__
    Array.prototype.push(100)  push中的this->Array.prototype
    const push=Array.prototype.push;
    push();  push中的this->window
    -----
    (function(){
        console.log(this); //window
    })();
    arr.forEach(function(){
        console.log(this); //window
    });
    arr.forEach(function(){
        console.log(this); //obj
    },obj); //forEach内部做了处理，让回调函数执行的时候，让其this指向传递的第二个实参值

  @3 构造函数执行，构造函数体中的this是当前类的实例
    function Foo(){
        console.log(this); //f
    }
    let f=new Foo();

  @4 箭头函数中没有自己的this(即使基于call/apply修改也没用，因为“没有”this)，函数中出现的this都是其上级上下文中的this！！
     window.name='window';
     let obj={
         name:'obj',
         fn(){
            //this->obj

            let self=this;
            setTimeout(function(){
                //this->window
                console.log(self.name);
            },1000);

            setTimeout(()=>{
                //this->obj
                console.log(this.name);
            },1000);
         }
     };
     obj.fn();

   @5 基于call/apply/bind强制改变函数中的this指向
     + call/apply([context])：把函数立即执行，把函数中的this改为[context]
       + call需要一项项把实参传递给函数
       + apply写成一个数组(集合)即可(最后也是一项项传递给函数)
     + bind：函数不会立即执行，只是预先改变函数中的this或者参数
*/
/* window.val = 1;
var json = {
    val: 10,
    dbl: function () {
        this.val *= 2;
    }
}
json.dbl(); //this:json  json.val:20
var dbl = json.dbl;
dbl(); //this:window  window.val:2
json.dbl.call(window); //this:window  window.val:4
alert(window.val + json.val); //“24” */


/* let obj = {
    fn: (function () {
        return function () { //0x000
            console.log(this);
        };
    })() //自执行函数执行，把其返回值赋值给obj.fn => obj.fn=0x000
};
obj.fn(); //this->obj
let fn = obj.fn;
fn(); //this->window */


/* (function () {
    var val = 1; //2
    var json = {
        val: 10,
        dbl: function () {
            val *= 2;
        }
    };
    json.dbl();
    alert(json.val + val); //“12”
})(); */

// const num; //Uncaught SyntaxError: Missing initializer in const declaration
/* const num = 20;
num = 30; //Uncaught TypeError: Assignment to constant variable. */
/* const obj = { name: 'zhufeng' };
obj.name = '珠峰培训';
console.log(obj); */
// const 声明的也是变量，只不过不允许更改其“指针指向”（不允许重新赋值其他值）