(function () {
  let arr = location.search.match(/id=(\w+)/)
  let id = arr && arr[1];
  // console.log(id)
  // 不管是新增员工 还是修改员工； 我们都需要部门列表和职务列表这两个下拉框
  let userdepartment = document.querySelector('.userdepartment')
  let userjob = document.querySelector('.userjob');
  let username = document.querySelector('.username')
  let man = document.querySelector('#man')
  let woman = document.querySelector('#woman')
  let useremail = document.querySelector('.useremail')
  let userphone = document.querySelector('.userphone')
  let userdesc = document.querySelector('.userdesc')
  let submit = document.querySelector('.submit')
  function renderSelect() {
    let p1 = http.get('/department/list').then(data => {
      let str = '';
      data.data.forEach(item => {
        str += `<option value="${item.id}">${item.name}</option>`
      })
      userdepartment.innerHTML = str;
    })
    let p2 = http.get('/job/list').then(data => {
      let str = '';
      data.data.forEach(item => {
        str += `<option value="${item.id}">${item.name}</option>`
      })
      userjob.innerHTML = str;
    })
    return Promise.all([p1, p2])
  }

  if (id) {
    //证明是编辑
    function getData() {
      http.get('/user/info?userId=' + id).then(data => {
        console.log(data)
        let { departmentId,
          jobId,
          name,
          email,
          phone,
          desc, sex } = data.info;
        userdepartment.value = departmentId
        userjob.value = jobId
        username.value = name
        useremail.value = email
        userphone.value = phone
        userdesc.value = desc
        man.checked = sex == 0 ? true : false
      })
    }
    renderSelect().then(data => {
      // 保证两个下拉框都完成之后 再去根据id请求数据回显
      getData()
    })

  } else {
    // 证明是新增
    renderSelect()
  }

  submit.onclick = function () {
    if (id) {
      // 编辑
      // userId=1&name=xxx&sex=0&email=xxx&
      //phone=xxx&departmentId=1&jobId=1&desc=xxx
      http.post('/user/update', {
        userId: id,
        name: username.value,
        sex: man.checked ? 0 : 1,
        email: useremail.value,
        phone: userphone.value,
        departmentId: userdepartment.value,
        jobId: userjob.value,
        desc: userdesc.value
      }).then(data => {
        // 庚辛成功之后跳转列表页
        location.href = './userlist.html'
      })
    } else {
      // 新增
      http.post('/user/add', {
        name: username.value,
        sex: man.checked ? 0 : 1,
        email: useremail.value,
        phone: userphone.value,
        departmentId: userdepartment.value,
        jobId: userjob.value,
        desc: userdesc.value
      }).then(data => {
        // 庚辛成功之后跳转列表页
        location.href = './userlist.html'
      })
    }
  }
})()