(function () {
  function queryUrl(str) {
    let obj = {};
    str.replace(/([^?=&]+)=([^?=&]+)/g, function (a, b, c) {
      obj[b] = c;
    })
    return obj;
  }
  let id = queryUrl(location.href).id;
  console.log(id)
  //根据id请求对应的回访信息
  if (!id) {
    alert("无此用户")
    return
  }
  let tbody = document.querySelector('#tbody');
  let visitTime = document.querySelector('.visitTime')
  let visitText = document.querySelector('.visitText')
  let submit = document.querySelector('.submit');
  getData()
  function getData() {
    http.get('/visit/list', {
      params: {
        customerId: id
      }
    }).then(data => {
      // console.log(data)
      render(data.data)
    })
  }
  function render(ary) {
    let str = '';
    ary.forEach(item => {
      let { id, visitText, visitTime } = item;
      str += `<tr data-vid="${id}">
      <td class="w5">${id}</td>
      <td class="w15">${visitTime}</td>
      <td class="w70 wrap">${visitText}</td>
      <td class="w10">
        <a href="javascript:;" data-id="${id}">删除</a>
      </td>
    </tr>`
    })
    if (str) {
      tbody.innerHTML = str;
    } else {
      tbody.innerHTML = `<h3>暂无记录</h3>`
    }

  }

  //  给提交按钮绑定点击事件
  submit.onclick = function () {
    // customerId=xxx&visitText=xxx&visitTime=xxx
    http.post('/visit/add', {
      customerId: id,
      visitText: visitText.value,
      visitTime: visitTime.value
    }).then(data => {
      // 新增成功之后 重新请求列表数据
      getData()
      visitText.value = ''
      visitTime.value = ''
    })
  }

  //  绑定删除
  tbody.onclick = function (e) {

    let id = e.target.dataset.id;// 回访记录的id
    if (id) {
      // 若有id 则证明点击的是 删除按钮
      let res = confirm("是否删除？")
      if (!res) return;
      http.get('/visit/delete', {
        params: {
          visitId: id
        }
      }).then(data => {
        // 删除成功之后重新渲染数据
        getData()
      })
    } else {
      // 没有id证明点击的要不就是td标签 要不就是tr标签
      let tr = null;
      if (/td/i.test(e.target.tagName)) {
        // 证明点击的是 td标签
        tr = e.target.parentNode
      } else {
        tr = e.target
      }
      let vid = tr.dataset.vid;
      review(vid)
    }

  }

  // 根据id回显回访内容
  function review(id) {
    http.get('/visit/info?visitId=' + id).then(data => {
      console.log(data)
      visitText.value = data.info.visitText
      visitTime.value = data.info.visitTime
    })
  }
})()