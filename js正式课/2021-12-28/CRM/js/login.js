(function () {
  // 登录的  得获取用户名 密码 和登录按钮
  let nameBox = document.querySelector('.userName');
  let pswBox = document.querySelector('.userPass')
  let subBtn = document.querySelector('.submit')
  subBtn.onclick = function () {
    //  获取用户名密码 传给后端  /^ +| +$/g
    let account = nameBox.value.trim(),
      password = pswBox.value.trim();
    if (account && password) {
      http.post('/user/login', {
        account,
        password: md5(password)
      }).then(data => {
        console.log(data)
        // 把用户信息及对应的token存储到localStorage中
        localStorage.setItem('crmUserInfo', JSON.stringify(data.info))
        localStorage.setItem('crmUserToken', data.token)
        // 登录成功之后跳转首页
        location.href = './index.html'
      })
    } else {
      alert('用户名密码不能为空')
    }
  }
})()