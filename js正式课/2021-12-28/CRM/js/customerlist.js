(function () {
  let selectBox = document.querySelector('.selectBox');
  let searchInp = document.querySelector('.searchInp');
  let searchBtn = document.querySelector('#searchBtn');
  let pageNumBox = document.querySelector('.pageNum');// 分页盒子
  let tbodyBox = document.querySelector('.tableBox tbody')
  // selectBox.onchange = function (e) {
  //   // 给下拉框绑定改变事件
  //   console.log(e.target.value)
  // }
  // console.log(location)
  // location.search   ?lx=my  ?lx=all
  // let lx = location.search.replace(/\?lx=/,'')
  let lx = location.search.match(/\?lx=(\w+)/)[1];
  localStorage.setItem('lx', lx)
  let page = 1, limit = 5;
  searchBtn.onclick = function () {
    page = 1
    getData().then(data => {
      // 点击搜索的时候 需要重新渲染分页
      renderPagination(data.totalPage)
    })
  }

  function renderPagination(n) {
    // n 代表的是一共多少页
    let str = '';
    for (let i = 1; i <= n; i++) {
      str += `<li class="${i == 1 ? 'active' : ''}" data-page='${i}'>${i}</li>`
    }
    pageNumBox.innerHTML = str;
    pageNumBox.onclick = function (e) {
      // 事件委托 实现页数切换
      if (!e.target.dataset.page) return
      page = e.target.dataset.page || page
      // 修改完页码之后 重新请求新数据
      getData();
      // 修改样式
      changePgColor(page)
    }
    prev.onclick = function () {
      // 点击上一页
      page--;
      if (page < 1) {
        page = 1
      } else {
        getData()
      }
      // 修改样式
      changePgColor(page)
    }
    next.onclick = function () {
      // 点击下一页
      page++;
      if (page > n) {
        page = n
      } else {
        getData()
      }
      // 修改样式
      changePgColor(page)
    }
  }
  function changePgColor(n) {
    // n 对应是页码 1 2 3 4 --》 索引为0 1 2 3的那个li高亮
    let list = pageNumBox.querySelectorAll('li')
    list.forEach(item => {
      item.classList.remove('active')
    })
    list[n - 1].classList.add('active')

  }

  function getData() {
    // lx=my&type=''&search=''&limit=10&page=1
    return http.get('/customer/list', {
      params: {
        lx: lx,
        type: selectBox.value,
        search: searchInp.value,
        limit: limit,
        page: page
      }
    }).then(data => {
      renderList(data.data)// 每次获取数据之后都要渲染数据
      return data
    })
  }
  getData().then(qqq => {
    // 只有初始获取数据的那一次 我们才需要渲染分页
    console.log(qqq)
    renderPagination(qqq.totalPage)
  })

  function renderList(ary) {
    // 渲染列表的
    let str = ``;
    ary.forEach(item => {
      let { name, sex, id, email, phone, weixin, QQ, type, userName, address } = item;
      str += `<tr>
        <td class="w8">${name}</td>
        <td class="w5">${sex == 0 ? '男' : '女'}</td>
        <td class="w10">${email}</td>
        <td class="w10">${phone}</td>
        <td class="w10">${weixin}</td>
        <td class="w10">${QQ}</td>
        <td class="w5">${type}</td>
        <td class="w8">${userName}</td>
        <td class="w20">${address}</td>
        <td class="w14">
          <a href="./customeradd.html?id=${id}" data-id=${id} data-type='edit'>编辑</a>
          <a href="javascript:;" data-id=${id} data-type='del'>删除</a>
          <a href="./visit.html?id=${id}" data-id=${id} data-type='rev'>回访记录</a>
        </td>
      </tr>`
    })

    tbodyBox.innerHTML = str;
  }
  tbodyBox.onclick = function (e) {
    // 删除的判断
    let id = e.target.dataset.id;
    let type = e.target.dataset.type;
    if (!id) return;
    if (type == 'del') {
      // console.log('点击了删除')
      let res = confirm("是否确认删除?")
      if (res) {
        // 确定删除
        http.get('/customer/delete', {
          params: {
            customerId: id
          }
        }).then(data => {
          // 删除成功  重新渲染列表
          getData()
        })
      }
    }
  }
})()