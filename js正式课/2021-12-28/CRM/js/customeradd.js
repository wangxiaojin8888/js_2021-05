(function () {
  let username = document.querySelector('.username')
  let man = document.querySelector('#man')
  let woman = document.querySelector('#woman')
  let useremail = document.querySelector('.useremail')
  let userphone = document.querySelector('.userphone')
  let userqq = document.querySelector('.userqq')
  let userweixin = document.querySelector('.userweixin')
  let usertype = document.querySelector('.usertype')
  let address = document.querySelector('.address')
  let submit = document.querySelector('.submit')
  // ?id=ccccc
  let arr = location.search.match(/id=(\w+)/)
  let id = arr && arr[1];
  //  从 url上获取 id
  console.log(id)
  //  获取到id之后  我们要根据id请求对应的数据详情 然后回显到当前页面
  // id 存在 那就是编辑；  id不存在 那就是新增
  if (id) {
    http.get('/customer/info', {
      params: {
        customerId: id
      }
    }).then(data => {
      // console.log(data)
      let { QQ,
        address: adrs,
        department,
        departmentId,
        email,
        id,
        name,
        phone,
        sex,
        type,
        userId,
        userName,
        weixin } = data.info
      username.value = name;
      sex == 0 ? man.checked = true : woman.checked = true;
      useremail.value = email
      userphone.value = phone
      userqq.value = QQ
      userweixin.value = weixin
      usertype.value = type
      address.value = adrs

    })

    // woman.checked = true
  } else {

  }
  submit.onclick = function () {
    if (id) {
      // customerId=xxx&name=xxx&sex=xxx&email=xxx&phone=xxx&
      //QQ=xxx&weixin=xxx&type=xxx&address=xxx
      console.log("修改")
      http.post('/customer/update', {
        customerId: id,
        name: username.value,
        sex: man.checked ? 0 : 1,
        email: useremail.value,
        phone: userphone.value,
        QQ: userqq.value,
        weixin: userweixin.value,
        type: usertype.value,
        address: address.value
      }).then(data => {
        // 修改成功之后 给用户响应  然后跳转
        alert('修改成功')
        let lx = localStorage.getItem('lx') || 'my'
        location.href = './customerlist.html?lx=' + lx
      })
    } else {
      // 新增
      http.post('/customer/add', {
        name: username.value,
        sex: man.checked ? 0 : 1,
        email: useremail.value,
        phone: userphone.value,
        QQ: userqq.value,
        weixin: userweixin.value,
        type: usertype.value,
        address: address.value
      }).then(data => {
        // 新增成功之后 给用户响应  然后跳转到我的客户
        alert('新增成功')
        location.href = './customerlist.html?lx=my'
      })
    }
  }
})()