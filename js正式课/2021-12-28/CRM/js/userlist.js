(function () {
  let selectBox = document.querySelector('.selectBox');
  let searchInp = document.querySelector('.searchInp')
  let tbody = document.querySelector('#tbody');
  let checkAll = document.querySelector('#checkAll')// 全选框
  let deleteAll = document.querySelector('.deleteAll');
  // 获取下拉框的内容(部门列表)
  getSelectList();
  function getSelectList() {
    http.get('/department/list').then(data => {
      console.log(data)
      // 需要把data.data中的数据展示到 下拉框中
      let str = '<option value="0">全部</option>';
      data.data.forEach(item => {
        str += `<option value="${item.id}">${item.name}</option>`
      })
      selectBox.innerHTML = str;
      getData() // 等到下拉框的数据请求完成之后再去请求列表数据
    })
  }
  function getData() {
    //  请求数据用的
    http.get('/user/list', {
      departmentId: selectBox.value,
      search: searchInp.value
    }).then(data => {
      // console.log(1111, data)
      renderList(data.data)
    })

  }

  function renderList(ary) {
    let str = '';
    ary.forEach(item => {
      let { id,
        name,
        sex,
        email,
        phone,
        departmentId,
        department,
        jobId,
        job,
        desc,
        power
      } = item;
      str += `<tr>
        <td class="w3"><input type="checkbox" data-id='${id}'></td>
        <td class="w10">${name}</td>
        <td class="w5">${sex == 0 ? '男' : '女'}</td>
        <td class="w10">${department}</td>
        <td class="w10">${job}</td>
        <td class="w15">${email}</td>
        <td class="w15">${phone}</td>
        <td class="w20">${desc}</td>
        <td class="w12">
          <a href="./useradd.html?id=${id}">编辑</a>
          <a href="javascript:;">删除</a>
          <a href="javascript:;">重置密码</a>
        </td>
      </tr>`
    })
    tbody.innerHTML = str;
    checkBind();
  }
  //  给全选框绑定选中事件
  checkAll.onchange = function (e) {
    console.dir(e.target.checked)
    tbody.querySelectorAll('input').forEach(item => {
      item.checked = e.target.checked
    })
  }
  function checkBind() {
    let inputs = tbody.querySelectorAll('input');
    inputs.forEach(item => {
      item.onchange = function (e) {
        // console.log(e.target.checked)
        let flag = [...inputs].every(v => v.checked)
        checkAll.checked = flag;
        // [...inputs].every(v => v.checked) 只有当每一个inp的checked都是true的时候 返回值才是true
      }
    })
  }

  deleteAll.onclick = function () {
    // 获取所有被选中的那些数据的id
    let inputs = tbody.querySelectorAll('input');
    let arr = [];
    inputs.forEach(item => {
      if (item.checked) {
        arr.push(item.dataset.id)
      }
    })
    console.log(arr)
  }



})()