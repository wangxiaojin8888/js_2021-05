//  这个文件是用来对axios做一些二次封装的
let http = axios.create({
  baseURL: 'http://localhost:9999', // 后端服务的接口
  headers: {
    // Accept: '123123123'。
    authorzation: localStorage.getItem('crmUserToken')
  }
})
// 请求拦截器
http.interceptors.request.use((config) => {
  console.log(config)
  // 每一个请求都会执行这个函数
  // config.headers.authorzation = localStorage.getItem('crmUserToken')
  return config
}, (err) => {
  return Promise.reject(err)
})

// 响应拦截器
http.interceptors.response.use((response) => {
  if (response.data.code != 0) {
    alert(response.data.codeText) // 弹出后台给的错误信息
    return Promise.reject(response.data) //返回一个失败态
  }
  return response.data
}, (err) => {
  alert("系统繁忙！")
  return Promise.reject(err)
})