(function () {
  let userNameBox = document.querySelector('.userNameBox')
  let menuBox = document.querySelector('.menuBox')
  let navBox = document.querySelector('.navBox')
  // 从localStorage中获取用户信息
  let userInfo = JSON.parse(localStorage.getItem('crmUserInfo'))
  if (!userInfo) {
    //  证明不在登录态 跳转登录页
    location.href = 'http://127.0.0.1:5500/js%E6%AD%A3%E5%BC%8F%E8%AF%BE/2021-12-28/CRM/login.html'
    return
  }
  userNameBox.innerHTML = `你好：${userInfo.name}`
  //userInfo.power   "userhandle|departhandle|jobhandle|customerall"
  let power = userInfo.power.split("|");// 把用户权限转成一个数组
  console.log(power)

  // 考虑左侧导航栏里边的内容
  function renderCus() {
    let str = `
      <div class="itemBox" text="客户管理">
				<h3>
					<i class="iconfont icon-kehuguanli"></i>
					客户管理
				</h3>
				<nav class="item">
					<a href="page/customerlist.html?lx=my" target="iframeBox">我的客户</a>
					${power.includes('customerall') ? '<a href="page/customerlist.html?lx=all" target="iframeBox">全部客户</a>' : ''}
					<a href="page/customeradd.html" target="iframeBox">新增客户</a>
				</nav>
			</div>`
    menuBox.innerHTML = str;

  }
  function renderOrg() {
    // 渲染 组织结构 的导航
    let str = '';
    if (power.includes('userhandle')) {
      // 证明当前用户 有 员工管理的权限
      str += `<div class="itemBox" text="员工管理">
        <h3>
          <i class="iconfont icon-yuangong"></i>
          员工管理
        </h3>
        <nav class="item">
          <a href="page/userlist.html" target="iframeBox">员工列表</a>
          <a href="page/useradd.html" target="iframeBox">新增员工</a>
        </nav>
      </div>`
    }
    if (power.includes('departhandle')) {
      // // 证明当前用户 有 部门管理的权限
      str += `<div class="itemBox" text="部门管理">
        <h3>
          <i class="iconfont icon-guanliyuan"></i>
          部门管理
        </h3>
        <nav class="item">
          <a href="page/departmentlist.html" target="iframeBox">部门列表</a>
          <a href="page/departmentadd.html" target="iframeBox">新增部门</a>
        </nav>
      </div>`
    }
    if (power.includes('jobhandle')) {
      // // 证明当前用户 有 职务管理的权限
      str += `<div class="itemBox" text="职务管理">
        <h3>
          <i class="iconfont icon-zhiwuguanli"></i>
          职务管理
        </h3>
        <nav class="item">
          <a href="page/joblist.html" target="iframeBox">职务列表</a>
          <a href="page/jobadd.html" target="iframeBox">新增职务</a>
        </nav>
      </div>`
    }
    menuBox.innerHTML = str
  }
  renderCus() // 刚进页面的时候 渲染的应该是客户管理

  // 给客户管理和组织结构这两个按钮绑定点击事件
  navBox.onclick = function (e) {
    if (e.target.dataset.type == 1) {
      // 证明点击的是客户管理
      renderCus()
      e.target.nextElementSibling.classList.remove('active')
      e.target.classList.add('active')
    } else if (e.target.dataset.type == 2) {
      // 证明点击的组织结构
      renderOrg()
      e.target.previousElementSibling.classList.remove('active')
      e.target.classList.add('active')
    }
  }
})()