(function () {
  let contentBox = document.querySelector('.content_box')
  let img = document.querySelector('.header_box>img')
  let descBox = document.querySelector('.header_box .desc_box')
  let author = document.querySelectorAll('.author_box span')[0]
  function getUrlData(url) {
    let obj = {}
    let reg = /([^?=&]+)=([^?=&]+)/g;
    url.replace(reg, (a, b, c) => {
      obj[b] = c
    })
    return obj;
  }
  // location.href 地址栏里边的所有内容 拿到的是一个字符串
  // navigator.userAgent 用来检测运行设备的

  let obj = getUrlData(location.href)
  console.log(obj.articalId)

  function getData(url) {
    return new Promise((res, rej) => {
      let xhr = new XMLHttpRequest;
      xhr.open('get', url)
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          let data = JSON.parse(xhr.response)
          res(data)
        }
      }
      xhr.send()
    })
  }
  getData('http://localhost:8888/api/article?id=' + obj.articalId).then(data => {
    console.log(data)
    renderContent(data.content)
    img.src = data.img;
    descBox.innerHTML = data.title;
    author.innerHTML = `作者：${data.author}`
  })
  function renderContent(ary) {
    let str = '';
    ary.forEach(item => {
      if (item.type == 'text') {
        str += `<p>${item.text}</p>`
      } else {
        str += `<img src='${item.img}' />`
      }
    })
    contentBox.innerHTML = str;
  }


  if (new Date().getHours() >= 17) {
    document.body.classList.add('night')
  }
})()