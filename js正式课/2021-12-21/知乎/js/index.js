(function () {
  let dayBox = document.querySelector('.day_box'),
    monthBox = document.querySelector('.month_box'),
    helloBox = document.querySelector('.middle_box h2');
  let swiperWrapper = document.querySelector('.swiper-wrapper');
  let listBox = document.querySelector('.list_box')
  let loadMoreBox = document.querySelector('.loadMoreBox')
  let cliH = document.documentElement.clientHeight || document.body.clientHeight;
  let flag = true;
  let pageNum = 1;// 记录加载的次数
  function renderHeader() {
    // 处理 头部内容的
    let date = new Date();
    let day = date.getDate(),// 几号
      month = date.getMonth() + 1,// 几月
      hour = date.getHours();
    dayBox.innerHTML = day;
    monthBox.innerHTML = month + '月';

    // 早  上  下 晚
    switch (true) {
      case hour < 8:
        helloBox.innerHTML = '早上好!'
        break;
      case hour >= 8 && hour < 12:
        helloBox.innerHTML = '上午好!'
        break;
      case hour >= 12 && hour < 18:
        helloBox.innerHTML = '下午好!'
        break;
      default:
        helloBox.innerHTML = '晚上好!'
        break;
    }
  }

  renderHeader()
  // 轮播图 获取；  列表获取
  function getData(url) {
    return new Promise((res, rej) => {
      let xhr = new XMLHttpRequest;
      xhr.open('get', url)
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          let data = JSON.parse(xhr.response)
          res(data)
        }
      }
      xhr.send();
    })
  }
  // 获取轮播图的数据
  getData('http://localhost:8888/api/swiper').then(data => {
    console.log(data)
    renderSwiper(data)// 根据数据渲染轮播图
    //  结构渲染完成之后 再去 初始化轮播图
    var swiper = new Swiper(".mySwiper", {
      loop: true, // 无缝滚动
      autoplay: {
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        //type: 'fraction',
        //type : 'progressbar',
        //type : 'custom',
      },
    });
  })
  // 封装渲染轮播图的函数
  function renderSwiper(ary = []) {
    let str = ``;
    ary.forEach(item => {
      str += `
      <div class="swiper-slide">
        <img src='${item.img}' />
        <div class="bot_box">
          <div class="til_box">${item.title}</div>
          <div class="author_box">作者:${item.author}</div>
        </div>
      </div>
      `
    })
    swiperWrapper.innerHTML = str;
  }

  // 获取列表数据
  function getListData() {
    pageNum++
    getData('http://localhost:8888/api/articleList?date=2021-05-21').then(data => {
      // console.log(data)
      renderList(data)
      flag = true;
    })
  }
  getListData()

  // 渲染列表
  function renderList(ary = []) {
    let str = '';
    ary.forEach(item => {
      str += `<li class="item_box">
        <a href='./pages/info.html?articalId=${item.id}'>
          <div class="left_content_box">
            <div class="title_box">${item.title}</div>
            <div class="desc_box">${item.author} ${item.des}分钟阅读</div>
          </div>
          <div class="right_pic_box">
            <img src="${item.img}" alt="">
          </div>
        </a>
      </li>`
    })
    listBox.innerHTML += str
  }

  function loadMore() {
    // 根据盒子 到视口的偏移量 跟 视口的高度比较
    let t = loadMoreBox.getBoundingClientRect().top
    if (t < cliH) {
      // 证明 露底了
      if (!flag) return;
      flag = false
      console.log("加载更多")
      if (pageNum > 5) {
        alert('已经请求五次了')
      } else {
        getListData()
      }

    }
  }
  window.onscroll = function () {
    loadMore()
  }


})()