$(function(){
    //是否为夜间模式
    let query=location.search;
    //console.log(query);
    if(query){
        console.log("夜间模式");
        $(".loginbox").addClass("dark");
    }else{
        console.log("正常模式");
        $(".loginbox").removeClass("dark");
    }
    
    //返回首页
    $(".icon-fanhui").click(function(){
       location.href="../index.html";
    })
    //点击知乎
    $("#zhihu").click(function(){
        //根据下面input框是否选中来判断
        if($("#read").prop("checked")){//true 选中
            alert("点击了知乎");
        }else{//false 没选中
            alert("请先阅读下面内容");
        }
    })
    //点击微博
    $("#weibo").click(function(){
        if($("#read").prop("checked")){
            alert("点击了微博");
        }else{
            alert("请先阅读下面内容");
        }
    })

    let url="setting.html";
    //点击进入设置页面
    $(".set").click(function(){
        if($(".loginbox").hasClass("dark")){
           url+="?id=dark";
        }else{
            url="setting.html";
        }
        location.href=url;
    })

    //夜间模式切换
    $(".yejian").click(function(){
        if($(".loginbox").hasClass("dark")){//夜间--》正常
            $(".loginbox").removeClass("dark");
            url="setting.html";
         }else{//正常---》夜间
            $(".loginbox").addClass("dark");
            url+="?id=dark";
         }
    })

})