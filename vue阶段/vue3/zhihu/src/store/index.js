import { createStore, createLogger } from 'vuex';
import api from '@/api';

const env = process.env.NODE_ENV;
const store = createStore({
    state: {
        isLogin: null,
        info: null,
        storeList: null
    },
    mutations: {
        changeIsLogin(state, bool) {
            state.isLogin = bool;
        },
        changeInfo(state, info) {
            state.info = info;
        },
        queryStoreList(state, payload) {
            state.storeList = payload;
        },
        removeStoreList(state, id) {
            if (!state.storeList) return;
            state.storeList = state.storeList.filter(item => {
                return +item.id !== +id;
            });
        }
    },
    actions: {
        async changeIsLoginAsync({ commit }) {
            let { code, data } = await api.checkLogin();
            if (+code !== 0) {
                commit('changeIsLogin', false);
                return false;
            };
            commit('changeIsLogin', true);
            commit('changeInfo', data);
            return true;
        },
        async changeInfoAsync({ commit }) {
            let { code, data } = await api.userInfo();
            if (+code !== 0) return null;
            commit('changeInfo', data);
            return data;
        },
        async queryStoreListAsync({ commit }) {
            let { code, data } = await api.storeList();
            if (+code !== 0) data = [];
            commit('queryStoreList', data);
            return data;
        }
    },
    plugins: env !== 'production' ? [createLogger()] : []
});
export default store;