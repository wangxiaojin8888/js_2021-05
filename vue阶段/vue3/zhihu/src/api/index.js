import axios from "./http";

// 获取今日新闻&轮播图信息
const queryNewsLatest = () => {
    return axios.get('/api/news_latest');
};

// 获取以往的新闻信息
const queryNewsBefore = (time) => {
    return axios.get('/api/news_before', {
        params: {
            time
        }
    });
};

// 获取新闻详情
const queryNewsInfo = (id) => {
    return axios.get('/api/news_info', {
        params: {
            id
        }
    });
};

// 获取新闻的评论数等信息
const queryNewsComments = (id) => {
    return axios.get('/api/story_extra', {
        params: {
            id
        }
    });
};

// 用户登录/注册
const login = (data) => {
    return axios.post('/api/login', data);
};

// 发送验证码
const phoneCode = (phone) => {
    return axios.post('/api/phone_code', {
        phone
    });
};

// 校验是否登录
const checkLogin = () => {
    return axios.get('/api/check_login');
};

// 获取登录者信息
const userInfo = () => {
    return axios.get('/api/user_info');
};

// 获取收藏列表
const storeList = () => {
    return axios.get('/api/store_list');
};

// 收藏新闻
const store = (newsId) => {
    return axios.post('/api/store', {
        newsId
    });
};

// 移除收藏
const storeRemove = (id) => {
    return axios.get('/api/store_remove', {
        params: {
            id
        }
    });
};

// 修改用户信息
const userUpdate = (username, file) => {
    let fm = new FormData();
    fm.append('username', username);
    fm.append('file', file);
    return axios.post('/api/user_update', fm);
};

export default {
    queryNewsLatest,
    queryNewsBefore,
    queryNewsInfo,
    queryNewsComments,
    login,
    phoneCode,
    checkLogin,
    userInfo,
    storeList,
    store,
    storeRemove,
    userUpdate
};