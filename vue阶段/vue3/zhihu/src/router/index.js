import { createRouter, createWebHashHistory } from 'vue-router';
import routes from './routes';
import store from '@/store';
import { Toast } from 'vant';

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

// 全局前置守卫:每一次路由跳转(或最开始打开页面)都会触发这个守卫函数
router.beforeEach(async (to, _, next) => {
    let unsafe = ['/person', '/update', '/store'];
    if (unsafe.includes(to.path)) {
        // 需要登录才能进入
        let { isLogin } = store.state;
        if (isLogin === null) {
            isLogin = await store.dispatch('changeIsLoginAsync');
        }
        if (!isLogin) {
            Toast.fail('请您先登录');
            next(`/login?to=${to.path}`);
            return;
        }
    }
    next();
});

export default router;