import axios from "./http";

// 获取指定状态的任务
const getTaskList = (state = 0) => {
    return axios.get('/api/getTaskList', {
        params: {
            state
        }
    });
};

// 删除指定任务
const removeTask = (id) => {
    return axios.get('/api/removeTask', {
        params: {
            id
        }
    });
};

// 把指定任务设置为完成
const completeTask = (id) => {
    return axios.get('/api/completeTask', {
        params: {
            id
        }
    });
};

// 新增任务  body是一个对象，对象中包含:task/time
const addTask = (body) => {
    return axios.post('/api/addTask', body);
};

export default {
    getTaskList,
    removeTask,
    completeTask,
    addTask
};