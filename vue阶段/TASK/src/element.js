/* 按需导入需要的UI组件：把导入的组件注册为全局组件 */
import Vue from 'vue';
import { Icon, Button, Link, Input, DatePicker, Form, FormItem, Table, TableColumn, Tag, Message, MessageBox, Loading, Dialog } from 'element-ui';

Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox.confirm;

Vue.use(Loading.directive); //在全局创建了v-loading的自定义指令
Vue.use(Icon);
Vue.use(Button);
Vue.use(Link);
Vue.use(Input);
Vue.use(DatePicker);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Tag);
Vue.use(Dialog);