import Vue from 'vue';
import App from '@/App.vue';
// element-ui
import '@/assets/reset.min.css';
import '@/element';
// api
import api from '@/api';

Vue.prototype.$api = api;
Vue.config.productionTip = false;
new Vue({
  render: h => h(App),
}).$mount('#app');