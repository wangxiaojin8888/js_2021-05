const ENV = process.env.NODE_ENV; //获取Node环境变量，$ npm run serve启动的时候，环境变量值是development，$ npm run build 启动的时候，值是production!
module.exports = {
    lintOnSave: ENV !== 'production', //控制ESLint词法检测的规则：开发环境下，结果是TRUE（基于ESLint进行词法检测，但检测出语法问题后，只是提示错误，不影响项目的编译）如果是生产环境，值是FALSE（不进行ESLint词法检测，以此优化打包速度！）
    publicPath: './', //控制打包后导入资源文件的路径
    productionSourceMap: false, //控制打包过程中，不去生成js的map调试文件，以此加快打包的速度「一般不会对线上部署后的代码进行调试」
    devServer: {
        // 这里可以基于webpack-dev-server的配置项在此进行配置修改
        // host: '127.0.0.1',
        // open: true,
        // 基于proxy实现跨域代理
        proxy: {
            '/api': {
                target: 'http://127.0.0.1:9000',
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
};