import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: "准风格",
    age: 13,
    count: 100
  },
  mutations: {
    changeName(state, str) {
      state.name = str
    },
    changeAge(state, age) {
      state.age = age
    },
    changCount(state, n) {
      state.count += n
    }
  },

})
