import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: [{ text: "测试", id: 123 }]
  },
  mutations: {
    addTodo(state, option) {
      state.todoList.unshift(option)
    },
    removeTodo(state, id) {
      state.todoList = state.todoList.filter(item => item.id != id)
    }
  },

})
