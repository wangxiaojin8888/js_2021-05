# project4

router-link  router-view  是router提供的两个全局组件

router-link  有一个to属性 值对应的是点击这个元素的时候跳转到哪个地方

router-link-exact-active  只有当前地址栏的路径和 to一样的时候 才会有这个类名
router-link-active  只要当前地址栏的路径包含to的时候 就会有这个类名


<router-view /> 是用来展示当前路由对应的 组件

跳转有两种：  router-link   push
传参有两种：  query         params

获取的时候 通过this.$route.query 或者 this.$route.params
