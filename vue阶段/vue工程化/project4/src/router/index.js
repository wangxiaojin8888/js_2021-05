import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import About from '../views/About.vue'
console.log(Home)

Vue.use(VueRouter)

// 路由映射表
// const routes = [
//   {
//     path: '/',
//     component: Home
//   },
//   {
//     path: '/about',
//     // component: About
//     // 路由懒加载
//     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
//   }
// ]
console.log(process.env.BASE_URL, process.env.NODE_ENV)
/* 
process.env.NODE_EN  : 'development'   'production'

npm run serve  process.env.NODE_EN  ===  'development'
npm run build  process.env.NODE_EN  ===  'production'

*/
let routes = [
  {
    path: '/', // 路由重定向
    redirect: '/a'
  },

  {
    path: '/a',
    name: 'aaa',
    component: () => import(/*webpackChunkName:"AAA"*/'../views/A.vue')
  },
  {
    path: '/b',
    component: () => import(/*webpackChunkName:"b"*/'../views/B.vue')
  },
  {
    path: '/c',
    component: () => import(/*webpackChunkName:"c"*/'../views/C.vue'),
    children: [
      {
        path: '/c/c1/:ttt',
        name: 'c1',
        component: () => import(/*webpackChunkName:"c_c1" */'../views/C/c1.vue')
      },
      {
        path: '/c/c2',
        component: () => import(/*webpackChunkName:"c_c2" */'../views/C/c2.vue')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('../components/HelloWorld.vue')
  },
  {
    path: '/*', // 通配符    放在映射表的最后边
    redirect: '/404'
  },
]
const router = new VueRouter({
  mode: 'history',//'history'  'hash'
  base: process.env.BASE_URL,
  routes: routes,
  // linkActiveClass: 'qqq', // router-link-active 
  // linkExactActiveClass: 'www' //router-link-exact-active 
})

export default router
