import http from '@/utils/http.js'

export function getList(state) {
  // /getTaskList
  return http.get('/getTaskList?state=' + state)
}
export function addAll(option) {
  // option：{task,time}
  return http.post('/addTask', option)
}

export function del(id) {
  return http.get('/removeTask?id=' + id)
}

export function complete(id) {
  return http.get('/completeTask?id=' + id)
}