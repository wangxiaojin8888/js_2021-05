import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import All from '../views/All.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'All',
    component: All
  },
  {
    path: '/undo',
    component: () => import(/*webpackChunkName:"undo"*/'../views/Undo.vue')
  },
  {
    path: '/done',
    component: () => import(/*webpackChunkName:"done"*/'../views/Done.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
