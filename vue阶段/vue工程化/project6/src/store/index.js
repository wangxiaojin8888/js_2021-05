import Vue from 'vue'
import Vuex from 'vuex'
import { getList } from '../api/index'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    allList: [],
    undoList: [],
    doneList: []
  },
  mutations: {
    changeAll(state, list) {
      state.allList = list;
    },
    changeUndo(state, list) {
      state.undoList = list;
    },
    changeDone(state, list) {
      state.doneList = list;
    },
  },
  actions: {
    // this.$store.dispatch('changeAllFn')
    changeUndoFn(store) {
      // 调用接口 请求数据
      getList(1).then(data => {
        // console.log(data)
        // data.list
        store.commit('changeUndo', data.list)
      })
    },
    changeAllFn(store, option) {
      // 调用接口 请求数据
      getList(0).then(data => {
        // console.log(data)
        // data.list
        store.commit('changeAll', data.list)
      })
    }
  },
  modules: {
  }
})
