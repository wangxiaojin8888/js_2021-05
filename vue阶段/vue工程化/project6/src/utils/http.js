// http  request   api
import axios from 'axios'
let instance = axios.create({
  baseURL: process.env.NODE_ENV == 'production' ? 'http://localhost:9000' : '',
  timeout: 3000,
  // form-data
  headers: {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  transformRequest(data = {}) {
    // 这个函数是用来同意处理post传递的参数的
    console.log(data)
    let str = '';
    Object.keys(data).forEach(key => {
      str += `${key}=${data[key]}&`
    })
    str = str.replace(/&$/, '')
    return str
  }
});
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  // console.log(config)
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response.data;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
})

export default instance