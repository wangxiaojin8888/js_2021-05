module.exports = {

  devServer: {
    // 只有是 本地的请求  https://localhost:8080/qq
    // changeOrigin: true,
    proxy: {
      '/': {
        target: 'https://iot.wumei.live/prod-api',
        changeOrigin: true,
      }
    }
  },
  configureWebpack: {
    // 这个属性中的内容 最终会被合并到 webpack的配置当中
    // 所以这里就可以编写  webpack的配置了
    entry: './src2/index.js',
    externals: {
      jquery123: '$',
    },
  }
}