import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    // 存放公用数据
  },
  mutations: {
    //存放的是用来修改state的方法  通过 store.commit('qq')
    // 必须是同步函数
    qq(state,) { }
  },
  actions: {
    // 由于mutations只能是同步的  那么异步都翻在actions  store.dispatch
    qq(store) { }
  },
  getters: {
    // vuex 的计算属性  很少用到
  },
  modules: {
    // 模块化
    qq: {
      namespaced: true, // store.commit('qq/ww')
      mutations: {
        ww() { }
      }
    }
  }
})
/*

mapState
mapMutations
mapActions
mapGetters


*/