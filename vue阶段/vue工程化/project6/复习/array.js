/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

import { def } from '../util/index'
// def Jiushi用来劫持属性的一个函数


// 创造一个变量 指向 数组类的原型
const arrayProto = Array.prototype

// 创造一个空对象 但是这个空对象的__proto__是指向 数组的原型的
export const arrayMethods = Object.create(arrayProto)
//这个对象导出之后 被用来设置成vue中的数组的原型了

const methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
]

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  const original = arrayProto[method];
  // 使用original 存储了 数组原上上的那个方法

  // 给arrayMethods增加一个push属性 值是mutator函数
  // this.ary.push
  def(arrayMethods, method, function mutator(...args) {
    const result = original.apply(this, args)
    const ob = this.__ob__
    let inserted
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args
        break
      case 'splice':
        inserted = args.slice(2)
        break
    }
    if (inserted) ob.observeArray(inserted)
    // notify change
    ob.dep.notify()// 用来通知给个watcher执行update方法来更新视图
    // ob.dep.subs里边存储的都是使用当前数组的那些watcher
    return result
  })
})
