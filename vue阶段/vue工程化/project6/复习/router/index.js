import VueRouter from 'vue-router'
import Vue from 'vue'

Vue.use(VueRouter)
/*  
  query传参 

  params传参  配置路由映射表

  beforeEach((to,from,next)=>{})

*/
export default new VueRouter({
  mode: 'history',// hash hashchange事件;history popstate事件+pushState/replaceState
  routes: [
    {
      path: '/a/:t/:y',
      name: 'aaa',
      component: () => import('../compoents/A.vue'),
      meta: {
        title: "AAAA"
      }
    },
    {
      path: '/b',
      name: 'bbb',
      component: () => import('../compoents/B.vue'),
      meta: {
        title: "BBB"
      }
    }
  ]
})