import Vue from 'vue'
import App from './App2.vue'
import el from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
Vue.use(el)
// console.log(App)
// // Object.prototype.name = 888
// Vue.directive('color', (el, obj) => {
//   // console.log('color指令', el, obj)
//   el.style.color = obj.value
// })
// Vue.directive('color2', {
//   // bind() { },
//   inserted() { },
//   // update() { },
//   componentUpdated() { },
//   unbind() { }
// })
// // Vue.component  components
// // Vue.filter   filters
Vue.mixin({
  created() {
    console.log(this.name, "全局混入的created")
    this.name = 888
  },
  data() {
    return {
      qqq: 123
    }
  },
  methods: {
    fn() {
      console.log(666)
    }
  },
})
let vm = new Vue({
  el: '#app',
  router,
  store,
  render(createElement) {
    return createElement(App)
  }
})






































/*
  commonjs(node)   项目中的配置文件 module.exports = {}
                                   let a =  require('qqq')

  esmodule(浏览器)  项目中的业务代码 基本都是esmodule
    import zzz from './xxx'    export default
    import {aaa} from './xxxx'   export 声明关键字  aaa =

    import * as qqq from './xx'


    Object.defineProperty(obj,'aa',{
      get(){},
      set()
    })

    let mishu = new Proxy(obj)

*/