是否了解webpack?

了解，我们工作中经常会配置 vue.config.js ,
经常会配置  devServer来实现代理，这个配置项其实
就是webpack-dev-server 的配置，
webpack 有几个基础的配置  mode  entry  output  module  plugins

module  rules 用来配置各个loader的
plugins 是用来配置各个插件的
  经常用的 loader有 
    css-loader：加载 CSS，支持模块化、压缩、文件导入等特性
    postcss-loader：扩展 CSS 语法，使用下一代 CSS，可以配合 autoprefixer 插件自动补齐 CSS3 前缀
    babel-loader：把 ES6 转换成 ES5
    file-loader：把文件输出到一个文件夹中，在代码中通过相对 URL 去引用输出的文件 (处理图片和字体)
    url-loader：与 file-loader 类似，区别是用户可以设置一个阈值，大于阈值会交给 file-loader 处理，小于阈值时返回文件base64 形式编码 (处理图片和字体)
    vue-loader：加载 Vue.js 单文件组件

    mini-css-extract-plugin提供的loader: 使用 link 标签引入 外联的cs

    常用的插件有：
      html-webpack-plugin： 设置渲染页面的模板
      terser-webpack-plugin: 支持压缩 ES6 (Webpack4)
      mini-css-extract-plugin:分离样式文件，CSS 提取为独立文件，支持按需加载
      clean-webpack-plugin:目录清理
      define-plugin：定义环境变量
      DllPlugin:此插件用于在单独的 webpack 配置中创建一个dll-only-bundle。 此插件会生成一个名为 manifest.json 的文件，这个文件是用于让 DllReferencePlugin 能够映射到相应的依赖上

  还配置过 configerWebpack ，这个属性里边的配置其实就是webpack的配置，在这个里边 我配置过 resolve 下的alias 的api路径
  还配置过externals来实现通过cdn引入某些三方插件

let a:number = 1
function qq(m:number,q:string)void{}
interface  qq{
  q:number
}

let obj:qq = {
  q:23
}
reName(){
  return this.age + this.name
}

 每一个计算属性本质都是一个Watcher实例
这个实例会被当前使用的哪个元素收集到自己的subs中
每当依赖更新的时候 会把当前watcher实力的dirty属性重置成true
那么 当去在去使用reName的时候 会触发watcher的evaluate函数
这个函数的执行会更新watcher实例的value值，我们使用的reName
属性对应的值其实就是watcher.value,所以说若依赖没有更新，
那么调用rename属性的时候就不会触发evaluate，那也就不会更新这个value
也就是说用的是之前算出来的老value