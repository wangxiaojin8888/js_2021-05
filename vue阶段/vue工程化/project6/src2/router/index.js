import VueRouter from 'vue-router'
import Vue from 'vue'
Vue.use(VueRouter)

let routes = [
  {
    path: '/login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/404',
    component: () => import('../views/404.vue')
  },
  {
    path: '/',
    component: () => import('../views/layout.vue')
  }
]


export default new VueRouter({
  routes
})