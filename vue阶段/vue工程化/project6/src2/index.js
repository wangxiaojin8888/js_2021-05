import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import axios from 'axios'
import $ from 'jquery123'
conosle.log($)
axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.cookie.match(/Admin-Token=([^;]+)/)[1];
Vue.prototype.http = axios;
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')