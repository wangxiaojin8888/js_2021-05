import Vuex from 'vuex'
import Vue from 'vue'
import Layout from '../views/layout.vue'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    routes: []
  },
  mutations: {
    updateRoutes(state, arr) {
      state.routes = arr;
      handleRoutes(arr)
    }
  },
  actions: {}
})


function handleRoutes(routes) {
  function changeComponent(route) {
    if (route.component == 'Layout') {
      route.component = Layout
    } else {
      route.component = () => import(`@/views/${route.component}`)
    }
    if (route.children && route.children.length) {
      route.children.forEach(item => {
        changeComponent(item)
      })
    }
  }
  let newRoutes = routes.map(route => {
    changeComponent(route)
    return route
  })
}

/*
  项目的亮点  骄傲的  介绍项目
   之前的项目的路由权限处理都是在映射表中写死的 然后把相关权限存储到了 对应路由的
   meta中的role属性 这是一个数组 里边存储的哪些权限可以看到或者访问当前这个路由
    左侧导航是从映射表中获取所有的数据 然后从这些数据中 结合 用户的角色
    来筛选出具体要去渲染哪些路由
    还结合了导航首位 来去来拦截直接通过地址访问的路由

    后来由于产品有让新增一个角色 这是我们前端的代码 要根据新的角色来挨个修改
    路由的meta属性，所以说感觉这种写法有弊端，不太灵活；

    所以当后来有新的管理类的项目的时候， 采用动态路由的方式去编写导航及权限
    这种方式是登录成功之后 调用获取路由映射表的接口，然后把获取到的数据中的
    component属性 替换成对应的vue组件 采用的懒加载的方式，也就是做了一个字符串拼接
    替换时 由于要考虑子路由的问题 所以采用的时递归的方式处理的component
    这种写法的好处在于 前端不用去关心角色的变化 因为所有的能展示的路由都是后端
    根据当前用户的角色返回的，前端不用再去处理路由权限的问题。






*/