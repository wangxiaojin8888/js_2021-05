import Vue from 'vue'
import Vuex from 'vuex'
import AAA from './modules/a'
import BBB from './modules/b'
import CCC from './modules/c'

Vue.use(Vuex)

let store = new Vuex.Store({
  // state: { qqq: 555 },

  modules: {
    AAA123: AAA,
    BBB123: BBB,
    CCC123: CCC
  },

})
export default store