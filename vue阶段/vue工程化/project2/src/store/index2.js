import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
// const addCount = 'addCount'
// const addCount123 = 'addCount123'

let store = new Vuex.Store({
  state: {
    count: 100
  },
  mutations: {
    // 这里原则上是只能编写 同步函数；异步函数需要写到actions中
    // 组件中通过 this.$store.commit('函数名') 调用
    addCount(state, n) {
      state.count += n;
    },
    minusCount(state, n) {
      state.count -= n;
    }
  },
  actions: {
    //组件中通用过 this.$store.dispatch('函数名') 调用
    addCountAsync(store, option) {
      // 形参store 对应的是整个Store实例 也就是组件中的this.$store
      // 这里的函数一般都是用来编写异步请求的
      // 获取数据之后 还是要设置到vuex的state中
      // 由于更改state中的数据只能通过提交mutation
      // 所以 actions中的函数要想更改vuex中的state
      // 还是需要通过mutations中的函数实现
      setTimeout(() => {
        store.commit('addCount', option)
      }, 2000);

    }
  },
  getters: {
    //  相当于是vuex中的数据的计算属性
    type(state) {
      // state.count % 2 取模； 也就是取余数
      return state.count % 2 ? "奇数" : '偶数'
      // 只要count发生改变 那么 type就跟着改变
    }
  }
})
export default store