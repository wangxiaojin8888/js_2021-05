import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
/* 
  vuex 是一个vue插件  用来解决 组件间的数据交互问题的

  Vuex 数据持久化；

*/
const myPlugin = store => {
  // 当 store 初始化后调用
  let cb = (mutation, state) => {
    // 每次 mutation 之后调用 ， state是修改之后的最新的state
    // mutation 的格式为 { type, payload }
    // 每一次的mutation调用 都是为了更改state中的数据

    // 把最新的state存储到localStorage中
    localStorage.setItem("vuexStateqqqq", JSON.stringify(state))
  }
  store.subscribe(cb)
}
function qqq(store) {
  store.subscribe((mutaion, state) => {
    console.log("这个mutation", mutaion, '执行了')
  })
}

// let initList = JSON.parse(localStorage.getItem('qqqqqq'))

let store = new Vuex.Store({
  // 这里的属性都是给Store类的配置项

  // state: {
  //   // 这个属性里边是用来存放那些公用数据的
  //   qqq: 1235,
  //   list: initList || [{ text: '测试', id: 123 }]
  // },
  state: JSON.parse(localStorage.getItem('vuexStateqqqq')) || {
    qqq: 123,
    list: []
  },
  mutations: {
    // 存放的是用来修改state中的属性的方法
    addQQQ123(state, option) {
      console.log(state, option)
      state.qqq = state.qqq + option
    },
    addList123(state, option) {
      // console.log(state) 
      state.list.unshift(option)
      // localStorage.setItem('qqqqqq', JSON.stringify(state.list))
    },
    removeList123(state, id) {
      state.list = state.list.filter(item => item.id != id)
      // localStorage.setItem('qqqqqq', JSON.stringify(state.list))
    }
  },
  plugins: [myPlugin, qqq],
})
export default store


