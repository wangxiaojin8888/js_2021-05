import Vue from 'vue'
// import App from './App.vue'
// import Todo from './components/todo'
// import Count from './Count.vue'
import LM from './LearnModule.vue'
import store from './store/index3'

Vue.config.productionTip = false

new Vue({
  store: store, // 每一个渲染的组件都可以通过$store.state.xxx
  render: h => h(LM)
}).$mount('#app')
