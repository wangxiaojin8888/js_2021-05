
// Vue.use(vuex,1,2,3,4)
// Vue.use(vueRouter,{aa:111})
Vue.use = function (plugin) {
  // _installedPlugins 这个属性是用来存放已经use过的插件
  var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
  if (installedPlugins.indexOf(plugin) > -1) {
    return this
  }

  // additional parameters
  var args = toArray(arguments, 1); // [1,2,3,4]  [{aa:111}]
  args.unshift(this);// [Vue,1,2,3,4]  [Vue,{aa:111}]
  if (typeof plugin.install === 'function') {
    plugin.install.apply(plugin, args);
    // 如果插件里有一个属性install  并且他还是一个函数，那么就让这个install执行
    // 执行时 里边this是插件本身，把args当作实参传给了install
  } else if (typeof plugin === 'function') {
    plugin.apply(null, args);
  }
  installedPlugins.push(plugin);
  return this
};


/*
  use的原理：
    先以Vue的一个静态属性去判断是否是之前use过的插件，
      这块属于一个优化项

    在往下根据arguments创造一个数组，这个数组中第一项是Vue本身
    后边的项都是use某个插件时，传进来的其他的参数

    再往下就是判断插件的install属性是不是一个函数,
    是函数的化就让这个函数执行 并且把上边创造的数组当作实参通过apply
    传递给use的这个插件的install




*/