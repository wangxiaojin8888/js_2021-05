import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './utils/vant.js'
import './assets/icon.less'
import Goods from './components/Goods.vue'

Vue.config.productionTip = false
Vue.component('Goods', Goods)
Vue.filter('money', function (val) {
  return val.toFixed(2)
})
let myPlugin = {
  install(_Vue) {
    _Vue.qqq = function () { }
  }
}
console.log(Vue.use(myPlugin, 1, 2, 3, 4, 5))
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
