import {
  Button, Icon, Form, Field, NavBar, Swipe,
  SwipeItem, Grid, GridItem, GoodsAction, GoodsActionIcon,
  GoodsActionButton,
  Checkbox, Stepper, SubmitBar, AddressList, AddressEdit, Area,
  Tab, Tabs,
  List, Cell
} from 'vant'
import Vue from 'vue'
[Button, Icon, Form, Field, NavBar, Swipe,
  SwipeItem, Grid, GridItem,
  GoodsAction, GoodsActionIcon,
  GoodsActionButton, Checkbox, SubmitBar, AddressList, AddressEdit, Area,
  Stepper, Tab, Tabs, List, Cell].forEach(item => {
    Vue.use(item)
  })
// Vue.use(Button).use(Icon)