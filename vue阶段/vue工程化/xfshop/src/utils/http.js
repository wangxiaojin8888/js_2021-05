import axios from 'axios'
import { Notify } from 'vant'
import router from '../router'
let http = axios.create({
  baseURL: process.env.NODE_ENV == 'production' ? 'http://backend-api-01.newbee.ltd/api/v1' : '/api/v1'
})
// 添加请求拦截器
http.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  config.headers.token = localStorage.getItem('xftoken')
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});
console.log(router)
// 添加响应拦截器
http.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  let data = response.data;
  if (data.resultCode != 200) {
    // 证明后台给的不是我们要的数据
    if (!data.message.includes('已存在')) {
      Notify({ type: 'danger', message: data.message || "系统繁忙" });
    }
    if (data.resultCode == 416) {
      // 代表未登录
      // 如果当前路径就是/login 又去使用push 就会报重复跳转的错误
      if (router.currentRoute.path != '/login') {
        router.push('/login?needback=1')
      }

    }
    return Promise.reject(response.data);

  }
  return response.data;
}, function (error) {
  // 对响应错误做点什么
  Notify({ type: 'danger', message: "系统繁忙" });
  return Promise.reject(error);
});

export default http