import Vue from 'vue'
import Vuex from 'vuex'
import { getCartList } from '../api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartList: []
  },
  mutations: {
    changeCartList(state, list) {
      state.cartList = list.map(item => {
        item.checked = true
        return item
      });
    }
  },
  actions: {
    changeCartListAsync(store) {
      getCartList().then(data => {
        console.log(data)
        store.commit('changeCartList', data.data || [])
      })
    }
  },
  getters: {
    // 算出来的就是购物车的数量
    cartNum(state) {
      return state.cartList.reduce((prev, cur) => {
        return prev + cur.goodsCount
      }, 0)
    }
  }
})
