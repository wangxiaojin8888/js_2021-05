import http from '../utils/http'
import md5 from 'js-md5'
export function test() {
  return http.get('/shop-cart')
}

// /api/v1/user/register
export function register(name, psw) {
  return http.post('/user/register', {
    loginName: name,
    password: psw
  })
}

export function login(name, psw) {
  return http.post('/user/login', {
    loginName: name,
    passwordMd5: md5(psw)
  })
}

export function getIndexInfo() {
  return http.get('/index-infos')
}
export function getInfoData(id) {
  return http.get('/goods/detail/' + id)
}

export function addCart(options) {
  // options {goodsId,goodsCount}
  return http.post('/shop-cart', options)
}

export function getCartList() {
  return http.get('/shop-cart')
}

export function getAddressList() {
  return http.get('/address')
}

//请求地址详情数据
export function getAddressDetail(id) {
  return http.get('/address/' + id)
}

// 修改地址
export function updateAddress(obj) {
  return http.put('/address', obj)
}
// 新增接口
export function addAddress(obj) {
  return http.post('/address', obj)
}

// 获取订单列表
export function getOrderList(p, s) {
  // ?pageNumber=1&status=
  return http.get('/order', {
    params: {
      pageNumber: p,
      status: s
    }
  })
}

// 获取用户信息
export function getUserInfo() {
  return http.get('/user/info')
}