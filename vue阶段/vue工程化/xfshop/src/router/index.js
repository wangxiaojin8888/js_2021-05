import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: () => import(/*webpackChunkName:"home" */'../views/Home.vue'),
    meta: {
      // 路由元信息
      // 一般用来存储 路由相关信息 (权限之类的东西)
      isShowNav: true
    }
  },
  {
    path: '/list',
    component: () => import(/*webpackChunkName:"list" */'../views/List.vue'),
    meta: {
      isShowNav: true
    }
  },
  {
    path: '/cart',
    component: () => import(/*webpackChunkName:"cart" */'../views/Cart.vue'),
    meta: {
      isShowNav: true
    }
  },
  {
    path: '/user',
    component: () => import(/*webpackChunkName:"user" */'../views/User.vue'),
    meta: {
      isShowNav: true
    }
  },
  {
    path: '/login',
    component: () => import(/*webpackChunkName:"login" */'../views/Login.vue'),
    meta: {
      isShowNav: false // 控制的是是否展示下边的导航栏
    }
  },
  {
    path: '/info/:productId',
    component: () => import(/*webpackChunkName:"info" */'../views/Info.vue'),
    meta: {
      isShowNav: false // 控制的是是否展示下边的导航栏
    }
  },
  {
    path: '/addressList',
    component: () => import(/*webpackChunkName:"info" */'../views/AddressList.vue'),
    meta: {
      isShowNav: false // 控制的是是否展示下边的导航栏
    }
  },
  {
    path: '/addressEdit',
    component: () => import(/*webpackChunkName:"info" */'../views/AddressEdit.vue'),
    meta: {
      isShowNav: false // 控制的是是否展示下边的导航栏
    }
  },
  {
    path: '/order',
    component: () => import(/*webpackChunkName:"order" */'../views/Order.vue'),
    meta: {
      isShowNav: false, // 控制的是是否展示下边的导航栏
      til: "订单页"
    }
  },
  {
    path: '/create-order',
    component: () => import(/*webpackChunkName:"create-order" */'../views/CreateOrder.vue'),
    meta: {
      isShowNav: false // 控制的是是否展示下边的导航栏
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  document.title = to.meta.til || "新蜂商城"
  next()
})
export default router
