module.exports = {
  plugins: {
    'postcss-pxtorem': {
      rootValue: 37.5,// 根据设计稿的宽度 750 就写成75；375的就写成37.5
      propList: ['*'],
    },
  },
};