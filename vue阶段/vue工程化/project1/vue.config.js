// commonjs 规范； node遵循commonJS规范；webpack是基于node开发的
module.exports = {
  lintOnSave: false,// 关闭eslint检测
  configureWebpack: {
    //  这个对象中的属性 最终会直接合并到webpack中； 这里的属性都是webpack规定死的
    entry: './day2/index.js'// 不设置的时候 默认是./src/main.js
  }
}