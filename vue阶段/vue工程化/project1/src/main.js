import Vue from 'vue' // 不写路径的时候 是去node_modules中查找
import wwww from './App.vue'// .vue文件是再工程化中独有的文件 里边可以写结构 样式 和 逻辑
import zj2 from './zj/zj2.vue'
import todo from './todo/index.vue'
Vue.config.productionTip = false
console.log(zj2)
Vue.component('my-ele', zj2)//注册一个全局组件

new Vue({
  render: function (h) { return h(todo) },
  // render: h => h(App),
}).$mount('#app')
