import Vue from 'vue'
import App from './App.vue'
import myinput from './components123/my-input'
import tab from './tab/index.vue'
Vue.component('my-input', myinput) // 创造了一个全局组件my-input
Vue.config.productionTip = false
new Vue({
  render: function (createElement) {
    return createElement(tab)
  }
}).$mount('#app123')
