## A 组件中使用了 B组件  那么A称为父组件  B称为子组件

## 局部组件 导入然后注册然后使用
## 全局组件  全局注册  任何组件都能使用

#### 父子组件数据传递

父传子  自定义属性+props
子传父  自定义事件+$emit


# 虚拟DOM   使用原生JS对象模拟真实DOM

<自定义组件名>
  <template slot-scope='自己瞎起的名字'>
    这里的结构{{自己瞎起的名字.行内属性1}}
    这里的结构{{自己瞎起的名字.行内属性2}}
  </template>
  <template #default='自己瞎起的名字'>
    这里的结构{{自己瞎起的名字.行内属性1}}
    这里的结构{{自己瞎起的名字.行内属性2}}
  </template>
  
</自定义组件名>

自定义组件内容
  <slot 行内属性1='xxx' 行内属性2='xxx'></slot>

插槽的作用 就是为了提升组件的复用性及灵活性


v-model 原理：  是:value 和 @input的集合体


@vue/cli 搭建出来的项目 是已经配置好的webpack项目

在根目录下的配置文件都是为 webpack服务的；
  vue.config.js 其实就是给webpack的配置文件


  <keep-alive include="aaa">
        <component :is="comp"></component>
  </keep-alive> 
  - 保证 name是aaa的组件不消毁， 其他组件都销毁
  -activated() {
      console.log("A组件被展示了");
    },
    deactivated() {
      console.log("A组件被休眠示了");
    },
    这两个钩子函数是专门为 keep-alive创造的；

SPA : single page application  
  页面内容是通过 js执行添加上去的  不利于seo 优化
  服务端渲染  SSR(把页面内容先在 服务端造好)

  局部组件和全局组件
  组件传参 父传子 自定义属性+props
          子传父 自定是事件+$emit
  组件不消毁  keep-alive   include exclude  max
              activated
              deactivated
  动态组件  component     
  name 组件名(组件ID)  keep-alive或者 递归组件   



  vuex
    state 是用来存储公用的数据
    mutations 是用来存储修改state中的属性的方法的
          组件中可以通过this.$store.commit(mutations中的属性名,传递给对应函数的实参)

