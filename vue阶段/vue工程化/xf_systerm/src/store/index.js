import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'

// import { getUserInfo } from '../api'

Vue.use(Vuex)
/* 
const user ={
  namespaced: true,
  state: {
    userInfo: {}
  },
  mutations: {
    updateUserInfo(state, option) {
      state.userInfo = option
    }
  },
  actions: {
    async updateUserInfoFn({ commit }) {
      let data = await getUserInfo()
      // console.log(data)
      commit('updateUserInfo', data.data)
    }
  }
} */

export default new Vuex.Store({
  // strict:true
  modules: {
    user123: user
  }
})
