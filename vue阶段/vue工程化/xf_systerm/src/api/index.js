import http from './http'
export function login(obj) {
  return http.post('/adminUser/login', obj)
}

// 获取用户信息的一个接口
export function getUserInfo() {
  return http.get('/adminUser/profile')
}

// 获取商品的分类
export function getCategories(level, pId) {
  return http.get('/categories', {
    params: {
      pageNumber: 1,
      pageSize: 10,
      categoryLevel: level,
      parentId: pId
    }
  })
}

// 获取商品列表   
export function getGoodsList(pn, ps = 10) {
  pn = pn || 1;
  return http.get('/goods/list', {
    params: {
      pageNumber: pn,
      pageSize: ps
    }
  })
}

export function changeStatus(status, ids) {
  return http.put('/goods/status/' + status, { ids })
}

export function getGoodsInfo(id) {
  return http.get('/goods/' + id)
}

export function addGoods(body) {
  return http.post('/goods', body);
}
export function updateGoods(body) {
  return http.put('/goods', body);
}