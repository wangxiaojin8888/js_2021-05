import { getUserInfo } from '../../api'
export default {
  namespaced: true,
  state: {
    userInfo: {}
  },

  mutations: {
    updateUserInfo(state, option) {
      // role :'super' 'admin' 'user'  'costum'
      state.userInfo = { ...option }
    }
  },
  actions: {
    async updateUserInfoFn({ commit }) {
      let data = await getUserInfo()
      // console.log(data)
      let obj = { ...data.data, role: 'super' }
      commit('updateUserInfo', obj)
      return Promise.resolve(obj)
    }
  }
}