import axios from 'axios'
import { Message } from 'element-ui'

let http = axios.create({
  baseURL: process.env.NODE_ENV == 'production' ? 'http://backend-api-02.newbee.ltd/manage-api/v1' : '/manage-api/v1'
})
// 添加请求拦截器
http.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  let token = localStorage.getItem('xf_s_token');
  config.headers.token = token
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
http.interceptors.response.use(function (response) {
  // 对响应数据做点什么l
  let data = response.data;
  if (data.resultCode != 200) {
    Message.error(data.message)
    return Promise.reject() // then后边一定正常数据
  }
  return response.data;
}, function (error) {
  // 对响应错误做点什么
  Message.error("系统繁忙")
  return Promise.reject(error);
});

export default http