import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'


Vue.use(VueRouter)
export const navRoutes = [
  {
    path: '/introduce',
    component: () => import(/*webpackChunkName:"introduce" */'../views/Layout/Introduce.vue'),
    meta: {
      title: "系统介绍",
      parent: "Dashboard",
      icon: 'el-icon-data-line',
      roles: ['super', 'admin', 'user', 'costum'] // 存储的是权限
    },
  },
  {
    path: '/dashboard',
    component: () => import(/*webpackChunkName:"dashboard" */'../views/Layout/Dashboard.vue'),
    meta: {
      title: "Dashboard",
      parent: "Dashboard",
      icon: 'el-icon-notebook-2',
      roles: ['super', 'admin', 'user', 'costum'] // 存储的是权限
    },
  },
  {
    path: '/swiper',
    component: () => import(/*webpackChunkName:"swiper" */'../views/Layout/Swiper.vue'),
    meta: {
      title: "轮播图设置",
      parent: "首页设置",
      icon: 'el-icon-present',
      roles: ['super', 'admin', 'user'] // 存储的是权限
    },
  },
  {
    path: '/addGoods',
    component: () => import(/*webpackChunkName:"addGoods" */'../views/Layout/AddGoods.vue'),
    meta: {
      title: "添加商品",
      parent: "Dashboard",
      icon: 'el-icon-present',
      roles: ['super', 'admin'] // 存储的是权限
    },
  },
  {
    path: '/hotGoods',
    component: () => import(/*webpackChunkName:"hotGoods" */'../views/Layout/HotGoods.vue'),
    meta: {
      title: "热销商品配置",
      parent: "首页设置",
      icon: 'el-icon-present',
      roles: ['super', 'admin', 'user'] // 存储的是权限
    },
  },
  {
    path: '/sort',
    component: () => import(/*webpackChunkName:"sort" */'../views/Layout/Sort.vue'),
    meta: {
      title: "分类管理",
      parent: "模块管理",
      icon: 'el-icon-present',
      roles: ['super', 'admin', 'user'] // 存储的是权限
    },
  },
  {
    path: '/goods',
    component: () => import(/*webpackChunkName:"goods" */'../views/Layout/Goods.vue'),
    meta: {
      title: "商品管理",
      parent: "模块管理",
      icon: 'el-icon-present',
      roles: ['super', 'admin', 'user'] // 存储的是权限
    },
  },
  {
    path: '/changepsw',
    component: () => import(/*webpackChunkName:"changepsw" */'../views/Layout/ChangePsw.vue'),
    meta: {
      title: "修改密码",
      parent: "系统管理",
      icon: 'el-icon-present',
      roles: ['super', 'admin', 'user', 'costum'] // 存储的是权限
    },
  }
]
const routes = [
  {
    path: '/',
    component: () => import(/*webpackChunkName:"layout"*/'../views/Layout.vue'),
    meta: {
      title: "新蜂商城后台管理系统"
    },
    redirect: '/introduce',
    children: navRoutes
  },
  {
    path: '/login',
    component: () => import(/*webpackChunkName:"login"*/'../views/Login.vue'),
    meta: {
      title: "登录"
    }
  },
  {
    path: '/404',
    component: () => import(/*webpackChunkName:"404"*/'../views/404.vue'),
    meta: {
      title: "404",
      roles: ['super', 'admin', 'user', 'costum']
    }
  },
  {
    path: '/*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // 登录i校验
  let token = localStorage.getItem('xf_s_token')
  if (to.path != '/login' && !token) {
    // 说明要去的路径 不是login 并且还没有登录  要调到登录页
    next('/login')
  } else {
    next()
  }

})

router.beforeEach((to, from, next) => {
  // 判断权限  to.meta.roles,  用户的权限 store.state.user123.userInfo.role
  if (to.meta.roles) {
    // 有这个属性 证明要去的路径是有权限设置的，就要判断是否有权限了
    // 需要等着后台返给了用户权限信息之后 再去决定是否执行next
    if (store.state.user123.userInfo.role) {
      console.log("非刷新")
      // 已经有用户权限了
      if (to.meta.roles.includes(store.state.user123.userInfo.role)) {
        next()
      } else {
        next('/404')
      }
    } else {
      console.log("刷新")
      // 还没有获取到用户的权限， 这个时候就不知道能不能向下走
      // 针对的是刷新的那次操作
      store.dispatch('user123/updateUserInfoFn').then(data => {
        if (to.meta.roles.includes(data.role)) {
          next()
        } else {
          console.log(404)
          next('/404')
        }

      })

    }
  } else {
    // 处理的是不需要权限的路径
    next()
  }
})
router.beforeEach((to, from, next) => {
  document.title = to.meta.title || "新蜂商城后台管理系统"
  next()
})


export default router

/*
前置守卫
router.beforeEach((to, from, next) => {
  // ...
})


全局解析守卫
router.beforeResolve

后置守卫
router.afterEach((to, from) => {
  // ...
  //路径跳转完成之后执行
})

路由独享守卫 配置到路由映射表里边的
// beforeEnter: (to, from, next) => {
//   // ...
// },


组件内的守卫
beforeRouteEnter(to, from, next) {
    // 在渲染该组件的对应路由被 confirm 前调用
    // 不！能！获取组件实例 `this`
    // 因为当守卫执行前，组件实例还没被创建
  },
  beforeRouteUpdate(to, from, next) {
    // 在当前路由改变，但是该组件被复用时调用
    // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，
    // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
    // 可以访问组件实例 `this`
  },
  beforeRouteLeave(to, from, next) {
    // 导航离开该组件的对应路由时调用
    // 可以访问组件实例 `this`
  }




  hash模式的原理  利用的就是window.addEventListener('hashchange')
  history模式：   利用的就是window.addEventListener('popstate')事件和pushstate、replaceState方法
*/
