/*
 Vue的生命周期函数「俗称：钩子函数」 
   + 从new Vue开始到页面渲染完成（或者从状态更改，到视图更新完成）中间都会经历很多事情，Vue内部提供很多钩子函数，可以让开发者在某个阶段做一些自己想做的事情！

  「第一次渲染视图/组件」
     + beforeCreate 创建实例之前，所以在此函数中是没有实例的「或者有实例对象，但是对象上啥都没挂载呢」
     + created 创建实例之后，在此钩子函数中，状态/方法等该劫持劫持、该挂载挂载...
       + 真实项目中，我们会在这个周期函数里发送数据请求，从服务器获取需要绑定的数据
       + 这样可以保证第一次渲染的过程中，数据已经在请求中了，当第一次渲染完，用不了多久真实数据就回来了，此时再重新渲染视图；这样保证真实的数据可以更快的出来！！
     + beforeMount 第一次渲染之前
     开始第一次渲染「模板编译->vNode虚拟DOM对象->DOM-DIFF->真实的DOM」...
     + mounted 第一次渲染完成
       + 虽然Vue推崇的是数据驱动视图，但是某些需求基于操作DOM完成会更方便，所以vue开发中是允许操作DOM的；而在mounted周期函数中，可以获取页面中的DOM元素！！
       + vue中提供了获取DOM的快捷方式：ref & $refs
       + 还可以在这里设置定时器，等待多久后干啥事等等...

   「视图/组件重新渲染(更新)：修改状态值...」
     + beforeUpdate 更新之前
     开始更新...
     + updated 更新之后「可以获取到最新的DOM内容」
     真实项目中，我们往往会基于vm.$nextTick([callback])代替updated：nextTick也会在状态更改、视图更新后触发，而且比updated还靠后!!

   「销毁视图/组件：vm.$destroy()...」
     + beforeDestroy 销毁之前
     + destroyed 销毁之后
     都销毁了个啥？--> 到销毁前,视图长啥样,以后就啥样了!
     还有一些东西并不会销毁：
       + 自己设定的定时器
       + 自己获取DOM后，基于onxxx或者addEventListener实现的事件绑定也不会被销毁
       + ...
       所以我们会在beforeDestroy或者destroyed中，把这些自己写的手动释放！！
 */
let vm = new Vue({
    data: {
        num: 10
    },
    methods: {
        setNum() {
            this.num++;
            this.$nextTick(() => {
                console.log('nextTick');
            });
        }
    },
    //-----
    beforeCreate() {
        // console.log('创建实例之前', this.num); //=>undefined
    },
    created() {
        // console.log('创建实例之后', this.num); //=>10
    },
    beforeMount() {
        // console.log('第一次渲染之前');
    },
    mounted() {
        // console.log('第一次渲染完成', this.$refs.yufei); //=>获取视图中ref='yufei'的DOM原生对象
        this.autoTimer = setInterval(() => {
            console.log('OK');
            this.num++;
        }, 2000);
    },
    //-----
    beforeUpdate() {
        // console.log('更新之前');
    },
    updated() {
        // console.log('更新之后');
    },
    //-----
    beforeDestroy() {
        // console.log('销毁之前');
        clearInterval(this.autoTimer);
    },
    destroyed() {
        // console.log('销毁之后');
    }
});
vm.$mount('#app');