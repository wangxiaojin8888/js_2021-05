/*
 filters 过滤器：把视图中要渲染的内容进行格式化或者二次处理
   例如：{{time|formatTime}}  要渲染time状态，渲染之前先经过formatTime过滤方法处理一下，并且把time值传递给formatTime这个方法
   + 过滤器方法中的this不再是当前类的实例，而是window
   + 方法返回的结果就是最后要渲染的内容
   + 过滤器中写的方法并没有直接挂载到实例上
   + 和methods相同，每一次视图更新，过滤器方法都要执行，不管依赖的状态是否改变
   ------
   特点：函数中的this不是实例；没有挂载到实例上；可以基于“{{xxx|filterA|filterB}}”实现出“filterB(filterA(xxx))”这样的效果；可以基于Vue.filter创建全局过滤器函数，这样每个组件中都可以使用；
 */
let vm = new Vue({
    data: {
        msg: 'hello world',
        time: '2022-1-4 10:48:00'
    },
    filters: {
        formatTime(time) {
            let arr = time.match(/\d+/g),
                temp = '{1}-{2} {3}:{4}';
            return temp.replace(/\{(\d+)\}/g, (_, $1) => {
                let val = arr[$1];
                val = val.length < 2 ? '0' + val : val;
                return val;
            });
        }
    }
});
vm.$mount('#app');
setTimeout(() => {
    vm.msg = '你好世界';
}, 2000);