/*
 watch监听器：监听现有某个状态的改变，当状态改变，可以做一些自己想做的事情
   + 默认情况下，第一次渲染，监听器不会执行，只有当状态改变，监听函数才会执行「可以获取最新值和原始状态值」
   + 默认是“浅监听”：只能监听当前值有没有改变，如果是对象，对象里面的属性值改变，它监听不到
   + 我们可以自己调整为：第一次立即执行immediate & 深度进行监听deep
   + 可以把watch写在配置项中，设置局部的监听器；也可以基于“vm.$watch”创建监听器！！
 ----
 watch & computed
   + watch是监听现有状态，不会创建新的状态出来；而computed虽然也可以监听某个状态的改变，但是他的目的是创造一个全新的状态出来！！「很多需求，用两者都可以实现」
   + watch每次只能监听一个值「假设:A/B状态不论哪个改变，都要执行handle方法，用watch需要一个个的去监听」；但是computed不需要，因为只要出现在它方法中的状态都会自动建立依赖，不论哪个依赖改变，方法都会重新执行！！
     watch:{
         A(next,prev){this.handle(next,prev)},
         B(next,prev){this.handle(next,prev)}
     },
     computed:{
         C(){
            this.A + this.B
         }
     }
   + 但是都可以实现：只有监听或者依赖的状态改变，方法才会执行！！
   ----项目中尽可能多用computed少用watch！！
 */
let vm = new Vue({
    data: {
        msg: 'hello world',
        time: '2022-1-4 10:48:00',
        time2: '',
        obj: {
            name: '珠峰培训'
        }
    },
    watch: {
        time: {
            handler(next) {
                let arr = next.match(/\d+/g),
                    temp = '{1}-{2} {3}:{4}';
                temp = temp.replace(/\{(\d+)\}/g, (_, $1) => {
                    let val = arr[$1];
                    val = val.length < 2 ? '0' + val : val;
                    return val;
                });
                this.time2 = temp;
            },
            immediate: true
        },
        /* obj: {
            handler(next, prev) {
                console.log(next?.name, prev?.name);
            },
            deep: true,
            immediate: true
        }, */
        /* obj(next, prev) {
            // next:最新的状态值
            // prev:修改之前的状态值
            console.log(next, prev);
        } */
    }
});
vm.$mount('#app');
setTimeout(() => {
    vm.msg = '你好世界';
    vm.obj.name = '哈哈哈';
}, 2000);