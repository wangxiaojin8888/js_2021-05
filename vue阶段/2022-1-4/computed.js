let vm = new Vue({
    data: {
        list: [{
            id: 1,
            count: 2,
            price: 12.5
        }, {
            id: 2,
            count: 0,
            price: 10.5
        }, {
            id: 3,
            count: 3,
            price: 8.5
        }, {
            id: 4,
            count: 0,
            price: 8
        }, {
            id: 5,
            count: 0,
            price: 14.5
        }]
    },
    computed: {
        // 总数量
        total() {
            return this.list.reduce((result, item) => result + item.count, 0);
        },
        // 总价格
        prices() {
            return this.list.reduce((result, item) => result + item.count * item.price, 0);
        },
        // 最贵单价
        max() {
            let arr = [0];
            this.list.forEach(item => {
                if (item.count > 0) {
                    arr.push(item.price);
                }
            });
            return Math.max(...arr);
        }
    },
    methods: {
        // 点击事件
        handle(id, type) {
            let item = this.list.find(item => +item.id === +id);
            if (!item) return;
            if (type === 'minus') {
                item.count--;
                if (item.count < 0) item.count = 0;
                return;
            }
            item.count++;
        }
    }
});
vm.$mount('#app');