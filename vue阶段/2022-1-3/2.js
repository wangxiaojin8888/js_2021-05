let vm = new Vue({
    data: {
        a: 'box',
        username: '珠峰培训',
        sex: 0,
        hobby: ['唱歌', '跳舞'],
        city: '北京',
        citys: ['北京', '上海']
    },
    methods: {
        /* change(ev) {
            let text = ev.target.value;
            this.username = text;
        },
        handle(ev) {
            console.log('OK');
        }, */
        /* centerHandle(ev) {
            console.log('CENTER');
        },
        innerHandle(ev) {
            console.log('INNER');
        },
        boxHandle(ev) {
            console.log('BOX');
        } */
    }
});
vm.$mount('#app');



/* let vm = new Vue({
    data: {
        msg: '<a href="">hello world</a>',
        info: {
            name: '珠峰',
            age: 13
        },
        arr: [10, 20, 30, 5],
        flag: true,
        obj: {
            0: 10,
            1: 20,
            length: 2
        }
    },
    // 编写普通方法：这里编写的方法都会挂载到vm实例上{这样可以在视图中直接使用}
    //   + 方法不要是箭头函数，因为写普通函数，方法中的this永远都是vm实例「vue内部做的处理」
    methods: {
        // @click="handle1"  可以获取事件对象
        handle1(ev) {
            this.flag = !this.flag;
        },
        // @click="handle2(10,20)" handle2也不是立即执行，而是等待点击才执行，这里只是实现与传递一些实参信息而已；当点击的时候，把handle2执行，并且传递10/20；-> 无法获取事件对象
        handle2(x, y) {
            this.flag = !this.flag;
            console.log(x, y);
        },
        // @click="handle3(10,20,$event)" 在上述的基础上，基于$event把事件对象传递进来
        handle3(x, y, ev) {
            this.flag = !this.flag;
            console.log(x, y, ev);
        }
    }
});
vm.$mount('#app');
 */