/*
 Vue2.6.14 采用的是 options API 
   @1 指定视图模板「vue中的视图是基于template语法构建的」
     + el：指定页面中的哪个容器作为要渲染的视图，并且把渲染后的结果“挂载”到这个容器中
     + template：指定视图模板，视图渲染完成，需要基于vm.$mount('#app')挂载到页面中
     + render：基于jsx语法渲染视图，渲染完成后也要基于$mount进行挂载
   @2 指定数据模型
     + data：基于data构建数据模型，它是一个对象，对象中包含视图中需要的所有数据
       + 这里的数据会直接挂载到vm实例上 -> vm.msg
       + data中设定的数据是“响应式数据(也称之为状态)”：当数据更改后，vue可以自动监听到它的改变，并且让视图重新进行渲染；自己手动往实例上挂载的数据(例如:vm.xxx=xxx)，是非响应式的！！ => 所以真实项目中，需要在视图中渲染的数据，我们都要把其放在data中！！

 Vue2的响应式数据原理：基于Object.defineProperty对数据进行监听劫持
   + 想要知道数据是否为响应式的，只需要展开数据，看其是否有进行 get/set 劫持；有被劫持它就是响应式的，反之则是非响应式数据；
   + 如果某个属性值还是对象：默认基于递归的方式，把data中每一层级都进行深度的监听劫持
   + 如果属性值是一个数组：没有对数组的索引项做劫持，但是重写了数组的7个方法「push/pop/shift/unshift/splice/sort/reverse」，以后基于这几个方法去修改数组某一项，改变值的同时，也会触发视图重新渲染！！
   + 但是只有最开始写在data中的内容，才会按照上述的操作去做监听劫持；之前data中没有指定，后续自己手动加入进来的新内容，不会做劫持处理！！ => 所以真实项目中，我们会在data中，初始化所有后续需要的数据(哪怕开始不知道值，我们先赋值为null，也先把其初始化)，目的是让数据做监听劫持！！
     但是可以基于 vm.$set(对象,新增的属性,属性值) 来处理：一方面让新增的属性变为响应式的，同时也通知视图重新渲染！！
     也可以不把数据变为响应式的，但是每一次更改数据后，都需要基于 vm.$forceUpdate() 强制通知视图更新
 */
/* 
let data = {
    msg: 'hello world',
    info: {
        name: '珠峰',
        age: 13,
        teacher: {
            vue: '任金辉',
            js: '周老师'
        }
    },
    arr: [10, 20, 30, 40]
};

const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};
const compile = function compile() {
    // 视图渲染
    console.log('视图渲染');
};
let proto = {
    push() { },
    pop() { },
    shift() { },
    unshift() { },
    splice(...params) {
        [].splice.call(this, ...params);
        // 通知视图渲染
        compile();
    },
    sort() { },
    reverse() { }
};
Object.setPrototypeOf(proto, Array.prototype);
const observe = function observe(data) {
    let keys = Reflect.ownKeys(data);
    let proxy = Object.assign({}, data);
    keys.forEach(key => {
        Object.defineProperty(data, key, {
            get() {
                return proxy[key];
            },
            set(value) {
                if (proxy[key] === value) return;
                proxy[key] = value;
                // 通知视图渲染
                compile();
            }
        });
        // 如果属性值是对象：深度劫持
        if (isPlainObject(data[key])) {
            observe(data[key]);
            return;
        }
        // 如果属性值是数组：不会对索引做劫持,而是改变原型链指向 数组->自己构建的原型->Array.prototype
        if (Array.isArray(data[key])) {
            Object.setPrototypeOf(data[key], proto);
            return;
        }
    });
};
observe(data);
// data.info.name = 'xxx';
// data.info = {};
// data.arr = [];
// data.arr[0] = 100; //这样还是不渲染
// data.arr.splice(0, 1, 100); 
*/

let vm = new Vue({
    // 指定视图&挂载「view」：也可以直接基于$mount处理
    // el: '#app',
    // 指定数据层「model」
    data: {
        msg: 'hello world',
        info: {
            name: '珠峰',
            age: 13,
            teacher: {
                vue: '任金辉',
                js: '周老师'
            }
        },
        arr: [10, 20, 30, 40],
        obj: {}
    }
});
vm.text = '你好 世界';
vm.$mount('#app');

setTimeout(() => {
    // 第一次渲染后：我们去修改数据
    // vm.text = '哈哈哈'; //视图不渲染
    // vm.$set(vm, 'text', '哈哈哈'); //视图还是不渲染
    // vm.$set(vm.obj, 'name', 'xxx'); //这样可以通知视图渲染 & 把obj.name做了劫持

    vm.text = '哈哈哈';
    vm.$forceUpdate();
}, 2000);




/* let vm = new Vue({
    template: '#temp1', //<script type="x-template" id="temp1"> 视图模板 </script>
    data: {
        msg: 'hello world'
    }
});
vm.$mount('#app'); */

/* let vm = new Vue({
    template: `<div>
        <h2>{{msg}}</h2>
    </div>`,
    data: {
        msg: 'hello world'
    }
});
vm.$mount('#app'); */
