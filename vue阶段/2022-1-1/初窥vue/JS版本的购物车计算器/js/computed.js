const data = [{
    id: 1,
    count: 2,
    price: 12.5
}, {
    id: 2,
    count: 0,
    price: 10.5
}, {
    id: 3,
    count: 3,
    price: 8.5
}, {
    id: 4,
    count: 0,
    price: 8
}, {
    id: 5,
    count: 0,
    price: 14.5
}];

let computedBox = document.querySelector('#computedBox'),
    topBox = computedBox.querySelector('.top'),
    bottomBox = computedBox.querySelector('.bottom'),
    [countsBox, pricesBox, maxBox] = Array.from(bottomBox.querySelectorAll('.pronum')),
    pronumsBox = null,
    emsBox = null;

// 动态绑定数据
const render = function render() {
    let str = ``;
    data.forEach((item, index) => {
        let { count, price } = item;
        str += `<div class="hang" index="${index}">
            <i class="minus"></i>
            <span class="pronum">${count}</span>
            <i class="add"></i>
            <span class="info">
                单价：${price}元 &nbsp;&nbsp; 
                小计：<em>${count * price}</em>元
            </span>
        </div>`;
    });
    topBox.innerHTML = str;
    pronumsBox = Array.from(topBox.querySelectorAll('.pronum'));
    emsBox = Array.from(topBox.querySelectorAll('em'));
};

// 计算总计信息
const computed = function computed() {
    if (!pronumsBox || !emsBox) return;
    let counts = 0,
        prices = 0,
        max = [0];
    pronumsBox.forEach((item, index) => {
        let n = +item.innerHTML,
            m = +emsBox[index].innerHTML;
        counts += n;
        prices += m;
        if (n > 0) max.push(data[index].price);
    });
    countsBox.innerHTML = counts;
    pricesBox.innerHTML = prices.toFixed(2);
    maxBox.innerHTML = Math.max(...max);
};

// 基于事件委托实现点击操作
topBox.addEventListener('click', function (ev) {
    let target = ev.target,
        index,
        count;
    if (target.tagName === "I") {
        index = +target.parentNode.getAttribute('index');
        count = +pronumsBox[index].innerHTML;
        if (target.className === 'add') {
            count++;
        } else {
            count--;
            if (count < 0) count = 0;
        }
        pronumsBox[index].innerHTML = count;
        emsBox[index].innerHTML = count * data[index].price;
        // 重新计算总计信息
        computed();
    }
});

render();
computed();