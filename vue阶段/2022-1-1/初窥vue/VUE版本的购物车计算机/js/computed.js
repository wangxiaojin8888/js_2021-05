let vm = new Vue({
    // 指定视图层
    el: '#computedBox',
    // 指定数据层
    data: {
        list: [{
            id: 1,
            count: 2,
            price: 12.5
        }, {
            id: 2,
            count: 0,
            price: 10.5
        }, {
            id: 3,
            count: 3,
            price: 8.5
        }, {
            id: 4,
            count: 0,
            price: 8
        }, {
            id: 5,
            count: 0,
            price: 14.5
        }]
    },
    // 方法
    methods: {
        handle(id, type) {
            let item = this.list.find(item => +item.id === +id);
            if (!item) return;
            if (type === 'minus') {
                item.count--;
                if (item.count < 0) item.count = 0;
                return;
            }
            item.count++;
        }
    },
    // 计算属性
    computed: {
        subtotal() {
            let counts = 0,
                prices = 0,
                max = [0];
            this.list.forEach(item => {
                let { count, price } = item;
                counts += count;
                prices += count * price;
                if (count > 0) max.push(price);
            });
            return {
                counts,
                prices: prices.toFixed(2),
                max: Math.max(...max)
            };
        }
    }
});