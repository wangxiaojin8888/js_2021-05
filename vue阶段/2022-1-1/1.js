/* 
 数组常用的方法
   Array.prototype 供其实例调用 -> arr.xxx()
     + push
     + pop
     + shift
     + unshift
     + splice
     + slice
     + concat
     + indexOf/lastIndexOf
     + includes 「ES6」
     + join
     + toString
     + sort((a,b)=>a-b)
     + reverse
     ------迭代方法
     + forEach 迭代
     + map 迭代(支持返回值)
     + filter 按照指定条件实现筛选
     + find/findIndex 按照指定条件进行查找「ES6」
     + some 查找数组中是否有符合条件的这一样,只要有一项符合条件,结果就是TRUE
     + every 所有项都必须符合条件,结果才是TRUE
     + reduce/reduceRight 迭代(可以实现结果的累计)
     ------其它方法
     + fill(value[,start[,end]]) 填充数组「不传递索引是每一项都填充为value，设置索引，只把找到的索引项内容进行填充」「ES6」
     + flat(数字/Infinity) 数组扁平化「ES6」

   Array 静态私有属性方法 -> Array.xxx()
     + from 把类数组(或者Set)转换为数组「ES6」
     + isArray 检测当前值是否为数组「ES5」
     + of 创建新数组，并为其准备数组中的内容「ES6」
     + ...
 */

/* // 思考题：flat方法不兼容IE，需要你去扩展其余的方案?
let arr = [
    0,
    [
        1,
        2,
        [3, 4],
        5,
        [
            6,
            7,
            [8, 9, [100, 200, [300, 400, [500, 600]]]]
        ]
    ],
    10
];
arr = arr.flat(Infinity);
console.log(arr); */

let data = [{
    id: 1,
    count: 2,
    price: 12.5
}, {
    id: 2,
    count: 0,
    price: 10.5
}, {
    id: 3,
    count: 3,
    price: 8.5
}, {
    id: 4,
    count: 0,
    price: 8
}, {
    id: 5,
    count: 0,
    price: 14.5
}];

/* let counts = data.reduce((result, item) => result + item.count, 0);
console.log(counts); */

/* // reduce：依次迭代数据中的每一项，并且可以累积每一次处理的结果
let arr = [11, 12, 13, 14];
let total = arr.reduce((result, item, index) => {
    // 第一次: result=11 {获取数组第一项}  item=12/index=1 {从数组第二项开始迭代} -> 23
    // 第二次：result=23 {获取上一次回调函数处理的结果} item=13/index=2 -> 36
    // 第三次：result=36 item=14/index=3 -> 50  最后一次迭代，把最后返回的结果当做reduce的返回值
    return result + item;
});
total = arr.reduce((result, item, index) => {
    // 第一次：result=100  item=11/index=0  如果设置了初始值，result用的是初始值，数组从第一项开始迭代
    // 第二次：result=111  item=12/index=1
    // ...
    return result + item;
}, 100);
console.log(total); */

/* // some和every唯一的区别：some只要有一项符合就是TRUE，every需要所有项都符合条件...
let flag = data.every((item, index) => {
    return item.count > 0;
});
console.log(flag); */

/* 
// find：相对于filter来讲，只找到第一个符合条件的这一项即可；如果没有任何匹配的项，则结果是undefined;
let item = data.find((item, index) => {
    return item.count > 0;
});
console.log(item); 
*/

/* 
// filter:依次迭代数组中的每一项,每一次迭代都把回调函数执行
//   + 检测回调函数执行的返回值：如果返回的是TRUE，那么这一项就被筛选到了，返回的是FALSE，则把这一项过滤掉，原始数组不变，以新数组形式返回
//   + 如果没有任何的匹配条件的项，最后返回空数组
let arr = data.filter((item, index) => {
    // 条件:只要每一项count大于零的
    return item.count > 0;
});
console.log(arr); 
*/