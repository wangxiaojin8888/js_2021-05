/*
 对象常用的方法
   Object.prototype -> 实例.xxx()
     + hasOwnProperty 检测是否为私有属性
     + isPrototypeOf 检测某个对象是否出现在另外一个对象的原型链上
     + propertyIsEnumerable 检测某个属性是不是可枚举属性
     + toString 检测数据类型的
     + valueOf 获取对象原始值
     + ...

   Object静态私有属性方法 -> Object.xxx()
     + assign(obj1,obj2,...) 合并对象「ES6」
     + create([proto]) 创建一个新的对象,并且把[proto]对象作为它的原型,[proto]可以为null/对象
     + getPrototypeOf(obj) 获取obj实例的原型对象
     + setPrototypeOf(obj,prototype) 设置obj实例的原型对象是prototype「兼容IE11及以上」
     + is(val1,val2) 比较两个值是否相等(规则基于===比较)，Object.is(NaN,NaN)返回true「ES6」
     -----
     + defineProperty(obj, prop, descriptor) 可以对某个对象的某个属性做监听劫持(给对象的某个属性设置描述规则「是否可删除、是否可枚举、是否可修改、初始值」)
     + getOwnPropertyDescriptor(obj,prop) 获取对象某个属性的规则描述
     + getOwnPropertyDescriptors(obj) 获取对象每一个属性的规则描述
     -----
     + keys(obj) 获取obj对象所有 “可枚举、非Symbol类型” 的私有属性
     + getOwnPropertyNames(obj) 获取obj对象所有 “非Symbol类型” 的私有属性，含可枚举和不可枚举的
     + getOwnPropertySymbols(obj) 获取 “Symbol类型” 的私有属性，含可枚举和不可枚举「ES6」
     -----
     + freeze(obj) 冻结这个对象「一但被冻结后，不能新增属性(不可扩展的)、不能删除属性、不能做劫持、不能改变属性值...(只有枚举和之前还是一致的)」
     + isFrozen(obj) 检测当前对象是否是被冻结的
     -----
     + seal(obj) 密封一个对象(在冻结的基础上，如果之前是可修改的，现在还是可修改的)
     + isSealed(obj) 检测是否是密封的对象
     -----
     + preventExtensions(obj) 让obj对象变为不可扩展的(只限制不能加新的属性)「兼容IE11及以上」
     + isExtensible(obj) 检测对象是否为可扩展的

  Reflect ES6新增的一个对象，集成了很多对“对象”进行操作的方法
    + apply(target,thisArgument,argumentsList) 等价于 Function.prototype.apply
    + construct(target,argumentsList) 等价于 new target(...argumentsList)
    + defineProperty 等价于 Object.defineProperty
    + deleteProperty 等价于 delete
    + get 对“对象”进行成员访问，和 obj[属性] 一样
    + getOwnPropertyDescriptor 等价于 Object.getOwnPropertyDescriptor
    + getPrototypeOf 等价于 Object.getPrototypeOf
    + has 等价于 in操作符
    + isExtensible 等价于 Object.isExtensible
    *+ ownKeys 等价于 Object.getOwnPropertyNames+Object.getOwnPropertySymbols
    + preventExtensions 等价于 Object.preventExtensions
    + set 等价于 对象[属性]=值
    + setPrototypeOf 等价于 Object.setPrototypeOf
  Reflect不是在创造新的API，只是把原有对“对象”操作的各种API“整合”在一起！！
 */
let obj = { name: '珠峰培训', age: 13, [Symbol('AA')]: 100 };
const func = function func(x, y) {
    console.log(this, x + y);
};

// obj['sex'] = 12;
// Reflect.set(obj, 'sex', 12);
// console.log(obj);

// let keys = Object.getOwnPropertyNames(obj).concat(Object.getOwnPropertySymbols(obj));
// let keys = Reflect.ownKeys(obj);
// console.log(keys);

// console.log('name' in obj);
// console.log(Reflect.has(obj, 'name'));

// console.log(obj.name);
// console.log(Reflect.get(obj, 'name'));

// delete obj.age;
// Reflect.deleteProperty(obj, 'age');
// console.log(obj);

// Object.defineProperty(obj, 'name', {
//     get() { return '@'; }
// });
// Reflect.defineProperty(obj, 'name', {
//     get() { return '@'; }
// });

// let f = new func(10, 20);
// Reflect.construct(func, [10, 20]);

// func.apply(obj, [10, 20]);
// Reflect.apply(func, obj, [10, 20]);

//--------------
/* let obj = {
    name: '珠峰培训'
};
Object.defineProperty(obj, 'age', {
    configurable: false,
    enumerable: false,
    writable: false,
    value: 13
});
console.log(obj); */

//--------------
/* let obj1 = {
    name: '珠峰培训',
    age: 13
};
Object.preventExtensions(obj1);
console.log(Object.isExtensible(obj1)); //false
obj1.sex = 1;
console.log(obj1.sex); //undefined */

/* let obj = {
    name: '珠峰培训',
    age: 13
};
Object.freeze(obj);
console.log(Object.isExtensible(obj)); //false 冻结的对象本身也是不可扩展的 */

//--------------
/* let obj = {
    name: '珠峰培训',
    age: 13
};
console.log(Object.getOwnPropertyDescriptor(obj, 'name'));
console.log(Object.getOwnPropertyDescriptors(obj)); */

//--------------
/*
let obj = {
    name: '珠峰培训',
    age: 13
};
console.log(Object.isFrozen(obj)); //false
Object.freeze(obj);
console.log(Object.isFrozen(obj)); //true
*/

//--------------
/* 
let obj = {
    name: '珠峰培训'
};
Object.defineProperty(obj, 'name', {
    get() {
        // 只要获取obj的name属性值，就会触发get函数
        console.log('GETTER');
        return '@';
    },
    set(value) {
        // 只要给obj的name属性赋值，就会触发set函数；value是你需要设置的值；
        console.log('SETTER', value);
    }
}); 
*/

/* 
let obj = {
    name: '珠峰培训'
};
等价于
let obj = {};
Object.defineProperty(obj, 'name', {
    configurable: true,
    enumerable: true,
    writable: true,
    value: '珠峰培训'
}); 
*/

/* 
// 对“对象”的某个属性做监听劫持
//   + 属性事先就存在于对象中：则此属性是 可被删除、一般也是可被枚举、可被修改 的
//   + 对象中原本不存在这个属性：则给对象新设置这个属性，可以基于value设置初始值，新增的属性默认是 不可删除、不可枚举、不可修改 的
Object.defineProperty(obj, 'name', {
    // 是否允许删除  false是不允许(默认)  true是允许
    configurable: false,
    // 是否可枚举「如果是不可枚举的，基于 for/in循环、Object.keys 都获取不到」
    enumerable: false,
    //-----以下两个规则不能和get/set劫持并存
    // 是否可修改
    writable: false,
    // 给对象的属性设置初始值
    // value: '珠峰培训'
}); 
*/
