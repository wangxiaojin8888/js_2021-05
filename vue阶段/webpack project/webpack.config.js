/*
 自定义webpack打包的规则 webpack.config.js或webpackfile.js
   模式mode：开发模式development  生产模式production
   入口entry：打包的入口文件，默认src/index.js
   出口output：打包后的内容输出到哪里，默认dist/main.js
   加载器loader：主要用于代码的编译
     + @babel-loader  编译JS
     + style-loader/less-loader/postcss-loader 编译CSS
     + file-loader/url-loader 编译图片
     + ...
   插件plugin：主要用于代码抽离、压缩、合并等
     + html-webpack-plugin 把生层的js/css插入到html页面中 & 压缩html
     + terser-webpack-plugin 压缩js
     + mini-css-extract-plugin 提取CSS
     + css-minimizer-webpack-plugin 实现CSS压缩
     + clean-webpack-plugin 清除之前打包的内容
     + ...
   优化项optimization：主要用于让压缩的文件体积更小、让打包速度更快等
   解析项resolve
   webpack-dev-server
     + 启动web服务预览项目，实现热更新
     + 实现proxy跨域代理
     + ...
 */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    mode: 'production',
    entry: './src/main.js',
    output: {
        // 打包后的文件名
        filename: '[hash].min.js',
        // 打包后的文件路径
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: 'index.html'
        })
    ],
    devServer: {
        port: 8080,
        host: '127.0.0.1',
        compress: true,
        open: true,
        hot: true,
        proxy: {
            "/api": {
                target: "http://127.0.0.1:9000",
                changeOrigin: true,
                ws: true,
                pathRewrite: { '^/api': '' }
            }
        }
    }
};