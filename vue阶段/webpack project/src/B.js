import sum from './A';
export default function average(...params) {
    params = params.filter(item => !isNaN(item));
    let total = sum(...params);
    return (total / params.length).toFixed(2);
};