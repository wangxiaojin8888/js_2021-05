/*
 ES6Module规范中的导入导出
   import需要放在每个模块中最开始的位置
  「只会导出一个」
    导出：export default sum;  -> 本质导出一个对象 {default:sum}
    导入：import xxx from './A.js';
       + 在浏览器端直接使用ES6Module，导入路径中的后缀名不能省略；基于webpack处理后的时候，是可以省略后缀名的！
       + 声明一个叫做 xxx 的变量，用来获取导出对象 default 的属性值

  「想导出多个：方案一」
    export default { sum, ...};
    import A from './A.js';
    ----PS
    import {sum} from '...';  这样会报错，因为只有写成一个变量，才会默认去找 导出对象中的default属性值！如果写为解构赋值的方式，就不是去找default属性值了！

  「想导出多个：方法二」
    export 后面必须跟着声明变量且赋值的操作
    export default sum;
    export default function sum(){}; 加了default的，可以导出声明&定义所创建的值，也可以把一个变量存储的值导出
    ---
    export const num=10;
    export function sum(){};
    export const obj={};
    ---
    import {num,sum} from '...';  可以直接解构赋值
    import * as A from '...'; 也可以导入所有，并且赋值给A
 */
// import A from './A.js';
// let { sum } = A;
// console.log(sum);

// import * as A from './A.js';
// console.log(A['default']); //sum

// import sum, { num, obj, fn } from './A.js';
// console.log(sum, num, obj, fn); //sum 10 对象 函数

import sum from "./A.js";
import average from "./B.js";
console.log(sum(12, 23, 34, 45, 56));
console.log(average(12, 23, 34, 45, 56));


/* 
 项目中常用的模块规范：
   + 非(webpack)工程化项目「也就是不用vue/react」：我们一般基于单例模式或者AMD思想去做！！
   + 工程化项目：一般用CommonJS或者ES6Module
   -----
   JS运行环境和模块规范的一些事：
   + 浏览器端能支持 ES6Module，但是不支持 CommonJS
   + Node端能支持 CommonJS，但是不支持 ES6Module
   + 但是“webpack环境”下，ES6Module和CommonJS可以混着用！例如：基于CommonJS导出的，可以基于ES6Module导入，同理基于ES6Module导出的，也可以基于CommonJS导入！！  “原因：webpack是基于Node去打包编译代码、然后要把编译后的内容放在浏览器中运行；webpack内部自己实现了模块的管理，基于它可以实现两种模式的混用”
*/