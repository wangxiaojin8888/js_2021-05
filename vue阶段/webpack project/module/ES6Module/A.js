/* export const num = 10;
export const obj = { name: 'A' };
export function fn() { }; */
export default function sum(...params) {
    return params.reduce((result, item) => {
        item = +item;
        if (isNaN(item)) return result;
        return result + item;
    }, 0);
};

/*
 {
    Symbol.toStringTag:'Module',
    num:10,
    obj:对象,
    fn:函数,
    default:sum
 } 
 */