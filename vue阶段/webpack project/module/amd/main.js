require.config({
    baseUrl: './lib'
});

require(['B', 'A'], (B, A) => {
    console.log(A.sum(12, 23, 34, 45, 56));
    console.log(B.average(12, 23, 34, 45, 56));
});

/*
 AMD管理模块之间的依赖，都是“前置导入”
   + define(['A'], function (A) {}); 
 如果依赖的模块可以按需导入就很好！！ -> Node自带的模块规范：CommonJS规范
 */