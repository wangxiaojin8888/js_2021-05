define(['A'], function (A) {
    const average = function average(...params) {
        params = params.filter(item => !isNaN(item));
        let total = A.sum(...params);
        return (total / params.length).toFixed(2);
    };
    return {
        average
    };
});