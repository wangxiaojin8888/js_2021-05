// 依赖了A模块中的方法
let BModule = (function () {
    const name = 'B';
    const average = function average(...params) {
        params = params.filter(item => !isNaN(item));
        let total = AModule.sum(...params);
        return (total / params.length).toFixed(2);
    };

    return {
        name,
        average
    };
})();