let AModule = (function () {
    const name = "A";
    const sum = function sum(...params) {
        return params.reduce((result, item) => {
            item = +item;
            if (isNaN(item)) return result;
            return result + item;
        }, 0);
    };

    return {
        name,
        sum
    };
})();