// 每创建一个JS就相当于创建一个模块「Node内部会基于闭包把它包起来」
const sum = function sum(...params) {
    return params.reduce((result, item) => {
        item = +item;
        if (isNaN(item)) return result;
        return result + item;
    }, 0);
};

module.exports = {
    sum
};