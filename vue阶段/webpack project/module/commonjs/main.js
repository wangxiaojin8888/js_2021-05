/*
 CommonJS模块规范中
    module.exports = { sum:function... };  导出模块中需要供外部调用的方法 
    let A = require(地址「相对地址」);  导入模块 -> A.sum()
    let {sum}= require(地址「相对地址」);

    module.exports = sum;
    let sum = require(地址);

 CommonJS模块规范不支持浏览器直接访问，它是Node中的，只能在Node环境下执行！！
    + 淘宝的玉伯，在很早之前，研究了一个插件 sea.js ，目的是把Node端的 CommonJS 规范搬到浏览器中使用 => CMD模块化管理思想
 CommonJS的按需导入使用起来更方便、性能更好
*/

const { sum } = require('./A');
console.log(sum(12, 23, 34, 45, 56));

const average = require('./B');
console.log(average(12, 23, 34, 45, 56));